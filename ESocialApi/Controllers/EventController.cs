﻿using ESocialApi.DB.Model;
using ESocialApi.Interfaces.ESocialTransmissao;
using ESocialApi.Interfaces.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Newtonsoft.Json;
using ESocialApi.Interfaces;
using System;
using System.Text;

namespace ESocialApi.Controllers
{
    [Route("eventos")]
    public class EventController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICreateLotEvents _createLotEvents;
        private readonly IESocialService _eSocialService;


        public EventController(IUnitOfWork unitOfWork, ICreateLotEvents createLotEvents, IESocialService eSocialService)
        {
            _unitOfWork = unitOfWork;
            _createLotEvents = createLotEvents;
            _eSocialService = eSocialService;
        }

        [HttpPost("postEvents")]
        public IActionResult postEvents([FromBody]object json)
        {
            ESocial obj = JsonConvert.DeserializeObject<ESocial>(json.ToString());
            return Ok(_eSocialService.GenerateEvents(obj));
        }

        [HttpGet("2210")]
        public IActionResult Get2210()
        {
            return Ok(_createLotEvents.CreateEvent("2210", Request.Headers["Token"], 2));
        }

        [HttpGet("2220")]
        public IActionResult Get2220()
        {
            return Ok(_createLotEvents.CreateEvent("2220", Request.Headers["Token"], 2));
        }

        [HttpGet("2240")]
        public IActionResult Get2240()
        {
            return Ok(_createLotEvents.CreateEvent("2240", Request.Headers["Token"], 2));
        }

        [HttpGet("getXML")]
        public IActionResult GetXML()
        {
            int idESocial = int.Parse(Request.Query["idESocial"]);
            return Ok(_createLotEvents.GetXmlEvent(idESocial, Request.Query["tpEvento"]));
        }

        [HttpGet("getESocial")]
        public IActionResult GetESocial()
        {
            string token = Request.Headers["Token"];
            string idESocial = Request.Query["idESocial"];
            string tpEvento = Request.Query["tpEvento"];

            ESocial ESocial = _unitOfWork.ESocial.GetESocialAll(int.Parse(idESocial));
            List<ESocialProcessamento> processamentos = new List<ESocialProcessamento>();
            if (ESocial != null)
            {
                if (_unitOfWork.ESocial.UltimoProcessamento(ESocial.Id) != null)
                {
                    processamentos.Add(_unitOfWork.ESocial.UltimoProcessamento(ESocial.Id));
                }
                ESocial.ESocialProcessamento = processamentos;
            }
            var json = JsonConvert.SerializeObject(ESocial);
            return Ok(json);
        }

        [HttpGet("getEventos")]
        public IActionResult GetEventos()
        {
            string token = Request.Headers["Token"];
            string tpEvento = Request.Query["tpEvento"];
            string cnpjs = Request.Query["cnpj"];
            int ano = int.Parse(Request.Query["ano"]);
            int mes = int.Parse(Request.Query["mes"]);
            string status = Request.Query["status"];
            string nomeFuncionario = Request.Query["nomeFuncionario"];
            string codAmb = Request.Query["ambiente"];

            var eventos = _unitOfWork.ESocial.ObtemEventosPorEmpresa(cnpjs, tpEvento, ano, mes, status, nomeFuncionario, codAmb);

            var json = JsonConvert.SerializeObject(eventos);
            return Ok(json);
        }
    }
}
