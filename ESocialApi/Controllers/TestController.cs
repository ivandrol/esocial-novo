﻿using Microsoft.AspNetCore.Mvc;
using ESocialApi.Interfaces.Repository;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Linq;
using System;
using ESocialApi.Utils;
using System.Xml;
using ESocialApi.Helper.Xml;
using Xml.Model.RetornoEnvio;
using System.Text;
using System.Net;
using System.IO;

namespace ESocialApi.Controllers
{
    [Route("api")]
    public class TestController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoteRepository _lotRepository;
        private readonly IESocialRepository _eSocial;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        string agrat8 = "18115085";
        string agrat = "18115085000119";

        string serplamed = "07131887000104";

        X509Certificate2 certificado = Assinatura.GetCertiticate("464832ce748a32d9");



        public TestController(IUnitOfWork unitOfWork, ILoteRepository lotRepository, IESocialRepository eSocial, IServiceScopeFactory serviceScopeFactory)
        {
            _unitOfWork = unitOfWork;
            _lotRepository = lotRepository;
            _eSocial = eSocial;
            _serviceScopeFactory = serviceScopeFactory;
        }

        [HttpGet("enviar_1000")]
        public async System.Threading.Tasks.Task<IActionResult> enviar_1000()
        {
            var Id = $"ID{"1"}{agrat8.PadRight(14, '0')}{DateTime.Now.ToString("yyyyMMddHHmmss")}00001";

            XAttribute grupo = new XAttribute("grupo", "1");
            XNamespace ns = "http://www.esocial.gov.br/schema/lote/eventos/envio/v1_1_1";
            XElement xESocial = new XElement(ns + "eSocial");
            XElement xEnvioLoteEventos = new XElement(ns + "envioLoteEventos", grupo);
            XElement xIdeEmpregador = new XElement(ns + "ideEmpregador");
            xIdeEmpregador.Add(new XElement(ns + "tpInsc", "1"));
            xIdeEmpregador.Add(new XElement(ns + "nrInsc", agrat8));
            XElement xIdeTransmissor = new XElement(ns + "ideTransmissor");
            xIdeTransmissor.Add(new XElement(ns + "tpInsc", "1"));
            xIdeTransmissor.Add(new XElement(ns + "nrInsc", serplamed));
            XElement xEventos = new XElement(ns + "eventos");
            XAttribute id = new XAttribute("Id", Id);
            XElement xEvento = new XElement(ns + "evento", id);

            XNamespace nsevt = "http://www.esocial.gov.br/schema/evt/evtInfoEmpregador/v02_05_00";
            XElement evento = new XElement(nsevt + "eSocial");
            XElement evtInfoEmpregador = new XElement(nsevt + "evtInfoEmpregador");
            evtInfoEmpregador.Add(id);
            XElement ideEvento = new XElement(nsevt + "ideEvento");
            ideEvento.Add(new XElement(nsevt + "tpAmb", "2"));
            ideEvento.Add(new XElement(nsevt + "procEmi", "1"));
            ideEvento.Add(new XElement(nsevt + "verProc", "1.0"));
            XElement ideEmpregador = new XElement(nsevt + "ideEmpregador");
            ideEmpregador.Add(new XElement(nsevt + "tpInsc", "1"));
            ideEmpregador.Add(new XElement(nsevt + "nrInsc", agrat8));

            XElement infoEmpregador = new XElement(nsevt + "infoEmpregador");

            XElement inclusao = new XElement(nsevt + "inclusao");

            XElement idePeriodo = new XElement(nsevt + "idePeriodo");
            idePeriodo.Add(new XElement(nsevt + "iniValid", "2017-01"));

            XElement infoCadastro = new XElement(nsevt + "infoCadastro");
            infoCadastro.Add(new XElement(nsevt + "nmRazao", "Serplamed"));
            infoCadastro.Add(new XElement(nsevt + "classTrib", "99"));
            //infoCadastro.Add(new XElement(nsevt + "natJurid", "2305"));
            infoCadastro.Add(new XElement(nsevt + "natJurid", "2062"));
            infoCadastro.Add(new XElement(nsevt + "indCoop", "0"));
            infoCadastro.Add(new XElement(nsevt + "indConstr", "0"));
            infoCadastro.Add(new XElement(nsevt + "indDesFolha", "0"));
            infoCadastro.Add(new XElement(nsevt + "indOptRegEletron", "0"));
            infoCadastro.Add(new XElement(nsevt + "indEntEd", "N"));
            infoCadastro.Add(new XElement(nsevt + "indEtt", "N"));

            XElement contato = new XElement(nsevt + "contato");
            contato.Add(new XElement(nsevt + "nmCtt", "Ivandro Lessing"));
            contato.Add(new XElement(nsevt + "cpfCtt", "01590315081"));
            contato.Add(new XElement(nsevt + "foneFixo", "5535376148"));
            contato.Add(new XElement(nsevt + "email", "ivandro.lessing@outlook.com"));

            XElement softwareHouse = new XElement(nsevt + "softwareHouse");
            softwareHouse.Add(new XElement(nsevt + "cnpjSoftHouse", agrat));
            softwareHouse.Add(new XElement(nsevt + "nmRazao", "Serplamed"));
            softwareHouse.Add(new XElement(nsevt + "nmCont", "Ivandro Lessing"));
            softwareHouse.Add(new XElement(nsevt + "telefone", "55996527853"));
            softwareHouse.Add(new XElement(nsevt + "email", "ivandro.lessing@outlook.com"));

            XElement infoComplementares = new XElement(nsevt + "infoComplementares");
            XElement situacaoPJ = new XElement(nsevt + "situacaoPJ");
            situacaoPJ.Add(new XElement(nsevt + "indSitPJ", "0"));
            infoComplementares.Add(situacaoPJ);

            infoCadastro.Add(contato);
            infoCadastro.Add(softwareHouse);
            infoCadastro.Add(infoComplementares);
            inclusao.Add(idePeriodo);
            inclusao.Add(infoCadastro);
            infoEmpregador.Add(inclusao);
            evtInfoEmpregador.Add(ideEvento);
            evtInfoEmpregador.Add(ideEmpregador);
            evtInfoEmpregador.Add(infoEmpregador);
            evento.Add(evtInfoEmpregador);


            XmlDocument xml = new XmlDocument();
            xml.LoadXml(evento.ToString(SaveOptions.DisableFormatting));
            string eventoAssinado = XmlSignatureHelper.SignXmlDoc(xml, certificado, 2).OuterXml;
            evento = XElement.Parse(eventoAssinado);

            xEvento.Add(evento);
            xEventos.Add(xEvento);
            xEnvioLoteEventos.Add(xIdeEmpregador);
            xEnvioLoteEventos.Add(xIdeTransmissor);
            xEnvioLoteEventos.Add(xEventos);
            xESocial.Add(xEnvioLoteEventos);

            return Ok(enviar(xESocial, certificado));
        }

        [HttpGet("enviar_1005")]
        public async System.Threading.Tasks.Task<IActionResult> enviar_1005()
        {
            var Id = $"ID{"1"}{agrat8.PadRight(14, '0')}{DateTime.Now.ToString("yyyyMMddHHmmss")}00001";

            XAttribute grupo = new XAttribute("grupo", "1");
            XNamespace ns = "http://www.esocial.gov.br/schema/lote/eventos/envio/v1_1_1";
            XElement xESocial = new XElement(ns + "eSocial");
            XElement xEnvioLoteEventos = new XElement(ns + "envioLoteEventos", grupo);
            XElement xIdeEmpregador = new XElement(ns + "ideEmpregador");
            xIdeEmpregador.Add(new XElement(ns + "tpInsc", "1"));
            xIdeEmpregador.Add(new XElement(ns + "nrInsc", agrat8));
            XElement xIdeTransmissor = new XElement(ns + "ideTransmissor");
            xIdeTransmissor.Add(new XElement(ns + "tpInsc", "1"));
            xIdeTransmissor.Add(new XElement(ns + "nrInsc", serplamed));
            XElement xEventos = new XElement(ns + "eventos");
            XAttribute id = new XAttribute("Id", Id);
            XElement xEvento = new XElement(ns + "evento", id);

            XNamespace nsevt = "http://www.esocial.gov.br/schema/evt/evtTabEstab/v02_05_00";
            XElement evento = new XElement(nsevt + "eSocial");
            XElement evtTabEstab = new XElement(nsevt + "evtTabEstab");
            evtTabEstab.Add(id);
            XElement ideEvento = new XElement(nsevt + "ideEvento");
            ideEvento.Add(new XElement(nsevt + "tpAmb", "2"));
            ideEvento.Add(new XElement(nsevt + "procEmi", "1"));
            ideEvento.Add(new XElement(nsevt + "verProc", "1.0"));
            XElement ideEmpregador = new XElement(nsevt + "ideEmpregador");
            ideEmpregador.Add(new XElement(nsevt + "tpInsc", "1"));
            ideEmpregador.Add(new XElement(nsevt + "nrInsc", agrat8));
            XElement infoEstab = new XElement(nsevt + "infoEstab");
            XElement inclusao = new XElement(nsevt + "inclusao");
            XElement ideEstab = new XElement(nsevt + "ideEstab");
            ideEstab.Add(new XElement(nsevt + "tpInsc", "1"));
            ideEstab.Add(new XElement(nsevt + "nrInsc", agrat));
            ideEstab.Add(new XElement(nsevt + "iniValid", "2017-01"));

            XElement dadosEstab = new XElement(nsevt + "dadosEstab");
            dadosEstab.Add(new XElement(nsevt + "cnaePrep", "7119704"));

            XElement aliqGilrat = new XElement(nsevt + "aliqGilrat");
            aliqGilrat.Add(new XElement(nsevt + "aliqRat", "1"));
            aliqGilrat.Add(new XElement(nsevt + "fap", "1"));
            aliqGilrat.Add(new XElement(nsevt + "aliqRatAjust", "1"));

            XElement infoTrab = new XElement(nsevt + "infoTrab");
            infoTrab.Add(new XElement(nsevt + "regPt", "2"));

            XElement infoApr = new XElement(nsevt + "infoApr");
            infoApr.Add(new XElement(nsevt + "contApr", "0"));

            infoTrab.Add(infoApr);
            dadosEstab.Add(aliqGilrat);
            dadosEstab.Add(infoTrab);
            inclusao.Add(ideEstab);
            inclusao.Add(dadosEstab);
            infoEstab.Add(inclusao);
            evtTabEstab.Add(ideEvento);
            evtTabEstab.Add(ideEmpregador);
            evtTabEstab.Add(infoEstab);
            evento.Add(evtTabEstab);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(evento.ToString(SaveOptions.DisableFormatting));
            string eventoAssinado = XmlSignatureHelper.SignXmlDoc(xml, certificado, 2).OuterXml;
            evento = XElement.Parse(eventoAssinado);

            xEvento.Add(evento);
            xEventos.Add(xEvento);
            xEnvioLoteEventos.Add(xIdeEmpregador);
            xEnvioLoteEventos.Add(xIdeTransmissor);
            xEnvioLoteEventos.Add(xEventos);
            xESocial.Add(xEnvioLoteEventos);

            return Ok(enviar(xESocial, certificado));
        }

        [HttpGet("enviar_1030")]
        public async System.Threading.Tasks.Task<IActionResult> enviar_1030()
        {
            var Id = $"ID{"1"}{agrat8.PadRight(14, '0')}{DateTime.Now.ToString("yyyyMMddHHmmss")}00001";

            XAttribute grupo = new XAttribute("grupo", "1");
            XNamespace ns = "http://www.esocial.gov.br/schema/lote/eventos/envio/v1_1_1";
            XElement xESocial = new XElement(ns + "eSocial");
            XElement xEnvioLoteEventos = new XElement(ns + "envioLoteEventos", grupo);
            XElement xIdeEmpregador = new XElement(ns + "ideEmpregador");
            xIdeEmpregador.Add(new XElement(ns + "tpInsc", "1"));
            xIdeEmpregador.Add(new XElement(ns + "nrInsc", agrat8));
            XElement xIdeTransmissor = new XElement(ns + "ideTransmissor");
            xIdeTransmissor.Add(new XElement(ns + "tpInsc", "1"));
            xIdeTransmissor.Add(new XElement(ns + "nrInsc", serplamed));
            XElement xEventos = new XElement(ns + "eventos");
            XAttribute id = new XAttribute("Id", Id);
            XElement xEvento = new XElement(ns + "evento", id);

            XNamespace nsevt = "http://www.esocial.gov.br/schema/evt/evtTabCargo/v02_05_00";
            XElement evento = new XElement(nsevt + "eSocial");
            XElement evtTabCargo = new XElement(nsevt + "evtTabCargo");
            evtTabCargo.Add(id);
            XElement ideEvento = new XElement(nsevt + "ideEvento");
            ideEvento.Add(new XElement(nsevt + "tpAmb", "2"));
            ideEvento.Add(new XElement(nsevt + "procEmi", "1"));
            ideEvento.Add(new XElement(nsevt + "verProc", "1.0"));
            XElement ideEmpregador = new XElement(nsevt + "ideEmpregador");
            ideEmpregador.Add(new XElement(nsevt + "tpInsc", "1"));
            ideEmpregador.Add(new XElement(nsevt + "nrInsc", agrat8));

            XElement infoCargo = new XElement(nsevt + "infoCargo");
            XElement inclusao = new XElement(nsevt + "inclusao");
            XElement ideCargo = new XElement(nsevt + "ideCargo");
            ideCargo.Add(new XElement(nsevt + "codCargo", "0050"));
            ideCargo.Add(new XElement(nsevt + "iniValid", "2017-01"));

            XElement dadosCargo = new XElement(nsevt + "dadosCargo");
            dadosCargo.Add(new XElement(nsevt + "nmCargo", "Cargo Serplamed"));
            dadosCargo.Add(new XElement(nsevt + "codCBO", "212405"));

            evtTabCargo.Add(ideEvento);
            evtTabCargo.Add(ideEmpregador);
            evtTabCargo.Add(infoCargo);
            infoCargo.Add(inclusao);
            inclusao.Add(ideCargo);
            inclusao.Add(dadosCargo);

            evento.Add(evtTabCargo);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(evento.ToString(SaveOptions.DisableFormatting));
            string eventoAssinado = XmlSignatureHelper.SignXmlDoc(xml, certificado, 2).OuterXml;
            evento = XElement.Parse(eventoAssinado);

            xEvento.Add(evento);
            xEventos.Add(xEvento);
            xEnvioLoteEventos.Add(xIdeEmpregador);
            xEnvioLoteEventos.Add(xIdeTransmissor);
            xEnvioLoteEventos.Add(xEventos);
            xESocial.Add(xEnvioLoteEventos);

            return Ok(enviar(xESocial, certificado));
        }

        [HttpGet("enviar_2200")]
        public async System.Threading.Tasks.Task<IActionResult> enviar_2200()
        {
            var Id = $"ID{"1"}{agrat8.PadRight(14, '0')}{DateTime.Now.ToString("yyyyMMddHHmmss")}00001";

            XAttribute grupo = new XAttribute("grupo", "2");
            XNamespace ns = "http://www.esocial.gov.br/schema/lote/eventos/envio/v1_1_1";
            XElement xESocial = new XElement(ns + "eSocial");
            XElement xEnvioLoteEventos = new XElement(ns + "envioLoteEventos", grupo);
            XElement xIdeEmpregador = new XElement(ns + "ideEmpregador");
            xIdeEmpregador.Add(new XElement(ns + "tpInsc", "1"));
            xIdeEmpregador.Add(new XElement(ns + "nrInsc", agrat8));
            XElement xIdeTransmissor = new XElement(ns + "ideTransmissor");
            xIdeTransmissor.Add(new XElement(ns + "tpInsc", "1"));
            xIdeTransmissor.Add(new XElement(ns + "nrInsc", serplamed));
            XElement xEventos = new XElement(ns + "eventos");
            XAttribute id = new XAttribute("Id", Id);
            XElement xEvento = new XElement(ns + "evento", id);

            XNamespace nsevt = "http://www.esocial.gov.br/schema/evt/evtAdmissao/v02_05_00";
            XElement evento = new XElement(nsevt + "eSocial");
            XElement evtAdmissao = new XElement(nsevt + "evtAdmissao");
            evtAdmissao.Add(id);
            XElement ideEvento = new XElement(nsevt + "ideEvento");
            ideEvento.Add(new XElement(nsevt + "indRetif", "1"));
            //ideEvento.Add(new XElement(nsevt + "nrRecibo", "1"));
            ideEvento.Add(new XElement(nsevt + "tpAmb", "2"));
            ideEvento.Add(new XElement(nsevt + "procEmi", "1"));
            ideEvento.Add(new XElement(nsevt + "verProc", "1.0"));
            XElement ideEmpregador = new XElement(nsevt + "ideEmpregador");
            ideEmpregador.Add(new XElement(nsevt + "tpInsc", "1"));
            ideEmpregador.Add(new XElement(nsevt + "nrInsc", agrat8));

            XElement trabalhador = new XElement(nsevt + "trabalhador");
            trabalhador.Add(new XElement(nsevt + "cpfTrab", "01590315081"));
            trabalhador.Add(new XElement(nsevt + "nisTrab", "16175057547"));
            trabalhador.Add(new XElement(nsevt + "nmTrab", "Ivandro Lessing"));
            trabalhador.Add(new XElement(nsevt + "sexo", "M"));
            trabalhador.Add(new XElement(nsevt + "racaCor", "1"));
            trabalhador.Add(new XElement(nsevt + "estCiv", "1"));
            trabalhador.Add(new XElement(nsevt + "grauInstr", "08"));
            trabalhador.Add(new XElement(nsevt + "indPriEmpr", "N"));

            XElement nascimento = new XElement(nsevt + "nascimento");
            nascimento.Add(new XElement(nsevt + "dtNascto", "1993-12-13"));
            nascimento.Add(new XElement(nsevt + "codMunic", "4309605"));
            nascimento.Add(new XElement(nsevt + "uf", "RS"));
            nascimento.Add(new XElement(nsevt + "paisNascto", "105"));
            nascimento.Add(new XElement(nsevt + "paisNac", "105"));

            XElement endereco = new XElement(nsevt + "endereco");
            XElement brasil = new XElement(nsevt + "brasil");
            brasil.Add(new XElement(nsevt + "tpLograd", "RES"));
            brasil.Add(new XElement(nsevt + "dscLograd", "Rua regina Viana"));
            brasil.Add(new XElement(nsevt + "nrLograd", "569"));
            brasil.Add(new XElement(nsevt + "cep", "98920000"));
            brasil.Add(new XElement(nsevt + "codMunic", "4309605"));
            brasil.Add(new XElement(nsevt + "uf", "RS"));

            XElement vinculo = new XElement(nsevt + "vinculo");
            vinculo.Add(new XElement(nsevt + "matricula", "0000070129"));
            //vinculo.Add(new XElement(nsevt + "matricula", "00004"));
            vinculo.Add(new XElement(nsevt + "tpRegTrab", "1"));
            vinculo.Add(new XElement(nsevt + "tpRegPrev", "1"));
            vinculo.Add(new XElement(nsevt + "cadIni", "S"));


            XElement infoRegimeTrab = new XElement(nsevt + "infoRegimeTrab");
            XElement infoCeletista = new XElement(nsevt + "infoCeletista");
            infoCeletista.Add(new XElement(nsevt + "dtAdm", "2017-01-01"));
            infoCeletista.Add(new XElement(nsevt + "tpAdmissao", "1"));
            infoCeletista.Add(new XElement(nsevt + "indAdmissao", "1"));
            infoCeletista.Add(new XElement(nsevt + "tpRegJor", "1"));
            infoCeletista.Add(new XElement(nsevt + "natAtividade", "1"));
            infoCeletista.Add(new XElement(nsevt + "cnpjSindCategProf", "90863663000122"));

            XElement FGTS = new XElement(nsevt + "FGTS");
            FGTS.Add(new XElement(nsevt + "opcFGTS", "1"));
            FGTS.Add(new XElement(nsevt + "dtOpcFGTS", "2017-01-01"));

            XElement infoContrato = new XElement(nsevt + "infoContrato");
            infoContrato.Add(new XElement(nsevt + "codCargo", "0050")); //S-1030.
            infoContrato.Add(new XElement(nsevt + "codCateg", "101"));

            XElement remuneracao = new XElement(nsevt + "remuneracao");
            remuneracao.Add(new XElement(nsevt + "vrSalFx", "9000"));
            remuneracao.Add(new XElement(nsevt + "undSalFixo", "5"));

            XElement duracao = new XElement(nsevt + "duracao");
            duracao.Add(new XElement(nsevt + "tpContr", "1"));

            XElement localTrabalho = new XElement(nsevt + "localTrabalho");
            XElement localTrabGeral = new XElement(nsevt + "localTrabGeral");
            localTrabGeral.Add(new XElement(nsevt + "tpInsc", "1"));
            localTrabGeral.Add(new XElement(nsevt + "nrInsc", agrat));

            XElement horContratual = new XElement(nsevt + "horContratual");
            horContratual.Add(new XElement(nsevt + "qtdHrsSem", "48"));
            horContratual.Add(new XElement(nsevt + "tpJornada", "9"));
            horContratual.Add(new XElement(nsevt + "dscTpJorn", "Jornada Variável"));
            horContratual.Add(new XElement(nsevt + "tmpParc", "0"));

            evtAdmissao.Add(ideEvento);
            evtAdmissao.Add(ideEmpregador);
            evtAdmissao.Add(trabalhador);
            trabalhador.Add(nascimento);
            trabalhador.Add(endereco);
            endereco.Add(brasil);
            evtAdmissao.Add(vinculo);
            vinculo.Add(infoRegimeTrab);
            infoRegimeTrab.Add(infoCeletista);
            infoCeletista.Add(FGTS);
            vinculo.Add(infoContrato);
            infoContrato.Add(remuneracao);
            infoContrato.Add(duracao);
            infoContrato.Add(localTrabalho);
            localTrabalho.Add(localTrabGeral);
            infoContrato.Add(horContratual);

            evento.Add(evtAdmissao);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(evento.ToString(SaveOptions.DisableFormatting));
            string eventoAssinado = XmlSignatureHelper.SignXmlDoc(xml, certificado, 2).OuterXml;
            evento = XElement.Parse(eventoAssinado);

            xEvento.Add(evento);
            xEventos.Add(xEvento);
            xEnvioLoteEventos.Add(xIdeEmpregador);
            xEnvioLoteEventos.Add(xIdeTransmissor);
            xEnvioLoteEventos.Add(xEventos);
            xESocial.Add(xEnvioLoteEventos);


            return Ok(enviar(xESocial, certificado));
        }

        public async System.Threading.Tasks.Task<IActionResult> enviar(XElement xESocial, X509Certificate2 certificado)
        {
            var client = new ESocialHomologacaoEnviar.ServicoEnviarLoteEventosClient();
            client.ClientCredentials.ClientCertificate.Certificate = certificado;
            await client.OpenAsync();
            var retornoEnvioXElement = await client.EnviarLoteEventosAsync(xESocial);
            await client.CloseAsync();

            XElement response = retornoEnvioXElement.Body.EnviarLoteEventosResult;
            eSocial eSocialResponse = XmlConverter.XmlToObject<eSocial>(response.ToString());
            if (eSocialResponse != null && eSocialResponse.retornoEnvioLoteEventos.status.cdResposta == 201)
            {
                string protocolo = eSocialResponse.retornoEnvioLoteEventos.dadosRecepcaoLote.protocoloEnvio;
                var clientConsultar = new ESocialHomologacaoConsultar.ServicoConsultarLoteEventosClient();
                clientConsultar.ClientCredentials.ClientCertificate.Certificate = certificado;
                await clientConsultar.OpenAsync();
                var retornoConsultaXElement = await clientConsultar.ConsultarLoteEventosAsync(XmlQuery(protocolo));
                await clientConsultar.CloseAsync();

                return Ok(retornoConsultaXElement.Body.ConsultarLoteEventosResult);
            }
            return Ok();
        }

        [HttpGet("consultar")]
        public async System.Threading.Tasks.Task<IActionResult> consultar()
        {
            X509Certificate2 certificado = Assinatura.GetCertiticate("");
            string protocolo = "1.2.201905.0000000000000801588";
            var clientConsultar = new ESocialHomologacaoConsultar.ServicoConsultarLoteEventosClient();
            clientConsultar.ClientCredentials.ClientCertificate.Certificate = certificado;
            await clientConsultar.OpenAsync();
            var retornoConsultaXElement = await clientConsultar.ConsultarLoteEventosAsync(XmlQuery(protocolo));
            await clientConsultar.CloseAsync();

            return Ok(retornoConsultaXElement.Body.ConsultarLoteEventosResult);
        }

        [HttpGet("requestAPI")]
        public async System.Threading.Tasks.Task<IActionResult> requestAPI()
        {

            var request = (HttpWebRequest)WebRequest.Create("https://esocial.agrat.com.br/eventos/postEvents");

            var postData = "{"
+ "	\"idESocial\": 0,"
+ "	\"TpEvento\": \"2245\","
+ "	\"DataEvento\": \"2019-06-04T11:00:00\","
+ "	\"NrInsc\": \"07131887000104\","
+ "	\"Empregador\": {"
+ "		\"NrInsc\": \"07131887000104\","
+ "		\"NrInscTransmissor\": \"07131887000104\","
+ "		\"idTpInsc\": 1,"
+ "		\"TpInsc\": null,"
+ "		\"RazaoSocial\": \"SERPLAMED\","
+ "		\"Transmitir\": true,"
+ "		\"EnviarEmail\": true,"
+ "		\"Emails\": \"ivandro.lessing@outlook.com\""
+ "	},"
+ "	\"Operacao\": \"I\","
+ "	\"StatusTransmissao\": \"P\","
+ "	\"ESocial2245\": {"
+ "		\"idESocial\": 0,"
+ "		\"CpfTrab\": \"01590315081\","
+ "		\"Funcionario\": {"
+ "			\"CpfTrab\": \"01590315081\","
+ "			\"Nome\": \"Ivandro Lessing\","
+ "			\"NisTrab\": \"16175057547\","
+ "			\"Matricula\": \"0000070129\","
+ "			\"CodCateg\": null,"
+ "			\"CategTrab\": null"
+ "		},"
+ "		\"codTreiCap\": \"3103\","
+ "		\"TreiCap\": null,"
+ "		\"obsTreiCap\": \"Observação\","
+ "		\"dtTreiCap\": \"2019-01-01T00:00:00\","
+ "		\"durTreiCap\": 2.00,"
+ "		\"modTreiCap\": \"1\","
+ "		\"tpTreiCap\": \"1\","
+ "		\"indTreinAnt\": \"N\","
+ "		\"ESocial2245IdeProfResp\": [{"
+ "			\"id\": 0,"
+ "			\"idESocial\": 0,"
+ "			\"cpfProf\": \"67207482035\","
+ "			\"nmProf\": \"Professor Jorge Camargo\","
+ "			\"tpProf\": 1,"
+ "			\"formProf\": \"Formação básica\","
+ "			\"codCBO\": \"342120\","
+ "			\"nacProf\": 1"
+ "		}]"
+ "	}"
+ "}";
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            //request.ContentType = "application/x-www-form-urlencoded";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return Ok();
        }

        private static XElement XmlQuery(string protocolo)
        {
            StringBuilder s = new StringBuilder();
            s.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\">");
            s.Append("<soap:Header/>");
            s.Append("<soap:Body>");
            s.Append("<eSocial xmlns=\"http://www.esocial.gov.br/schema/lote/eventos/envio/consulta/retornoProcessamento/v1_0_0\">");
            s.Append("<consultaLoteEventos>");
            s.Append($"<protocoloEnvio>{ protocolo }</protocoloEnvio>");
            s.Append("</consultaLoteEventos>");
            s.Append("</eSocial>");
            s.Append("</soap:Body>");
            s.Append("</soap:Envelope>");

            return XElement.Parse(s.ToString());
        }

    }
}
