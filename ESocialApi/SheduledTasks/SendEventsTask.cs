﻿using ESocialApi.Interfaces.ESocialTransmissao;
using ESocialApi.Services.Scheduler;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace ESocialApi.ScheduledTasks
{
    public class SendEventsTask : ScheduledProcessor
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public SendEventsTask(IServiceScopeFactory serviceScopeFactory) : base(serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override string Schedule => "*/1 * * * *";

        public override Task ProcessInScope(IServiceProvider serviceProvider)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                IPostEvents postEventsScoped = scope.ServiceProvider.GetRequiredService<IPostEvents>();

                //postEventsScoped.TransmitirEventos();
            }

            return Task.CompletedTask;
        }
    }
}
