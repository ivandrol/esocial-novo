﻿using System;
using System.Threading.Tasks;
using ESocialApi.Interfaces;
using ESocialApi.Services.Scheduler;
using Microsoft.Extensions.DependencyInjection;

namespace ESocialApi.ScheduledTasks
{
    public class RetriveEventsTask : ScheduledProcessor
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public RetriveEventsTask(IServiceScopeFactory serviceScopeFactory) : base(serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override string Schedule => "*/1 * * * *";

        public override Task ProcessInScope(IServiceProvider serviceProvider)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                IRetriveEvents scoped = scope.ServiceProvider.GetRequiredService<IRetriveEvents>();

                //scoped.GetAllEventsAsync();
            }

            return Task.CompletedTask;
        }
    }
}
