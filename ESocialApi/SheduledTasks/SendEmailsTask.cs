﻿using ESocialApi.Interfaces;
using ESocialApi.Interfaces.ESocialTransmissao;
using ESocialApi.Services.Scheduler;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace ESocialApi.SheduledTasks
{
    public class SendEmailsTask : ScheduledProcessor
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public SendEmailsTask(IServiceScopeFactory serviceScopeFactory) : base(serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        protected override string Schedule => "*/1 * * * *";

        public override Task ProcessInScope(IServiceProvider serviceProvider)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                IESocialService postEventsScoped = scope.ServiceProvider.GetRequiredService<IESocialService>();

                //postEventsScoped.EnviarEmailEventos();
            }

            return Task.CompletedTask;
        }

    }
}
