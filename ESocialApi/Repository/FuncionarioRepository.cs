﻿using ESocialApi.DB;
using ESocialApi.DB.Model;
using ESocialApi.Interfaces.Repository;

namespace ESocialApi.Repository
{
    public class FuncionarioRepository : Repository<Funcionario>, IFuncionarioRepository
    {
        public FuncionarioRepository(ApiContext context)
           : base(context)
        { }

        public ApiContext Context
        {
            get { return ApiContext as ApiContext; }
        }
    }
}
