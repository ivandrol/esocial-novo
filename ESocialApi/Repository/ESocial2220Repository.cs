﻿using ESocialApi.DB;
using ESocialApi.DB.Model;
using ESocialApi.Interfaces.Repository;

namespace ESocialApi.Repository
{
    public class ESocial2220Repository : Repository<ESocial2220>, IESocial2220Repository
    {
        public ESocial2220Repository(ApiContext context)
           : base(context)
        { }

        public ApiContext Context
        {
            get { return ApiContext as ApiContext; }
        }
    }
}
