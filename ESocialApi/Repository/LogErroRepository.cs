﻿using ESocialApi.DB;
using ESocialApi.DB.Model;
using ESocialApi.Interfaces.Repository;

namespace ESocialApi.Repository
{
    public class LogErroRepository : Repository<LogErro>, ILogErroRepository
    {
        public LogErroRepository(ApiContext context)
           : base(context)
        { }

        public ApiContext Context
        {
            get { return ApiContext as ApiContext; }
        }
    }
}
