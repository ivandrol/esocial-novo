﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ESocialApi.DB;
using ESocialApi.Interfaces.Repository;

namespace ESocialApi.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly ApiContext ApiContext;

        public Repository(ApiContext apiContext)
        {
            this.ApiContext = apiContext;
        }

        public TEntity Get(int id)
        {
            return ApiContext.Set<TEntity>().Find(id);
        }

        public TEntity Get(string id)
        {
            return ApiContext.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return ApiContext.Set<TEntity>().ToList();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return ApiContext.Set<TEntity>().Where(predicate);
        }

        public void Add(TEntity entity)
        {
            ApiContext.Set<TEntity>().Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            ApiContext.Set<TEntity>().AddRange(entities);
        }

        public void Remove(TEntity entity)
        {
            ApiContext.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            ApiContext.Set<TEntity>().RemoveRange(entities);
        }
    }
}
