﻿using ESocialApi.DB;
using ESocialApi.DB.Model;
using ESocialApi.Interfaces.Repository;

namespace ESocialApi.Repository
{
    public class ESocial2210Repository : Repository<ESocial2210>, IESocial2210Repository
    {
        public ESocial2210Repository(ApiContext context)
           : base(context)
        { }

        public ApiContext Context
        {
            get { return ApiContext as ApiContext; }
        }
    }
}
