﻿using ESocialApi.DB;
using ESocialApi.DB.Model;
using ESocialApi.Interfaces.Repository;

namespace ESocialApi.Repository
{
    public class EmpregadorRepository : Repository<Empregador>, IEmpregadorRepository
    {
        public EmpregadorRepository(ApiContext context)
           : base(context)
        { }

        public ApiContext Context
        {
            get { return ApiContext as ApiContext; }
        }
    }
}
