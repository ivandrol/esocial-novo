﻿using ESocialApi.DB;
using ESocialApi.Interfaces.Repository;

namespace ESocialApi.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApiContext _apiContext;

        public ILoteRepository Lote { get; private set; }
        public IESocialRepository ESocial { get; private set; }
        public IESocial2210Repository ESocial2210 { get; private set; }
        public IESocial2220Repository ESocial2220 { get; private set; }
        public IESocial2240Repository ESocial2240 { get; private set; }
        public IOcorrenciaRepository Ocorrencia { get; private set; }

        public ILogErroRepository LogErro  { get; private set; }
        public IEmpregadorRepository Empregador { get; private set; }
        public IFuncionarioRepository Funcionario { get; private set; }

        public UnitOfWork(ApiContext apiContext)
        {
            _apiContext = apiContext;
            Lote = new LoteRepository(_apiContext);
            ESocial = new ESocialRepository(_apiContext);
            ESocial2210 = new ESocial2210Repository(_apiContext);
            ESocial2220 = new ESocial2220Repository(_apiContext);
            ESocial2240 = new ESocial2240Repository(_apiContext);
            Ocorrencia = new OcorrenciaRepository(_apiContext);
            LogErro = new LogErroRepository(_apiContext);
            Empregador = new EmpregadorRepository(_apiContext);
            Funcionario = new FuncionarioRepository(_apiContext);

        }

        public int Complete()
        {
            return _apiContext.SaveChanges();
        }

        public void Dispose()
        {
            _apiContext.Dispose();
        }
    }
}
