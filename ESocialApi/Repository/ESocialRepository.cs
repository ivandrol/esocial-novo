﻿using ESocialApi.DB;
using ESocialApi.DB.Model;
using ESocialApi.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ESocialApi.Repository
{
    public class ESocialRepository : Repository<ESocial>, IESocialRepository
    {
        public ESocialRepository(ApiContext context)
           : base(context)
        { }

        public List<ESocial> ObtemEventosPorLote(int LoteId)
        {

            return Context.ESocial.Where(e => e.Lote.Id == LoteId).ToList();
        }

        public ESocial ObtemEventoPorId(string IdEvento)
        {
            return Context.ESocial.Include(e => e.ESocialProcessamento).Include(e => e.Empregador).FirstOrDefault(e => e.IdEvento == IdEvento);
        }


        public List<string> ObtemEmpresasComEventosNaoTrasmitidos()
        {
            return Context.ESocial
                .Where(e => e.Empregador.Transmitir && (e.StatusTransmissao == "P" || e.StatusTransmissao == "R"))
                .Select(e => e.NrInsc)
                .ToList();
        }

        public List<ESocial> ObtemEventosPorEmpresa(string cnpjs, string tpEvento, int ano, int mes, string status, string nomeFuncionario, string codAmb)
        {
            return Context.ESocial
                .Where(e => (!tpEvento.Equals("Todos") ? e.TpEvento == tpEvento : e.TpEvento != null) && cnpjs.Contains(e.NrInsc) && e.DataEvento.Year == ano && e.DataEvento.Month == mes && (!status.Equals("") ? e.StatusTransmissao == status : e.StatusTransmissao != null)
                && (e.ESocial2210.Funcionario.Nome.Contains(nomeFuncionario) || e.ESocial2220.Funcionario.Nome.Contains(nomeFuncionario) || e.ESocial2240.Funcionario.Nome.Contains(nomeFuncionario))
                && (e.ESocial2210.CodAmb.Contains(codAmb) || e.ESocial2210.CodAmb.Contains(codAmb))
                )

                .Include(e => e.Lote)
                .Include(e => e.ESocialProcessamento)
                .Include(e => e.Empregador)
                .ToList();
        }

        public List<ESocial> ObtemEventosNaoEnviadosPorEmpresa(string NrInscr)
        {
            return Context.ESocial
                 .Where(e => e.Empregador.NrInsc == NrInscr && (e.StatusTransmissao.Equals("P") || e.StatusTransmissao.Equals("R")))
               .Include(e => e.Empregador)
                   .ThenInclude(x => (x as Empregador).Transmissor)
               .Include(es => es.ESocial2210)
                   .ThenInclude(x => (x as ESocial2210).ESocial2210AgenteCausador)
               .Include(es => es.ESocial2210)
                   .ThenInclude(x => (x as ESocial2210).ESocial2210ParteAtingida)
               .Include(es => es.ESocial2210)
                   .ThenInclude(x => (x as ESocial2210).Funcionario)
               .Include(es => es.ESocial2220)
                   .ThenInclude(x => (x as ESocial2220).ESocial2220Exame)
               .Include(es => es.ESocial2220)
                   .ThenInclude(x => (x as ESocial2220).Funcionario)
               .Include(es => es.ESocial2240)
                   .ThenInclude(a => (a as ESocial2240).ESocial2240AtivPericInsal)
               .Include(es => es.ESocial2240)
                  .ThenInclude(b => (b as ESocial2240).ESocial2240InfoAmb)
               .Include(es => es.ESocial2240)
                   .ThenInclude(c => (c as ESocial2240).ESocial2240FatRisco)
                       .ThenInclude(d => (d as ESocial2240FatRisco).ESocial2240FatRiscoEpi)
               .Include(es => es.ESocial2240)
                   .ThenInclude(e => (e as ESocial2240).ESocial2240RespReg)
               .Include(es => es.ESocial2240)
                   .ThenInclude(x => (x as ESocial2240).Funcionario).ToList();
        }

        public List<ESocial> ObtemEventosNaoBaixadosPorEmpresa(string NrInscr)
        {
            return Context.ESocial
                .Where(e => e.NrInsc == NrInscr && (!e.StatusGet))
                .Include(e => e.Empregador)
                .ToList();
        }

        public List<ESocial> ObtemEventosNaoEnviadosEmailPorEmpresa()
        {
            return Context.ESocial
                .Where(e => !e.Empregador.Transmitir && e.Empregador.EnviarEmail && !e.StatusEmail)
                .Include(e => e.Empregador)
                .OrderBy(e => e.NrInsc)
                .ToList();
        }

        public ESocial GetESocial(int id)
        {
            return Context.ESocial
                .Include(e => e.Empregador)
                    .ThenInclude(x => (x as Empregador).Transmissor)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).ESocial2210AgenteCausador)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).ESocial2210ParteAtingida)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).Funcionario)
                .Include(es => es.ESocial2220)
                    .ThenInclude(x => (x as ESocial2220).ESocial2220Exame)
                .Include(es => es.ESocial2220)
                    .ThenInclude(x => (x as ESocial2220).Funcionario)
                .Include(es => es.ESocial2240)
                    .ThenInclude(a => (a as ESocial2240).ESocial2240AtivPericInsal)
                .Include(es => es.ESocial2240)
                   .ThenInclude(b => (b as ESocial2240).ESocial2240InfoAmb)
                .Include(es => es.ESocial2240)
                    .ThenInclude(c => (c as ESocial2240).ESocial2240FatRisco)
                        .ThenInclude(d => (d as ESocial2240FatRisco).ESocial2240FatRiscoEpi)
                .Include(es => es.ESocial2240)
                    .ThenInclude(e => (e as ESocial2240).ESocial2240RespReg)
                .Include(es => es.ESocial2240)
                    .ThenInclude(x => (x as ESocial2240).Funcionario)
                .FirstOrDefault(e => e.Id == id);
        }

        public ESocial GetESocialAll(int id)
        {
            return Context.ESocial
                .Include(e => e.Empregador)
                    .ThenInclude(x => (x as Empregador).Transmissor)
                .Include(e => e.Empregador)
                    .ThenInclude(x => (x as Empregador).TipoInscricao)
                .Include(e => e.Ocorencias)
                .Include(e => e.Lote)
                .Include(e => e.ESocialProcessamento)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).ESocial2210AgenteCausador)
                        .ThenInclude(y => (y as ESocial2210AgenteCausador).AgenteCausador)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).ESocial2210ParteAtingida)
                        .ThenInclude(y => (y as ESocial2210ParteAtingida).ParteAtingida)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).SituacaoGeradora)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).TipoAcidente)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).Pais)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).Funcionario)
                    .ThenInclude(x => (x as Funcionario).CategoriaTrabalhador)
                .Include(es => es.ESocial2220)
                    .ThenInclude(x => (x as ESocial2220).ESocial2220Exame)
                    .ThenInclude(x => (x as ESocial2220Exame).ProcedimentoRealizado)
                .Include(es => es.ESocial2220)
                    .ThenInclude(x => (x as ESocial2220).Funcionario)
                    .ThenInclude(x => (x as Funcionario).CategoriaTrabalhador)
                .Include(es => es.ESocial2240)
                    .ThenInclude(a => (a as ESocial2240).ESocial2240AtivPericInsal)
                        .ThenInclude(d => (d as ESocial2240AtivPericInsal).Atividade)
                .Include(es => es.ESocial2240)
                   .ThenInclude(b => (b as ESocial2240).ESocial2240InfoAmb)
                .Include(es => es.ESocial2240)
                    .ThenInclude(c => (c as ESocial2240).ESocial2240FatRisco)
                        .ThenInclude(d => (d as ESocial2240FatRisco).ESocial2240FatRiscoEpi)
                 .Include(es => es.ESocial2240)
                    .ThenInclude(c => (c as ESocial2240).ESocial2240FatRisco)
                        .ThenInclude(d => (d as ESocial2240FatRisco).FatorRisco)
                .Include(es => es.ESocial2240)
                    .ThenInclude(e => (e as ESocial2240).ESocial2240RespReg)
                .Include(es => es.ESocial2240)
                    .ThenInclude(x => (x as ESocial2240).Funcionario)
                    .ThenInclude(x => (x as Funcionario).CategoriaTrabalhador)
                .FirstOrDefault(e => e.Id == id);
        }

        public ESocialProcessamento UltimoProcessamento(int idESocial)
        {
            return Context.ESocialProcessamento.OrderByDescending(e => e.Id).FirstOrDefault(e => e.IdESocial == idESocial);
        }

        public ESocial GetEventoPendente(ESocial ESocial)
        {
            switch (ESocial.TpEvento)
            {
                case "2210":
                    return GetESocial2210(ESocial.NrInsc, ESocial.ESocial2210.CpfTrab, ESocial.ESocial2210.TpAcid, ESocial.ESocial2210.DtAcid);
                case "2220":
                    return GetESocial2220(ESocial.NrInsc, ESocial.ESocial2220.CpfTrab, ESocial.ESocial2220.TpExameOcup, ESocial.ESocial2220.DtAso);
                case "2240":
                    return GetESocial2240(ESocial.NrInsc, ESocial.ESocial2240.CpfTrab, ESocial.ESocial2240.DtIniCondicao);
            }
            return null;
        }

        private ESocial GetESocial2210(string NrInsc, string CpfTrab, string tpAcid, DateTime dtAcid)
        {
            return Context.ESocial
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).ESocial2210AgenteCausador)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).ESocial2210ParteAtingida)
                .Include(es => es.ESocial2210)
                    .ThenInclude(x => (x as ESocial2210).Funcionario)
                .Include(e => e.Empregador)
                    .ThenInclude(x => (x as Empregador).Transmissor)
                .FirstOrDefault(e => e.StatusTransmissao != "F" && e.NrInsc == NrInsc && e.ESocial2210.CpfTrab == CpfTrab && e.ESocial2210.TpAcid == tpAcid && e.ESocial2210.DtAcid == dtAcid);
        }

        private ESocial GetESocial2220(string NrInsc, string CpfTrab, int TpExameOcup, DateTime DtAso)
        {
            return Context.ESocial
                .Include(es => es.ESocial2220)
                    .ThenInclude(x => (x as ESocial2220).ESocial2220Exame)
                .Include(es => es.ESocial2220)
                    .ThenInclude(x => (x as ESocial2220).Funcionario)
                .Include(e => e.Empregador)
                    .ThenInclude(x => (x as Empregador).Transmissor)
                .FirstOrDefault(e => e.StatusTransmissao != "F" && e.NrInsc == NrInsc && e.ESocial2220.CpfTrab == CpfTrab && e.ESocial2220.TpExameOcup == TpExameOcup && e.ESocial2220.DtAso == DtAso);
        }

        private ESocial GetESocial2240(string NrInsc, string CpfTrab, DateTime DtIniCondicao)
        {
            return Context.ESocial
                .Include(es => es.ESocial2240)
                    .ThenInclude(a => (a as ESocial2240).ESocial2240AtivPericInsal)
                .Include(es => es.ESocial2240)
                   .ThenInclude(b => (b as ESocial2240).ESocial2240InfoAmb)
                .Include(es => es.ESocial2240)
                    .ThenInclude(c => (c as ESocial2240).ESocial2240FatRisco)
                        .ThenInclude(d => (d as ESocial2240FatRisco).ESocial2240FatRiscoEpi)
                .Include(es => es.ESocial2240)
                    .ThenInclude(e => (e as ESocial2240).ESocial2240RespReg)
                .Include(es => es.ESocial2240)
                    .ThenInclude(x => (x as ESocial2240).Funcionario)
                .Include(e => e.Empregador)
                    .ThenInclude(x => (x as Empregador).Transmissor)
                .FirstOrDefault(e => e.StatusTransmissao != "F" && e.NrInsc == NrInsc && e.ESocial2240.CpfTrab == CpfTrab && e.ESocial2240.DtIniCondicao == DtIniCondicao);
        }

        public string GetRecibo(ESocial ESocial)
        {
            try
            {
                switch (ESocial.TpEvento)
                {
                    case "2210":
                        return Context.ESocial.Include(es => es.ESocial2210).FirstOrDefault(e => e.StatusTransmissao == "F" && e.Operacao == "I" && e.NrRecibo != null && e.NrInsc == ESocial.NrInsc && e.ESocial2210.CpfTrab == ESocial.ESocial2210.CpfTrab && e.ESocial2210.TpAcid == ESocial.ESocial2210.TpAcid && e.ESocial2210.DtAcid == ESocial.ESocial2210.DtAcid).NrRecibo;
                    case "2220":
                        return Context.ESocial.Include(es => es.ESocial2220).FirstOrDefault(e => e.StatusTransmissao == "F" && e.Operacao == "I" && e.NrRecibo != null && e.NrInsc == ESocial.NrInsc && e.ESocial2220.CpfTrab == ESocial.ESocial2220.CpfTrab && e.ESocial2220.TpExameOcup == ESocial.ESocial2220.TpExameOcup && e.ESocial2220.DtAso == ESocial.ESocial2220.DtAso).NrRecibo;
                    case "2240":
                        return Context.ESocial.Include(es => es.ESocial2240).FirstOrDefault(e => e.StatusTransmissao == "F" && e.Operacao == "I" && e.NrRecibo != null && e.NrInsc == ESocial.NrInsc && e.ESocial2240.CpfTrab == ESocial.ESocial2240.CpfTrab && e.ESocial2240.DtIniCondicao == ESocial.ESocial2240.DtIniCondicao).NrRecibo;
                }
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public ApiContext Context
        {
            get { return ApiContext as ApiContext; }
        }
    }
}
