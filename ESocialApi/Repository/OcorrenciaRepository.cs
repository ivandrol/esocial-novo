﻿using ESocialApi.DB;
using ESocialApi.DB.Model;
using ESocialApi.Interfaces.Repository;
using System.Collections.Generic;
using System.Linq;

namespace ESocialApi.Repository
{
    public class OcorrenciaRepository : Repository<Ocorrencia>, IOcorrenciaRepository
    {
        public OcorrenciaRepository(ApiContext context)
            : base(context)
        { }

        public void DeleteOcorrencias(int idESocial)
        {
            List<Ocorrencia> ocorrencias = Context.Ocorrencia.Where(e => e.ESocial.Id == idESocial).ToList();
            for (int i = 0; i < ocorrencias.Count; i++)
            {
                Context.Ocorrencia.Remove(ocorrencias[i]);
                Context.SaveChanges();
            }
        }
        public ApiContext Context
        {
            get { return ApiContext as ApiContext; }
        }
    }
}
