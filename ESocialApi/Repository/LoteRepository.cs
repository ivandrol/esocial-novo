﻿using ESocialApi.DB;
using ESocialApi.DB.Model;
using ESocialApi.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ESocialApi.Repository
{
    public class LoteRepository : Repository<Lote>, ILoteRepository
    {
        public LoteRepository(ApiContext context)    
            : base(context)
        { }

        public Lote GetLote(string lote)
        {
            return Context.Lote.FirstOrDefault(l => l.NLote == lote);
        }

        public List<Lote> GetSendedLots()
        {
            return Context.Lote
                .Include(l => l.ESocial)
                    .ThenInclude(l => (l as ESocial).Empregador)
                        .ThenInclude(l => (l as Empregador).Transmissor)
                .Where(l => l.CdRespostaConsulta != 201 && l.ProtocoloEnvio != null && l.ESocial.Count > 0).ToList();
        }

        public ApiContext Context
        {
            get { return ApiContext as ApiContext; }
        }
    }
}
