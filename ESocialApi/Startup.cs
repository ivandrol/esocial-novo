﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc.Formatters;
using System;
using Microsoft.AspNetCore.Rewrite;
using ESocialApi.Helper.Xml;
using Microsoft.Extensions.Logging;
using ESocialApi.Interfaces;
using ESocialApi.Repository;
using ESocialApi.Interfaces.Repository;
using ESocialApi.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using ESocialApi.Interfaces.ESocialTransmissao;
using ESocialApi.Services;
using ESocialApi.ScheduledTasks;
using ESocialApi.Services.ESocialServices;
using ESocialApi.SheduledTasks;

namespace ESocialApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            var config = new NLog.Config.LoggingConfiguration();

            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = "file.txt" };
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            NLog.LogManager.Configuration = config;

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true;
                options.ReturnHttpNotAcceptable = true;
                options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
                options.InputFormatters.Add(new XmlSerializerInputFormatter(options));
                options.OutputFormatters.Add(new XmlOutputFormatter());
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<ApiContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("DefaultConnection")));

            // Register application services
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IRetriveEvents, RetriveEvents>();
            services.AddScoped<ILoteRepository, LoteRepository>();
            services.AddScoped<IESocialRepository, ESocialRepository>();
            services.AddScoped<IOcorrenciaRepository, OcorrenciaRepository>();
            services.AddScoped<ILogErroRepository, LogErroRepository>();

            services.AddScoped<ICreateLotEvents, CreateLotEvents>();
            services.AddScoped<IESocialService, ESocialService>();
            services.AddScoped<IQueryESocial, QueryESocial>();
            services.AddScoped<IPostEvents, PostEvents>();

            //Scheduled servives
            services.AddSingleton<IHostedService, RetriveEventsTask>();
            services.AddSingleton<IHostedService, SendEventsTask>();
            services.AddSingleton<IHostedService, SendEmailsTask>();

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new RequireHttpsAttribute());
            });

            services.AddHsts(options =>
            {
                options.Preload = true;
                options.IncludeSubDomains = true;
                options.MaxAge = TimeSpan.FromDays(60);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            //if (env.IsDevelopment())
            //{
                app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseHsts();
            //}

            loggerFactory
                .AddConsole()
                .AddDebug();

            var options = new RewriteOptions()
                .AddRedirectToHttpsPermanent();
            app.UseRewriter(options);

            app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseMvc();

        }
    }
}
