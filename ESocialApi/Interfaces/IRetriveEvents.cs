﻿using System.Threading.Tasks;

namespace ESocialApi.Interfaces
{
    public interface IRetriveEvents
    {
        void GetAllEventsAsync();
        Task GetS1060();
        Task GetS2220();
        Task GetS2240();
        Task GetS2245();
    }
}
