﻿using System.Threading;
using System.Threading.Tasks;

namespace ESocialApi.Interfaces
{
    public interface IScheduledTask
    {
        string Schedule { get; }
        Task ExecuteAsync(CancellationToken cancellationToken);
    }
}
