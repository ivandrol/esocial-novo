﻿using ESocialApi.DB.Model;
using System.Collections;

namespace ESocialApi.Interfaces.ESocialTransmissao
{
    public interface IQueryESocial
    {
        void QueryEvents();
        void QueryEvents(Transmissor Transmissor, Hashtable lotes);
    }
}
