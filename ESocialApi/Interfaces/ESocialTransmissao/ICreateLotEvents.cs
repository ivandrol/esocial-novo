﻿using ESocialApi.DB.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ESocialApi.Interfaces.ESocialTransmissao
{
    public interface ICreateLotEvents
    {
        Task<Hashtable> CreateEventLot(List<ESocial> EventList, int grupo);
        string CreateEvent(string tpEvento, string Token, int grupo);
        XElement GetXmlEvent(ESocial ESocial);
        string GetXmlEvent(int idESocial, string tpEvento);
        XElement CreateEvent2210(ESocial ESocial, ESocial2210 ESocial2210, string Id, X509Certificate2 certificado);
        XElement CreateEvent2220(ESocial ESocial, ESocial2220 ESocial2220, string Id, X509Certificate2 certificado);
        XElement CreateEvent2240(ESocial ESocial, ESocial2240 ESocial2240, string Id, X509Certificate2 certificado);
    }
}
