﻿using ESocialApi.DB.Model;
using System.Collections;
using System.Threading.Tasks;

namespace ESocialApi.Interfaces.ESocialTransmissao
{
    public interface IPostEvents
    {
        bool TransmitirLoteEventos(Hashtable Events, Transmissor Transmissor);
        void TransmitirEventos();
    }
}
