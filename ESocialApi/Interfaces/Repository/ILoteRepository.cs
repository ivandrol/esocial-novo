﻿using ESocialApi.DB.Model;
using System.Collections.Generic;

namespace ESocialApi.Interfaces.Repository
{
    public interface ILoteRepository : IRepository<Lote>
    {
        List<Lote> GetSendedLots();
        Lote GetLote(string lote);
    }
}
