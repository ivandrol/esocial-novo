﻿using ESocialApi.DB.Model;

namespace ESocialApi.Interfaces.Repository
{
    public interface IOcorrenciaRepository : IRepository<Ocorrencia>
    {
        void DeleteOcorrencias(int idESocial);
    }
}
