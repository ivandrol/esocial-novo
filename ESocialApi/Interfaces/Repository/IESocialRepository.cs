﻿using ESocialApi.DB.Model;
using System;
using System.Collections.Generic;

namespace ESocialApi.Interfaces.Repository
{
    public interface IESocialRepository : IRepository<ESocial>
    {
        List<ESocial> ObtemEventosPorLote(int loteId);
        ESocial ObtemEventoPorId(string eventoId);
        List<string> ObtemEmpresasComEventosNaoTrasmitidos();
        List<ESocial> ObtemEventosNaoEnviadosPorEmpresa(string NrInscr);
        List<ESocial> ObtemEventosNaoBaixadosPorEmpresa(string NrInscr);
        List<ESocial> ObtemEventosNaoEnviadosEmailPorEmpresa();
        List<ESocial> ObtemEventosPorEmpresa(string cnpjs, string tpEvento, int ano, int mes, string status, string nomeFuncionario, string codAmb);
        ESocial GetESocial(int id);
        ESocial GetESocialAll(int id);
        ESocial GetEventoPendente(ESocial ESocial);
        string GetRecibo(ESocial ESocial);
        ESocialProcessamento UltimoProcessamento(int idESocial);
    }
}
