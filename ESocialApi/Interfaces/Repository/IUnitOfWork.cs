﻿using System;

namespace ESocialApi.Interfaces.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        ILoteRepository Lote { get; }
        IESocialRepository ESocial { get; }
        IESocial2210Repository ESocial2210 { get; }
        IESocial2220Repository ESocial2220 { get; }
        IESocial2240Repository ESocial2240 { get; }
        IOcorrenciaRepository Ocorrencia { get; }
        ILogErroRepository LogErro { get; }
        IEmpregadorRepository Empregador { get; }
        IFuncionarioRepository Funcionario { get; }

        int Complete();
    }
}
