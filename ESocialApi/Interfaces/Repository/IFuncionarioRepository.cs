﻿using ESocialApi.DB.Model;

namespace ESocialApi.Interfaces.Repository
{
    public interface IFuncionarioRepository : IRepository<Funcionario>
    {
    }
}
