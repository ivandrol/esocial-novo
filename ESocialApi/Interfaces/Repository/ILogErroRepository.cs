﻿using ESocialApi.DB.Model;

namespace ESocialApi.Interfaces.Repository
{
    public interface ILogErroRepository : IRepository<LogErro>
    {
    }
}
