﻿using ESocialApi.DB.Model;

namespace ESocialApi.Interfaces.Repository
{
    public interface IEmpregadorRepository : IRepository<Empregador>
    {
    }
}
