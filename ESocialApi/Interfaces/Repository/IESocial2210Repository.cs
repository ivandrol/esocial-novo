﻿using ESocialApi.DB.Model;

namespace ESocialApi.Interfaces.Repository
{
    public interface IESocial2210Repository : IRepository<ESocial2210>
    {
    }
}
