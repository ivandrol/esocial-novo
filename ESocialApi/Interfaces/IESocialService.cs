﻿using ESocialApi.DB.Model;

namespace ESocialApi.Interfaces
{
    public interface IESocialService
    {
        int GenerateEvents(ESocial obj);
        void EnviarEmailEventos();
    }
}
