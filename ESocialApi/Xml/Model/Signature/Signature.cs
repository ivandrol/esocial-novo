﻿namespace ESocialApi.Xml.Model.Signature
{
    public class Signature
    {
        public SignedInfo SignedInfo;
        public string SignatureValue;
        public KeyInfo KeyInfo;

        public Signature()
        {

        }

        public Signature(SignedInfo SignedInfo, string SignatureValue, KeyInfo KeyInfo)
        {
            this.SignedInfo = SignedInfo;
            this.KeyInfo = KeyInfo;
            this.SignatureValue = SignatureValue;
        }
    }
}
