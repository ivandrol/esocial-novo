﻿namespace ESocialApi.Xml.Model.Signature
{
    public class SignedInfo
    {
        public CanonicalizationMethod CanonicalizationMethod;
        public SignatureMethod SignatureMethod;
        public Reference Reference;

        public SignedInfo()
        {

        }

        public SignedInfo(CanonicalizationMethod CanonicalizationMethod, SignatureMethod SignatureMethod, Reference Reference)
        {
            this.CanonicalizationMethod = CanonicalizationMethod;
            this.SignatureMethod = SignatureMethod;
            this.Reference = Reference;
        }
    }
}
