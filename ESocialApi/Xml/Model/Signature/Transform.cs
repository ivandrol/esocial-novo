﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.Signature
{
    public class Transform
    {
        [XmlAttribute]
        public string Algorithm { get; set; }

        public Transform() { 
}

        public Transform(string Algorithm)
        {
            this.Algorithm = Algorithm;
        }
    }
}
