﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.Signature
{
    public class DigestMethod
    {
        [XmlAttribute]
        public string Algorithm { get; set; }

        public DigestMethod()
        {

        }

        public DigestMethod(string Algorithm)
        {
            this.Algorithm = Algorithm;
        }
    }
}
