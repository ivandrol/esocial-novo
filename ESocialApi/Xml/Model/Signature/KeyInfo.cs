﻿namespace ESocialApi.Xml.Model.Signature
{
    public class KeyInfo
    {
        public X509Data X509Data;

        public KeyInfo()
        {

        }

        public KeyInfo(X509Data X509Data)
        {
            this.X509Data = X509Data;
        }
    }
}
