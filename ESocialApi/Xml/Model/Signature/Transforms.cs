﻿
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.Signature
{
    public class Transforms
    {
        [XmlElement("Transform")]
        public List<Transform> transforms { get; set; }

        public Transforms()
        {

        }

        public Transforms(List<Transform> transforms)
        {
            this.transforms = transforms;
        }
    }
}
