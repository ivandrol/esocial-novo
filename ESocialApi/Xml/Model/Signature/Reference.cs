﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.Signature
{
    public class Reference
    {
        [XmlAttribute]
        public string URI { get; set; }
        public Transforms Transforms;
        public DigestMethod DigestMethod;
        public string DigestValue;

        public Reference()
        {

        }

        public Reference(string URI, Transforms Transforms, DigestMethod DigestMethod, string DigestValue)
        {
            this.URI = URI;
            this.Transforms = Transforms;
            this.DigestMethod = DigestMethod;
            this.DigestValue = DigestValue;
        }
    }
}
