﻿
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.Signature
{
    public class SignatureMethod
    {
        [XmlAttribute]
        public string Algorithm { get; set; }

        public SignatureMethod()
        {

        }

        public SignatureMethod(string Algorithm)
        {
            this.Algorithm = Algorithm;
        }
    }
}
