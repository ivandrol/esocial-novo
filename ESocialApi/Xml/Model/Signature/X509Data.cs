﻿namespace ESocialApi.Xml.Model.Signature
{
    public class X509Data
    {
        public string X509Certificate;
        
        public X509Data()
        {

        }

        public X509Data(string X509Certificate)
        {
            this.X509Certificate = X509Certificate;
        }
    }
}
