﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.Signature
{
    public class CanonicalizationMethod
    {
        [XmlAttribute]
        public string Algorithm { get; set; }

        public CanonicalizationMethod()
        {

        }

        public CanonicalizationMethod(string Algorithm)
        {
            this.Algorithm = Algorithm;
        }
    }
}
