﻿namespace ESocialApi.Xml.Model
{
    public class ideTransmissor
    {
        public int tpInsc { get; set; }
        public string nrInsc { get; set; }

        public ideTransmissor() {}

        public ideTransmissor(int tpInsc, string nrInsc)
        {
            this.tpInsc = tpInsc;
            this.nrInsc = nrInsc;
        }
    }

}