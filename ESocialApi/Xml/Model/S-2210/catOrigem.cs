﻿namespace ESocialApi.Xml.Model.S_2210
{
    public class catOrigem
    {

        public string nrRecCatOrig { get; set; }

        public catOrigem()
        {

        }

        public catOrigem(string nrRecCatOrig)
        {
            this.nrRecCatOrig = nrRecCatOrig;
        }
    }
}
