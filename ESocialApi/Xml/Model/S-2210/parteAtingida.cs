﻿namespace ESocialApi.Xml.Model.S_2210
{
    public class parteAtingida
    {
        public string codParteAting { get; set; }
        public int lateralidade { get; set; }

        public parteAtingida(string codParteAting, int lateralidade)
        {
            this.lateralidade = lateralidade;
            this.codParteAting = codParteAting;
        }

        public parteAtingida()
        {

        }
    }
}
