﻿namespace ESocialApi.Xml.Model.S_2210
{
    public class localAcidente
    {

        public int tpLocal { get; set; }
        public string dscLocal { get; set; }
        public string codAmb { get; set; }
        public string tpLograd { get; set; }
        public string dscLograd { get; set; }
        public string nrLograd { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string cep { get; set; }
        public string codMunic { get; set; }
        public string uf { get; set; }
        public string pais { get; set; }
        public string codPostal { get; set; }
        public ideLocalAcid ideLocalAcid { get; set; }

        public localAcidente()
        {

        }
    }
}
