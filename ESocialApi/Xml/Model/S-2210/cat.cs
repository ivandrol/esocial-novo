﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.S_2210
{
    public class cat
    {
        public string dtAcid { get; set; }
        public string tpAcid { get; set; }
        public string hrAcid { get; set; }
        public string hrsTrabAntesAcid { get; set; }
        public int tpCat { get; set; }
        public string indCatObito { get; set; }
        public string dtObito { get; set; }
        public string indComunPolicia { get; set; }
        public string codSitGeradora { get; set; }
        public int iniciatCAT { get; set; }
        public string obsCAT { get; set; }

        public localAcidente localAcidente { get; set; }
        [XmlElement("parteAtingida")]
        public List<parteAtingida> parteAtingida { get; set; }
        [XmlElement("agenteCausador")]
        public List<agenteCausador> agenteCausador { get; set; }
        public atestado atestado { get; set; }
        public catOrigem catOrigem { get; set; }


        public cat()
        {
        }
    }
}
