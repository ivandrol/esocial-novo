﻿namespace ESocialApi.Xml.Model.S_2210
{
    public class agenteCausador
    {

        public string codAgntCausador { get; set; }

        public agenteCausador(string codAgntCausador)
        {
            this.codAgntCausador = codAgntCausador;
        }

        public agenteCausador()
        {

        }
    }
}
