﻿namespace ESocialApi.Xml.Model.S_2210
{
    public class emitente
    {
        public string nmEmit { get; set; }
        public string ideOC { get; set; }
        public string nrOC { get; set; }
        public string ufOC { get; set; }

        public emitente()
        {

        }
    }
}
