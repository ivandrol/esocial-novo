﻿using System;

namespace ESocialApi.Xml.Model.S_2210
{
    public class atestado
    {
        public string codCNES { get; set; }
        public string dtAtendimento { get; set; }
        public string hrAtendimento { get; set; }
        public string indInternacao { get; set; }
        public string durTrat { get; set; }
        public string indAfast { get; set; }
        public string dscLesao { get; set; }
        public string dscCompLesao { get; set; }
        public string diagProvavel { get; set; }
        public string codCID { get; set; }
        public string observacao { get; set; }
        public emitente emitente { get; set; }

        public atestado()
        {

        }

    }
}
