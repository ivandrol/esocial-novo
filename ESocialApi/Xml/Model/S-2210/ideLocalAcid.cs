﻿namespace ESocialApi.Xml.Model.S_2210
{
    public class ideLocalAcid
    {
        public string tpInsc { get; set; }
        public string nrInsc { get; set; }

        public ideLocalAcid()
        {

        }

        public ideLocalAcid(string tpInsc, string nrInsc)
        {
            this.tpInsc = tpInsc;
            this.nrInsc = nrInsc;
        }
    }
}
