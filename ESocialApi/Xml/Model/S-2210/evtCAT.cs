﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.S_2210
{
    public class evtCAT
    {
        [XmlAttribute("Id")]
        public string Id { get; set; }

        public ideEvento ideEvento { get; set; }
        public ideEmpregador ideEmpregador { get; set; }
        public ideVinculo ideVinculo { get; set; }
        public cat cat { get; set; }


        public evtCAT()
        {

        }
    }
}
