﻿namespace ESocialApi.Xml.Model.S_2210
{
    public class eSocial2210
    {

        public evtCAT evtCAT { get; set; }

        public eSocial2210(evtCAT evtCAT)
        {
            this.evtCAT = evtCAT;
        }

        public eSocial2210()
        {

        }
    }
}
