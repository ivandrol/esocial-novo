﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model
{
    [XmlRoot("eventos")]
    public class eventos
    {
        [XmlElement("evento")]
        public List<evento> ListaEventos { get; set; }

        public eventos() { }

        public eventos(List<evento> listaEventos)
        {
            ListaEventos = listaEventos;
        }
    }
}