﻿namespace ESocialApi.Xml.Model
{
    public class ideEvento
    {
        public string indRetif { get; set; }
        public string nrRecibo { get; set; }
        public int tpAmb { get; set; }
        public int procEmi { get; set; }
        public string verProc { get; set; }

        public ideEvento() { }

        public ideEvento(int tpAmb, int procEmi, string verProc)
        {
            this.tpAmb = tpAmb;
            this.procEmi = procEmi;
            this.verProc = verProc;
        }

        public ideEvento(string indRetif, int tpAmb, int procEmi, string verProc)
        {
            this.indRetif = indRetif;
            this.tpAmb = tpAmb;
            this.procEmi = procEmi;
            this.verProc = verProc;
        }

        public ideEvento(string indRetif, string nrRecibo, int tpAmb, int procEmi, string verProc)
        {
            this.indRetif = indRetif;
            this.nrRecibo = nrRecibo;
            this.tpAmb = tpAmb;
            this.procEmi = procEmi;
            this.verProc = verProc;
        }
    }
}