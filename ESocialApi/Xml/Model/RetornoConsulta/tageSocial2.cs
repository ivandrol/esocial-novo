﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tageSocial2
    {
        [XmlElement("retornoEvento")]
        public tagRetornoEvento2 tagRetornoEvento2 { get; set; }
    }
}
