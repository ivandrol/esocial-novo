﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tagRetornoProcessamentoLoteEventos
    {
        [XmlElement("retornoEventos")]
        public tagRetornoEventos tagRetornoEventos { get; set; }

        [XmlElement("status")]
        public tagStatus tagStatus { get; set; }
    }
}
