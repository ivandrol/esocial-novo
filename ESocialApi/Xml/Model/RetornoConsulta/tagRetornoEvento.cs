﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tagRetornoEvento
    {
        [XmlElement("eSocial", Namespace = "http://www.esocial.gov.br/schema/evt/retornoEvento/v1_2_1")]
        public tageSocial2 tageSocial2 { get; set; }
    }
}
