﻿using ESocialApi.Xml.Model.RetornoConsulta;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tagRetornoEvento2
    {
        [XmlAttribute("Id")]    
        public string Id { get; set; }

        [XmlElement("recepcao")]
        public tagRecepcao tagRecepcao { get; set; }

        [XmlElement("processamento")]
        public tagProcessamento tagProcessamento { get; set; }

        [XmlElement("recibo")]
        public tagRecibo recibo { get; set; }
    }
}
