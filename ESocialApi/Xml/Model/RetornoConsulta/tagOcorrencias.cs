﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tagOcorrencias
    {
        [XmlElement("ocorrencia")]
        public List<tagOcorrencia> tagOcorrencia { get; set; }
    }
}