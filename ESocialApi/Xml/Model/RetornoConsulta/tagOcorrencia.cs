﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tagOcorrencia
    {
        [XmlIgnore]
        public int Id { get; set; }

        public string tipo { get; set; }
        public string codigo { get; set; }
        public string descricao { get; set; }
        public string localizacao { get; set; }
    }
}