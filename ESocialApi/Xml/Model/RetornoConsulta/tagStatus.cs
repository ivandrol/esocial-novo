﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tagStatus
    {
        public int cdResposta { get; set; }
        public string descResposta { get; set; }
        [XmlElement("ocorrencias")]
        public tagOcorrencias tagOcorrencias { get; set; }
    }
}
