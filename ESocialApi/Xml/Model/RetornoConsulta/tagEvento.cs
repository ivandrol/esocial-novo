﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tagEvento
    {
        [XmlAttribute("Id")]
        public string Id { get; set; }

        [XmlElement("retornoEvento")]
        public tagRetornoEvento tagRetornoEvento { get; set; }
    }
}
