﻿using System.Xml.Serialization;
namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tagProcessamento
    {
        public string cdResposta { get; set; }
        public string descResposta { get; set; }
        public string versaoAppProcessamento { get; set; }
        public string dhProcessamento { get; set; }

        [XmlElement("ocorrencias")]
        public tagOcorrencias tagOcorrencias { get; set; }
    }
}