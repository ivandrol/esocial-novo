﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    [XmlRoot("eSocial", Namespace="http://www.esocial.gov.br/schema/lote/eventos/envio/retornoProcessamento/v1_3_0")]
    public class tageSocial
    {
        [XmlElement("retornoProcessamentoLoteEventos")]
        public tagRetornoProcessamentoLoteEventos tagRetornoProcessamentoLoteEventos { get; set; }
    }
}
