﻿namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tagRecepcao
    {
        public int tpAmb { get; set; }
        public string dhRecepcao { get; set; }
        public string versaoAppRecepcao { get; set; }
        public string protocoloEnvioLote { get; set; }
    }
}