﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoConsulta_New
{
    public class tagRetornoEventos
    {
        [XmlElement("evento")]
        public List<tagEvento> tagEvento { get; set; }
    }
}
