﻿namespace ESocialApi.Xml.Model
{
    public class ideVinculo
    {
        public string cpfTrab { get; set; }

        //Validação: O preenchimento é obrigatório, exceto se o código de categoria do trabalhador for igual a[901, 903, 904
        public string nisTrab { get; set; }

        //Se o trabalhador não está sujeito a esse evento(S-2200), a matrícula não deve ser informada, porém, o trabalhador deve ter sido registrado como TSVE - Trabalhador Sem Vínculo de Emprego/Estatutário, através do evento S-2300.
        public string matricula { get; set; }

        //Validação: Só informar se o trabalhador tiver sido registrado como TSVE - Trabalhador Sem Vínculo de Emprego/Estatutário, através do evento S-
        public string codCateg { get; set; }

        public ideVinculo() { }

        public ideVinculo(string cpfTrab, string matricula)
        {
            this.cpfTrab = cpfTrab;
            this.matricula = matricula;
        }
    }

}