﻿
namespace ESocialApi.Xml.Model.S_2240
{
    public class infoAmb
    {
        public string localAmb { get; set; }
        public string dscSetor { get; set; }
        public string tpInsc { get; set; }
        public string nrInsc { get; set; }

        public infoAmb(){ }

        public infoAmb(string localAmb, string dscSetor, string tpInsc, string nrInsc)
        {
            this.localAmb = localAmb;
            this.dscSetor = dscSetor;
            this.tpInsc = tpInsc;
            this.nrInsc = nrInsc;
        }
    }
}