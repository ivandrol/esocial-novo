﻿namespace ESocialApi.Xml.Model.S_2240
{
    public class eSocial2240
    {
        public evtExpRisco evtExpRisco { get; set; }

        public eSocial2240() { }

        public eSocial2240(evtExpRisco evtExpRisco)
        {
            this.evtExpRisco = evtExpRisco;
        }
    }
}