﻿
namespace ESocialApi.Xml.Model.S_2240
{
    public class obs
    {
        //Descrição da metodologia utilizada para o levantamento dos riscos ergonômicos. Validação: Preenchimento obrigatório se {obsCompl} não for informado.
        public string metErg { get; set; }

        //Observação(ões) complementar(es) referente(s) a registros ambientais. Validação: Preenchimento obrigatório se {metErg} não for informado.
        public string obsCompl { get; set; }

        public obs() { }

        public obs(string metErg, string obsCompl)
        {
            this.metErg = metErg;
            this.obsCompl = obsCompl;
        }
    }
}