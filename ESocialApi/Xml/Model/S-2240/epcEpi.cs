﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.S_2240
{
    public class epcEpi
    {
        //Utilização de EPC:
        //0 - Não se aplica;
        //1 - Não utilizado;
        //2 - Utilizado.
        public string utilizEPC { get; set;}

        //Os EPCs são eficazes na neutralização dos riscos ao trabalhador?
        //S - Sim;
        //N - Não
        public string eficEpc { get; set;}

        //Utilização de EPI:
        //0 - Não se aplica;
        //1 - Não utilizado;
        //2 - Utilizado.
        public string utilizEPI { get; set;}

        // Equipamento de Proteção Individual - EPI.
        [XmlElement("epi", IsNullable = false)]
        public List<epi> epi { get; set;}
        public epiCompl epiCompl { get; set; }

        public epcEpi() { }

        public epcEpi(string utilizEPC, string utilizEPI)
        {
            this.utilizEPC = utilizEPC;
            this.utilizEPI = utilizEPI;
        }

        public epcEpi(string utilizEPC, string utilizEPI, List<epi> epi)
        {
            this.utilizEPC = utilizEPC;
            this.utilizEPI = utilizEPI;
            this.epi = epi;
        }
    }
}