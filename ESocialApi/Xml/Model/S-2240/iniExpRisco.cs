﻿namespace ESocialApi.Xml.Model.S_2240
{
    public class iniExpRisco
    {
        public string dtIniCondicao { get; set; }
        public infoAmb infoAmb { get; set; }

        public iniExpRisco() { }

        public iniExpRisco(string dtIniCondicao, infoAmb infoAmb)
        {
            this.dtIniCondicao = dtIniCondicao;
            this.infoAmb = infoAmb;
        }
    }
}