﻿
namespace ESocialApi.Xml.Model.S_2240
{
    public class respReg
    {

        // CPF do responsável pelos registros ambientais. Validação: Deve ser um CPF válido.
        public string cpfResp { get; set; }

        //Preencher com o Número de Identificação Social - NIS do responsável pelos registros ambientais, o qual pode ser o PIS, PASEP ou NIT
        public string nisResp { get; set; }

        //nome do responsável pelos registros ambientais
        public string nmResp { get; set; }

        //1 - Conselho Regional de Medicina(CRM);
        //2 - Conselho Regional de Engenharia e Agronomia(CREA);
        //9 - Outros
        public int ideOC { get; set; }

        //Preenchimento obrigatório se {ideOC} = [9]. Não informar se {ideOC} = [1,2]
        public string dscOC { get; set; }

        //Número de Inscrição no órgão de classe
        public string nrOC { get; set; }

        //Sigla da UF do órgão de classe
        public string ufOC { get; set; }

        public respReg() { }

        public respReg(string cpfResp, string nisResp, string nmResp, int ideOC, string nrOC, string ufOC)
        {
            this.cpfResp = cpfResp;
            this.nisResp = nisResp;
            this.nmResp = nmResp;
            this.ideOC = ideOC;
            this.nrOC = nrOC;
            this.ufOC = ufOC;
        }
    }
}