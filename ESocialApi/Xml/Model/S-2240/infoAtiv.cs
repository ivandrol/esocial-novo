﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.S_2240
{
    public class infoAtiv
    {
        public string dscAtivDes { get; set; }

        [XmlElement("ativPericInsal")]
        public List<ativPericInsal> ativPericInsal { get; set; }

        public infoAtiv() { }

        public infoAtiv(string dscAtivDes, List<ativPericInsal> ativPericInsal)
        {
            this.dscAtivDes = dscAtivDes;
            this.ativPericInsal = ativPericInsal;
        }
    }
}