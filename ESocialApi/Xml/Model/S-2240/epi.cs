﻿
namespace ESocialApi.Xml.Model.S_2240
{
    public class epi
    {
        public string docAval { get; set;}

        //Descrição do EPI. Validação: Preenchimento obrigatório e exclusivo se {caEPI} não for informado.
        public string dscEPI { get; set; }

        //O EPI é eficaz na neutralização do risco ao trabalhador?
        //S - Sim;
        //N - Não.
        public string eficEpi { get; set; }

        //S - Sim;
        //N - Não

        public epi() { }

        public epi(string eficEpi, string docAval, string dscEPI)
        {
            this.eficEpi = eficEpi;
            this.docAval = docAval;
            this.dscEPI = dscEPI;
        }
    }
}