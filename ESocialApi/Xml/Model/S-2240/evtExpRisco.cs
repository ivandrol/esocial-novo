﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.S_2240
{
    public class evtExpRisco
    {
        [XmlAttribute]
        public string Id { get; set; }
        public ideEvento ideEvento { get; set; }
        public ideEmpregador ideEmpregador { get; set; }
        public ideVinculo ideVinculo { get; set; }
        public infoExpRisco infoExpRisco { get; set; }

        public evtExpRisco() { }

        public evtExpRisco(ideEmpregador ideEmpregador, ideVinculo ideVinculo, infoExpRisco infoExpRisco)
        {
            this.ideEmpregador = ideEmpregador;
            this.ideVinculo = ideVinculo;
            this.infoExpRisco = infoExpRisco;
        }

        public evtExpRisco(ideEvento ideEvento, ideEmpregador ideEmpregador, ideVinculo ideVinculo, infoExpRisco infoExpRisco)
        {
            this.ideEvento = ideEvento;
            this.ideEmpregador = ideEmpregador;
            this.ideVinculo = ideVinculo;
            this.infoExpRisco = infoExpRisco;
        }
    }
}