﻿
namespace ESocialApi.Xml.Model.S_2240
{ 
    public class fatRisco
    {
        public string codAgNoc { get; set; }
        public string dscAgNoc { get; set; }
        public int tpAval { get; set; }

        //Técnica utilizada para medição da intensidade ou concentração. Validação: Preenchimento obrigatório se {tpAval} = [1]. Não informar se {tpAval} = [2]
        public string intConc { get; set; }

        public string limTol { get; set; }

        //Técnica utilizada para medição da intensidade ou concentração. Validação: Preenchimento obrigatório se {tpAval} = [1]. Não informar se {tpAval} = [2]
        public string unMed { get; set; }

        //Técnica utilizada para medição da intensidade ou concentração. Validação: Preenchimento obrigatório se {tpAval} = [1]. Não informar se {tpAval} = [2]
        public string tecMedicao { get; set; }

        //Validação: Preenchimento obrigatório se {tpAval} = [1] e { codAgNoc} = [02.01.687, 02.01.788]. Não informar nos demais casos.

        /*public string insalubridade { get; set; }
        public string periculosidade { get; set; }
        public string aposentEsp { get; set; }
        public string dscFatRisc { get; set; }*/
        public epcEpi epcEpi { get; set; }

        public fatRisco() { }

        public fatRisco(string codAgNoc, string dscAgNoc, int tpAval, string intConc, string unMed, string tecMedicao, string insalubridade, string periculosidade, string aposentEsp, epcEpi epcEpi)
        {
            this.codAgNoc = codAgNoc;
            this.dscAgNoc = dscAgNoc;
            this.tpAval = tpAval;
            this.intConc = intConc;
            this.unMed = unMed;
            this.tecMedicao = tecMedicao;
            /*this.insalubridade = insalubridade;
            this.periculosidade = periculosidade;
            this.aposentEsp = aposentEsp;*/
            this.epcEpi = epcEpi;
        }

        public fatRisco(string codAgNoc, int tpAval, epcEpi epcEpi)
        {
            this.codAgNoc = codAgNoc;
            this.tpAval = tpAval;
            this.epcEpi = epcEpi;
        }
    }
}