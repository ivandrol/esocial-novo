﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.S_2240
{
    public class infoExpRisco
    {
        public string dtIniCondicao { get; set; }

        [XmlElement]
        public List<infoAmb> infoAmb { get; set; }

        public infoAtiv infoAtiv { get; set; }

        [XmlElement]
        public List<fatRisco> fatRisco { get; set; }

        [XmlElement]
        public List<respReg> respReg { get; set; }
        
        public obs obs { get; set; }

        public infoExpRisco() { }

        public infoExpRisco(string dtIniCondicao, List<infoAmb> infoAmb, List<fatRisco> fatRisco, List<respReg> respReg)
        {
            this.dtIniCondicao = dtIniCondicao;
            this.infoAmb = infoAmb;
            this.fatRisco = fatRisco;
            this.respReg = respReg;
        }
    }
}