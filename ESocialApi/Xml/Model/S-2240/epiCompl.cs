﻿
namespace ESocialApi.Xml.Model.S_2240
{
    public class epiCompl
    {
        //O EPI é eficaz na neutralização do risco ao trabalhador?
        //S - Sim;
        //N - Não.
        public string eficEpi { get; set; }

        //S - Sim;
        //N - Não
        public string medProtecao { get; set;}

        //S - Sim;
        //N - Não
        public string condFuncto { get; set;}

        //S - Sim;
        //N - Não
        public string usoInint { get; set; }

        //S - Sim;
        //N - Não
        public string przValid { get; set;}

        //S - Sim;
        //N - Não
        public string periodicTroca { get; set;}

        //S - Sim;
        //N - Não
        public string higienizacao { get; set;}

        public epiCompl() { }

        public epiCompl(string medProtecao, string condFuncto, string usoInint, string przValid, string periodicTroca, string higienizacao)
        {
            this.medProtecao = medProtecao;
            this.condFuncto = condFuncto;
            this.usoInint = usoInint;
            this.przValid = przValid;
            this.periodicTroca = periodicTroca;
            this.higienizacao = higienizacao;
        }
    }
}