﻿
namespace ESocialApi.Xml.Model.S_2240
{
    public class ativPericInsal
    {
        public string codAtiv { get; set; }

        public ativPericInsal(){ }

        public ativPericInsal(string codAtiv)
        {
            this.codAtiv = codAtiv;
        }
    }
}