﻿namespace ESocialApi.Xml.Model
{
    public class ideEmpregador
    {
        public int tpInsc { get; set; }
        public string nrInsc { get; set; }

        public ideEmpregador() { }

        public ideEmpregador(int TpInsc, string NrInsc)
        {
            tpInsc = TpInsc;
            nrInsc = NrInsc;
        }
    }
}