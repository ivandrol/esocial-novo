﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESocialApi.Xml.Model
{
    public class envioLoteEventos
    {
        public ideEmpregador ideEmpregador { get; set; }
        public ideTransmissor ideTransmissor { get; set; }

        public envioLoteEventos(ideEmpregador ideEmpregador, ideTransmissor ideTransmissor)
        {
            this.ideEmpregador = ideEmpregador;
            this.ideTransmissor = ideTransmissor;
        }

    }
}