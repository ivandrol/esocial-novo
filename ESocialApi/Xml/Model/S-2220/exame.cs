﻿using System;

namespace ESocialApi.Xml.Model.S_2220
{
    public class exame
    {
        public string dtExm { get; set; }
        public string procRealizado { get; set; }
        public string obsProc { get; set; }
        public int ordExame { get; set; }
        public string indResult { get; set; }
        public medico medico { get; set;  }

        public exame() { }

        public exame(string dtExm, string procRealizado, int ordExame, medico medico)
        {
            this.dtExm = dtExm;
            this.procRealizado = procRealizado;
            this.ordExame = ordExame;
            this.medico = medico;
        }
    }
}