﻿
namespace ESocialApi.Xml.Model.S_2220
{
    public class eSocial2220
    {
        public evtMonit evtMonit { get; set; }

        public eSocial2220() { }

        public eSocial2220(evtMonit evtMonit)
        {
            this.evtMonit = evtMonit;
        }

    }
}