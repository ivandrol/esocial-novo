﻿using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.S_2220
{
    public class evtMonit
    {
        [XmlAttribute]
        public string Id { get; set; }
        
        public ideEvento ideEvento { get; set; }
        public ideEmpregador ideEmpregador { get; set; }
        public ideVinculo ideVinculo { get; set; }
        public exMedOcup exMedOcup { get; set; }

        public evtMonit()
        {
        }

        public evtMonit(ideEvento ideEvento, ideEmpregador ideEmpregador, ideVinculo ideVinculo, exMedOcup exMedOcup)
        {
            this.ideEvento = ideEvento;
            this.ideEmpregador = ideEmpregador;
            this.ideVinculo = ideVinculo;
            this.exMedOcup = exMedOcup;
        }

    }
}