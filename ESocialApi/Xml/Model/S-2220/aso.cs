﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.S_2220
{
    public class aso
    {
        public string dtAso { get; set; }
        public int resAso { get; set; }
        [XmlElement("exame")]
        public List<exame> exames { get; set; }
        public medico medico { get; set; }

        public aso() { }

        public aso(string dtAso, int resAso, List<exame> exames, medico medico)
        {
            this.dtAso = dtAso;
            this.resAso = resAso;
            this.exames = exames;
            this.medico = medico;
        }
    }
}