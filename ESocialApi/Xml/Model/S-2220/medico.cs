﻿namespace ESocialApi.Xml.Model.S_2220
{
    public class medico
    {
        public string nmMed { get; set; }
        public string nrCRM { get; set; }
        public string ufCRM { get; set; }
        public string cpfMed { get; set; }
        public string nisMed { get; set; }

        public medico() { }

        public medico(string nmMed, string nrCRM, string ufCRM)
        {
            this.nmMed = nmMed;
            this.nrCRM = nrCRM;
            this.ufCRM = ufCRM;
        }
    }
}