﻿namespace ESocialApi.Xml.Model.S_2220
{ 

    public class respMonit
    {
        public string cpfResp { get; set; }
        public string nmResp { get; set; }
        public string nrCRM { get; set; }
        public string ufCRM { get; set; }

        public respMonit() { }

        public respMonit(string cpfResp, string nmResp, string nrCRM, string ufCRM)
        {
            this.nmResp = nmResp;
            this.nrCRM = nrCRM;
            this.ufCRM = ufCRM;
        }
    }
}