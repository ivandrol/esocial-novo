﻿namespace ESocialApi.Xml.Model.S_2220
{
    public class exMedOcup
    {
        //0 - Exame médico admissional;
        //1 - Exame médico periódico, conforme NR7 do MTb e/ou planejamento do PCMSO;
        //2 - Exame médico de retorno ao trabalho;
        //3 - Exame médico de mudança de função;
        //4 - Exame médico de monitoração pontual, não enquadrado nos demais casos;
        //9 - Exame médico demissional.
        public int tpExameOcup { get; set; }
        public aso aso { get; set; }
        public respMonit respMonit { get; set; }

        public exMedOcup()
        {
        }

        public exMedOcup(int tpExameOcup, aso aso, respMonit respMonit)
        {
            this.tpExameOcup = tpExameOcup;
            this.aso = aso;
            this.respMonit = respMonit;
        }
    }
}