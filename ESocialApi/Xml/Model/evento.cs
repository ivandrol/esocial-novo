﻿using ESocialApi.Xml.Model.S_2210;
using ESocialApi.Xml.Model.S_2220;
using ESocialApi.Xml.Model.S_2240;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model
{
    public class evento
    {
        [XmlAttribute("Id")]
        public string Id { get; set; }


        [XmlElement("eSocial", Namespace = "http://www.esocial.gov.br/schema/evt/evtCAT/v02_05_00")]
        public eSocial2210 eSocial2210 { get; set; }

        [XmlElement("eSocial", Namespace = "http://www.esocial.gov.br/schema/evt/evtMonit/v02_05_00")]
        public eSocial2220 eSocial2220 { get; set; }

        [XmlElement("eSocial", Namespace = "http://www.esocial.gov.br/schema/evt/evtExpRisco/v02_05_00")]
        public eSocial2240 eSocial2240 { get; set; }

        public evento() { }

        public evento(eSocial2210 eSocial2210)
        {
            this.eSocial2210 = eSocial2210;
        }

        public evento(eSocial2220 eSocial2220)
        {
            this.eSocial2220 = eSocial2220;
        }

        public evento(eSocial2240 eSocial2240)
        {
            this.eSocial2240 = eSocial2240;
        }

    }
}