﻿
namespace ESocialApi.Xml.Model.RetornoEnvio
{
    public class dadosRecepcaoLote
    {
        public string dhRecepcao { get; set; }
        public string versaoAplicativoRecepcao { get; set; }
        public string protocoloEnvio { get; set; }

        public dadosRecepcaoLote(){ }   
    }
}