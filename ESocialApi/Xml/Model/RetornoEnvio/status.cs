﻿using ESocialApi.Xml.Model.RetornoConsulta_New;
using System.Xml.Serialization;

namespace ESocialApi.Xml.Model.RetornoEnvio
{
    public class status
    {
        public int cdResposta { get; set; }
        public string descResposta { get; set; }
        [XmlElement("ocorrencias")]
        public tagOcorrencias tagOcorrencias { get; set; }

        public status()
        {
        }
    }
}