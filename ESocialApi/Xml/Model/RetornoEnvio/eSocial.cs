﻿using ESocialApi.Xml.Model.RetornoEnvio;
using System.Xml.Serialization;

namespace Xml.Model.RetornoEnvio
{
    [XmlRoot("eSocial", Namespace = "http://www.esocial.gov.br/schema/lote/eventos/envio/retornoEnvio/v1_1_0")]
    public class eSocial
    {
        public retornoEnvioLoteEventos retornoEnvioLoteEventos { get; set; }

        public eSocial() { }
    }

}