﻿
namespace ESocialApi.Xml.Model.RetornoEnvio
{
    public class retornoEnvioLoteEventos
    {
        public ideEmpregador ideEmpregador { get; set; }
        public ideTransmissor ideTransmissor { get; set; }
        public status status { get; set; }
        public dadosRecepcaoLote dadosRecepcaoLote { get; set; }

        public retornoEnvioLoteEventos()
        {
        }
    }
}