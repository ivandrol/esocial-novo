﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ESocialApi.Helper.Xml
{
    public static class XmlConverter 
    {
        public static XmlElement ObjectToXml(object obj)
        {
            XmlDocument doc = new XmlDocument();


            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
            {
                //XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                //ns.Add("", "");

                new XmlSerializer(obj.GetType()).Serialize(writer, obj);
            }

            return doc.DocumentElement;
        }

        public static T XmlToObject<T>(string xml)
        {
            StringReader reader = new StringReader(xml);
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            T DeserializedObject = (T)xmlSerializer.Deserialize(reader);

            return DeserializedObject;
        }
    }
}