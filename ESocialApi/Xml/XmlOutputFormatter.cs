﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ESocialApi.Helper.Xml
{
    public class XmlOutputFormatter : XmlSerializerOutputFormatter
    {
        public override Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            IServiceProvider serviceProvider = context.HttpContext.RequestServices;
            var logger = serviceProvider.GetService(typeof(ILogger<XmlOutputFormatter>)) as ILogger;

            var response = context.HttpContext.Response;

            byte[] utf8;

            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var settings = new XmlWriterSettings();
            settings.Indent = false;
            settings.OmitXmlDeclaration = false;

            var serializer = new XmlSerializer(context.Object.GetType());
            using (var memStm = new MemoryStream())
            using (var xmlwriter = XmlWriter.Create(memStm, settings))
            {
                serializer.Serialize(xmlwriter, context.Object, emptyNamepsaces);
                utf8 = memStm.ToArray();
            }

            return response.WriteAsync(Encoding.UTF8.GetString(utf8));
        }
    }
}
