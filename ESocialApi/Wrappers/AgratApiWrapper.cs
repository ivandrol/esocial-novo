﻿using System.Net.Http;
using System.Threading.Tasks;

namespace ESocialApi.Wrappers
{
    public static class AgratApiWrapper
    {
        public static async Task<string> GetCNPJs(string token)
        {
            using (var client = new HttpClient())
            {
                string url = "http://localhost:3000/api/esocial/getCNPJs?token=" + token;
                //string url = "https://www.sistema-agrat.com.br/api/esocial/getCNPJs?token=" + token;
                client.DefaultRequestHeaders.Accept.Clear();
                
                HttpResponseMessage response = await client.GetAsync(url);
                //if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsAsync<string>();
            }
        }
    }
}
