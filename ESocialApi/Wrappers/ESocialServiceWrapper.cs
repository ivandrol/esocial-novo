﻿using System.Security.Cryptography.X509Certificates;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.ServiceModel;
using ESocialApi.Utils;
using System;
using ESocialApi.Services;
using ESocialApi.DB.Model;

namespace ESocialApi.Wrappers
{
    public class ESocialServiceWrapper
    {
        public static async Task<XElement> enviarLoteEventosAsync(Transmissor Transmissor, XElement xml)
        {
            if (Transmissor.TpAmb.Equals("1"))
            {
                // Produção
                var urlServicoEnvio = @"https://webservices.envio.esocial.gov.br/servicos/empregador/enviarloteeventos/WsEnviarLoteEventos.svc";
                var address = new EndpointAddress(urlServicoEnvio);
                var binding = new BasicHttpsBinding();
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
                binding.MaxReceivedMessageSize = 10000000;
                binding.SendTimeout = new TimeSpan(0, 3, 00);

                var client = new ESocialProducaoEnviar.ServicoEnviarLoteEventosClient(binding, address);

                X509Certificate2 certificado = Assinatura.GetCertiticate(Transmissor.SerialNumber);
                client.ClientCredentials.ClientCertificate.Certificate = certificado;

                await client.OpenAsync();

                var response = await client.EnviarLoteEventosAsync(xml);

                await client.CloseAsync();

                return response.Body.EnviarLoteEventosResult;
            }
            else if (Transmissor.TpAmb.Equals("2"))
            {
                // Produção restrita
                var urlServicoEnvio = @"https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/enviarloteeventos/WsEnviarLoteEventos.svc";
                var address = new EndpointAddress(urlServicoEnvio);
                var binding = new BasicHttpsBinding();
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
                binding.MaxReceivedMessageSize = 10000000;
                binding.SendTimeout = new TimeSpan(0, 3, 00);

                var client = new ESocialHomologacaoEnviar.ServicoEnviarLoteEventosClient(binding, address);

                X509Certificate2 certificado = Assinatura.GetCertiticate(Transmissor.SerialNumber);
                client.ClientCredentials.ClientCertificate.Certificate = certificado;

                await client.OpenAsync();

                var response = await client.EnviarLoteEventosAsync(xml);

                await client.CloseAsync();

                return response.Body.EnviarLoteEventosResult;
            }
            return null;
        }

        public static async Task<XElement> consultarLoteEventos(Transmissor Transmissor, XElement xml)
        {
            if (Transmissor.TpAmb.Equals("1"))
            {
                // Produção
                var urlServicoEnvio = @"https://webservices.consulta.esocial.gov.br/servicos/empregador/consultarloteeventos/WsConsultarLoteEventos.svc";
                var address = new EndpointAddress(urlServicoEnvio);
                var binding = new BasicHttpsBinding();
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
                binding.MaxReceivedMessageSize = 10000000;
                binding.SendTimeout = new TimeSpan(0, 3, 00);

                var client = new ESocialProducaoConsultar.ServicoConsultarLoteEventosClient(binding, address);

                X509Certificate2 certificado = Assinatura.GetCertiticate(Transmissor.SerialNumber);
                client.ClientCredentials.ClientCertificate.Certificate = certificado;

                await client.OpenAsync();

                var response = await client.ConsultarLoteEventosAsync(xml);

                await client.CloseAsync();

                return response.Body.ConsultarLoteEventosResult;
            }
            else if (Transmissor.TpAmb.Equals("2"))
            {
                var urlServicoEnvio = @"https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/consultarloteeventos/WsConsultarLoteEventos.svc";
                var address = new EndpointAddress(urlServicoEnvio);
                var binding = new BasicHttpsBinding();
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Certificate;
                binding.MaxReceivedMessageSize = 10000000;
                binding.SendTimeout = new TimeSpan(0, 3, 00);

                var client = new ESocialHomologacaoConsultar.ServicoConsultarLoteEventosClient(binding, address);

                X509Certificate2 certificado = Assinatura.GetCertiticate(Transmissor.SerialNumber);
                client.ClientCredentials.ClientCertificate.Certificate = certificado;

                await client.OpenAsync();

                var response = await client.ConsultarLoteEventosAsync(xml);

                await client.CloseAsync();

                return response.Body.ConsultarLoteEventosResult;
            }
            return null;
        }
    }
}