﻿using System.Threading.Tasks;

namespace ESocialApi.Services.EmailServices
{
    public class SendGridHelper
    {
        private readonly SendGridService _sendGridService;

        public SendGridHelper()
        {
            _sendGridService = new SendGridService();
        }

        public async Task<bool> Send(EmailMessageInfo messageInfo)
        {
            // Make an API call, and return the response
            var apiResponse = await _sendGridService.Send(messageInfo);
            var body = apiResponse.Body.ReadAsStringAsync().Result;
            var headers = apiResponse.Headers.ToString();
            var statusCode = (int)apiResponse.StatusCode;
            var mensagemEnviada = statusCode == 202;

            return mensagemEnviada;
        }
    }
}
