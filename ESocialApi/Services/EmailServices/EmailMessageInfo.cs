﻿using System.Collections.Generic;

namespace ESocialApi.Services.EmailServices
{
    public class EmailMessageInfo
    {
        public EmailMessageInfo(string toEmailAddress,
            string emailSubject,
            string emailBody,
            List<AttachmentInfo> listAttachmentInfo = null,
            string bccEmailAddress = null,
            string ccEmailAddress = null,
            string fromName = "Agrat API e-Social",
            string fromEmailAddress = "nao-responder@agrat.com.br")
        {
            FromEmailAddress = fromEmailAddress;
            FromName = fromName;
            ToEmailAddress = toEmailAddress;
            CcEmailAddress = ccEmailAddress;
            BccEmailAddress = bccEmailAddress;
            EmailSubject = emailSubject;
            EmailBody = emailBody;
            ListAttachmentInfo = listAttachmentInfo;
        }

        public string FromEmailAddress { get; set; }
        public string FromName { get; set; }
        public string ToEmailAddress { get; set; }
        public string CcEmailAddress { get; set; }
        public string BccEmailAddress { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }

        public List<AttachmentInfo> ListAttachmentInfo { get; set; }
    }

    public class AttachmentInfo
    {
        public AttachmentInfo(string name, byte[] content, string contentType)
        {
            Name = name;
            Content = content;
            ContentType = contentType;
        }

        public string Name { get; set; }
        public byte[] Content { get; set; }
        public string ContentType { get; set; }
    }
}
