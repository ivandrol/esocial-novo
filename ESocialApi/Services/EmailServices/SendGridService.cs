﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ESocialApi.Services.EmailServices
{
    public class SendGridService
    {
        private readonly SendGridClient _client;

        public SendGridService()
        {
            // Retrieve the API key from an appSettings variable from the web.config
            // var apiKey = ConfigurationManager.AppSettings["SendGrid_API_Key"];
            var apiKey = "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";

            // Initialize the SendGrid client
            _client = new SendGridClient(apiKey);
        }

        public async Task<Response> Send(EmailMessageInfo messageInfo)
        {
            // Prepare the SendGrid email message
            var sendgridMessage = new SendGridMessage
            {
                From = new EmailAddress(messageInfo.FromEmailAddress, messageInfo.FromName),
                Subject = messageInfo.EmailSubject,
                HtmlContent = messageInfo.EmailBody
            };

            // Add the "To" email address to the message
            //sendgridMessage.AddTo(new EmailAddress(messageInfo.ToEmailAddress));
            var toArray = ConvertStringToArray(messageInfo.ToEmailAddress).Distinct().ToArray();

            foreach (var emailAddress in toArray)
            {
                sendgridMessage.AddTo(new EmailAddress(emailAddress));
            }

            // Check if any Cc email address was specified
            if (!string.IsNullOrWhiteSpace(messageInfo.CcEmailAddress))
            {
                // Add the email address for Cc to the message
                //sendgridMessage.AddCc(new EmailAddress(messageInfo.CcEmailAddress));

                var ccArray = ConvertStringToArray(messageInfo.CcEmailAddress).Distinct().ToArray();

                foreach (var emailAddress in ccArray)
                {
                    sendgridMessage.AddCc(new EmailAddress(emailAddress));
                }
            }

            // Check if any Bcc email address was specified
            if (!string.IsNullOrWhiteSpace(messageInfo.BccEmailAddress))
            {
                // Add the email address for Bcc to the message
                //sendgridMessage.AddBcc(new EmailAddress(messageInfo.BccEmailAddress));

                var bccArray = ConvertStringToArray(messageInfo.BccEmailAddress).Distinct().ToArray();

                foreach (var emailAddress in bccArray)
                {
                    sendgridMessage.AddBcc(new EmailAddress(emailAddress));
                }
            }

            if (messageInfo.ListAttachmentInfo != null)
            {
                foreach (AttachmentInfo a in messageInfo.ListAttachmentInfo)
                {
                    using (var fileStream = new MemoryStream(a.Content))
                    {
                        await sendgridMessage.AddAttachmentAsync(a.Name, fileStream, a.ContentType);
                    }
                }
            }

            // Return the SendGrid response
            return await _client.SendEmailAsync(sendgridMessage);
        }

        private string[] ConvertStringToArray(string str)
        {
            return str
                .Replace(";", "|")
                .Replace(",", "|")
                .Replace(" ", string.Empty)
                .Trim('|')
                .Split('|');
        }
    }
}
