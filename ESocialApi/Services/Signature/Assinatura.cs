﻿using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace ESocialApi.Utils
{
    public static class Assinatura
    {
        public static X509Certificate2 GetCertiticate(string SerialNumber)
        {
            var localMachineStore = new X509Store(StoreLocation.LocalMachine);
            localMachineStore.Open(OpenFlags.ReadOnly);
            var certificates = localMachineStore.Certificates;
            localMachineStore.Close();

            foreach (var cert in certificates.Cast<X509Certificate2>().Where(cert => cert.SerialNumber.Equals(SerialNumber.ToUpper())))
            {
                return cert;
            }

            return null;
        }
    }
}
