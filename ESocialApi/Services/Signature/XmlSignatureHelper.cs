﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ESocialApi.Utils
{
    public static class XmlSignatureHelper
    {
        //
        // SignedXml.CheckSignature Method (X509Certificate2, Boolean) -> Examples [SHA1]
        // https://msdn.microsoft.com/en-us/library/ms148731(v=vs.110).aspx
        //
        // Using SHA256 with the SignedXml Class
        // https://blogs.msdn.microsoft.com/winsdk/2015/11/14/using-sha256-with-the-signedxml-class/
        //
        public static XmlDocument SignXmlDoc(XmlDocument xmlDoc, X509Certificate2 certificate, int signatureMethod)
        {
            //
            // https://docs.microsoft.com/en-us/dotnet/framework/whats-new/#Crypto462
            //
            // SignedXml support for SHA-2 hashing The .NET Framework 4.6.2 adds support
            // to the SignedXml class for RSA-SHA256, RSA-SHA384, and RSA-SHA512 PKCS#1
            // signature methods, and SHA256, SHA384, and SHA512 reference digest algorithms.
            //
            // Any programs that have registered a custom SignatureDescription handler into CryptoConfig
            // to add support for these algorithms will continue to function as they did in the past, but
            // since there are now platform defaults, the CryptoConfig registration is no longer necessary.
            //
            //// First of all, we need to register a SignatureDescription class that defines the DigestAlgorithm as SHA256.
            //// You have to reference the System.Deployment assembly in your project.
            //CryptoConfig.AddAlgorithm(
            //   typeof(System.Deployment.Internal.CodeSigning.RSAPKCS1SHA256SignatureDescription),
            //   "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");
            // RSAPKCS1SHA256SignatureDescription -> Disponível desde .NET Framework 4.5

            SignedXml signedXml = new SignedXml(xmlDoc)
            {
                // Add the key to the SignedXml document. 
                SigningKey = certificate.GetRSAPrivateKey()   // Disponível desde .NET Framework 4.6
            };
            //signedXml.SigningKey = GetRSAPrivateKey(certificate);

            //
            // https://docs.microsoft.com/en-us/dotnet/framework/whats-new/#Crypto462
            //
            // SignedXml support for SHA-2 hashing The .NET Framework 4.6.2 adds support
            // to the SignedXml class for RSA-SHA256, RSA-SHA384, and RSA-SHA512 PKCS#1
            // signature methods, and SHA256, SHA384, and SHA512 reference digest algorithms.

          
            signedXml.SignedInfo.SignatureMethod = SecurityAlgorithms.RsaSha256Signature;

            // Create a reference to be signed. Pass "" to specify that
            // all of the current XML document should be signed.
            Reference reference = new Reference(string.Empty);

            reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
            reference.AddTransform(new XmlDsigC14NTransform());
            reference.DigestMethod = SecurityAlgorithms.Sha256Digest; //""http://www.w3.org/2001/04/xmlenc#sha256"

            // Add the reference to the SignedXml object.
            signedXml.AddReference(reference);

            signedXml.KeyInfo = new KeyInfo();
            // Load the certificate into a KeyInfoX509Data object
            // and add it to the KeyInfo object.
            signedXml.KeyInfo.AddClause(new KeyInfoX509Data(certificate));

            // Compute the signature.
            signedXml.ComputeSignature();

            // Get the XML representation of the signature and save
            // it to an XmlElement object.
            XmlElement xmlDigitalSignature = signedXml.GetXml();

            // Append the element to the XML document.
            xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, true));

            if (xmlDoc.FirstChild is XmlDeclaration)
                xmlDoc.RemoveChild(xmlDoc.FirstChild);

            return xmlDoc;
        }
    }
}