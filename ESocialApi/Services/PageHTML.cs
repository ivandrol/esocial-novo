﻿namespace ESocialApi.Services
{
    public class PageHTML
    {
        public string Head { get; set; }
        public string BeginBody { get; set; }
        public string EndBody { get; set; }
        public string Title { get; set; }
        public string Footer { get; set; }
        public string Card { get; set; }
        public string Ocorrencia { get; set; }
        public string DataEvento { get; set; }
        public string Ambiente { get; set; }

        public PageHTML()
        {
            Head = "<!DOCTYPE html>"
                + "<head>"
                + "    <title>Eventos e-Social</title>"
                + "    <meta charset=\"utf-8\" lang=\"pt-br\" />"
                + "</head>";

            BeginBody = "<body>"
                    + "<html>";

            EndBody = "</body>"
                    + "</html>";

            Title = "<h2 style=\"text-align: center;\">[titulo]</h2>";

            Ambiente = "<p style=\"font-size: x-large; font-weight: bold;\">Ambiente: [Ambiente]</p>";
            DataEvento = "<p style=\"font-size: x-large; font-weight: bold;\">Data do Evento: [DataEvento]</p>";

            Card = "	<div style=\"background-color: #eee; padding: 40px 25px; margin: 0 auto; max-width: 500px; text-align: center;\">"
                + "        <p style=\"font-size: x-large; font-weight: bold;\">Evento [tpEvento] - [idEvento]</p>"
                + "        <p>[resposta]</p>"
                + "    </div>"
                + "    <p style=\"font-size: x-small; text-align: center;\"></p>";

            Footer = "<p style=\"font-size: x-small; text-align: center;\">Mensagem enviada através do site API Agrat eSocial.</p>";



        }
    }
}
