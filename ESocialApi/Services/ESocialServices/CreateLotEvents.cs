﻿using System.Collections.Generic;
using ESocialApi.Helper.Xml;
using System.Text;
using ESocialApi.Utils;
using System;
using System.Collections;
using ESocialApi.Interfaces.Repository;
using ESocialApi.Interfaces.ESocialTransmissao;
using System.Threading.Tasks;
using ESocialApi.DB.Model;
using ESocialApi.Xml.Model.S_2210;
using ESocialApi.Xml.Model.S_2220;
using ESocialApi.Xml.Model.S_2240;
using ESocialApi.Xml.Model;
using System.Xml.Linq;
using System.Xml;
using System.Security.Cryptography.X509Certificates;
using ESocialApi.Wrappers;

namespace ESocialApi.Services.ESocialServices
{
    public class CreateLotEvents : ICreateLotEvents
    {

        //Hashtable com uma chave, o Id do lote, e o valor, o XElement 
        private Hashtable _eventTable;

        private readonly IUnitOfWork _unitOfWork;

        public CreateLotEvents(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _eventTable = new Hashtable();
        }

        //Retorna uma Hashtable com uma lista de lotes como valor e o id do lote como chave
        public Task<Hashtable> CreateEventLot(List<ESocial> EventList, int grupo)
        {
            Lote lote = new Lote(Guid.NewGuid().ToString("N"));
            _unitOfWork.Lote.Add(lote);

            var NrInsc = EventList[0].NrInsc;
            var TpInsc = EventList[0].Empregador.TpInsc;

            var NrInscTransmissor = EventList[0].Empregador.NrInscTransmissor;
            var TpInscTransmissor = EventList[0].Empregador.Transmissor.TpInsc;

            X509Certificate2 certificado = Assinatura.GetCertiticate(EventList[0].Empregador.Transmissor.SerialNumber);

            XAttribute xGrupo = new XAttribute("grupo", grupo);
            XNamespace ns = "http://www.esocial.gov.br/schema/lote/eventos/envio/v1_1_1";
            XElement eSocial = new XElement(ns + "eSocial");
            XElement envioLoteEventos = new XElement(ns + "envioLoteEventos", xGrupo);
            XElement ideEmpregador = new XElement(ns + "ideEmpregador");
            ideEmpregador.Add(new XElement(ns + "tpInsc", TpInsc));
            ideEmpregador.Add(new XElement(ns + "nrInsc", NrInsc.Substring(0, 8)));
            XElement ideTransmissor = new XElement(ns + "ideTransmissor");
            ideTransmissor.Add(new XElement(ns + "tpInsc", TpInscTransmissor));
            ideTransmissor.Add(new XElement(ns + "nrInsc", NrInscTransmissor));
            envioLoteEventos.Add(ideEmpregador);
            envioLoteEventos.Add(ideTransmissor);
            XElement eventos = new XElement(ns + "eventos");
            XElement eventosHomologacao = new XElement(ns + "eventos");

            string idAux = "";
            int sequencial = 1;
            ESocial2210 ESocial2210 = null;
            ESocial2220 ESocial2220 = null;
            ESocial2240 ESocial2240 = null;
            for (var i = 0; i < EventList.Count; i++)
            {
                ESocial ESocial = EventList[i];

                var Id = $"ID{TpInsc}{NrInsc.Substring(0, 8).PadRight(14, '0')}{DateTime.Now.ToString("yyyyMMddHHmmss")}";
                sequencial = Id.Equals(idAux) ? sequencial++ : 1;
                idAux = Id;
                Id = Id + sequencial.ToString().PadLeft(5, '0');
                XAttribute id = new XAttribute("Id", Id);
                XElement evento = new XElement(ns + "evento", id);
                if (ESocial.TpEvento.Equals("2210"))
                {
                    ESocial2210 = ESocial.ESocial2210;
                    evento.Add(CreateEvent2210(ESocial, ESocial2210, Id, certificado));
                }
                else if (ESocial.TpEvento.Equals("2220"))
                {
                    ESocial2220 = ESocial.ESocial2220;
                    evento.Add(CreateEvent2220(ESocial, ESocial2220, Id, certificado));
                }
                else if (ESocial.TpEvento.Equals("2240"))
                {
                    ESocial2240 = ESocial.ESocial2240;
                    evento.Add(CreateEvent2240(ESocial, ESocial2240, Id, certificado));
                }
                eventos.Add(evento);

                //Sai do loop e cria outro lote caso o lote for maior que 750kb(comparação com 749 pois 1 kb reservado para o conteudo do lote) ou tiver mais que 50 eventos
                var XmlEventsSize = Encoding.UTF8.GetByteCount(eventos.ToString());
                if (XmlEventsSize > 749000 || i >= 50)
                {
                    break;
                }

                lote.ESocial.Add(ESocial);
            }

            _unitOfWork.Complete();
            envioLoteEventos.Add(eventos);
            eSocial.Add(envioLoteEventos);
            _eventTable.Add(lote.Id, eSocial);

            return Task.FromResult(_eventTable);
        }

        public string CreateEvent(string tpEvento, string Token, int grupo)
        {
            string response = AgratApiWrapper.GetCNPJs(Token).Result;
            if (response.StartsWith("erro:"))
            {
                return response;
            }

            string idAux = "";
            int sequencial = 1;
            ESocial2210 ESocial2210 = null;
            ESocial2220 ESocial2220 = null;
            ESocial2240 ESocial2240 = null;
            XElement eventos = new XElement("eventos");
            var empresas = response.Split(",");
            int qtde = 0;
            foreach (string NrInsc in empresas)
            {
                var naoEnviados = _unitOfWork.ESocial.ObtemEventosNaoBaixadosPorEmpresa(NrInsc);
                for (var i = 0; i < naoEnviados.Count; i++)
                {
                    ESocial ESocial = naoEnviados[i];
                    ESocial eSocialDB = _unitOfWork.ESocial.Get(ESocial.Id);

                    var Id = $"ID{ESocial.Empregador.TpInsc}{NrInsc.Substring(0, 8).PadRight(14, '0')}{DateTime.Now.ToString("yyyyMMddHHmmss")}";
                    sequencial = Id.Equals(idAux) ? sequencial++ : 1;
                    idAux = Id;
                    Id = Id + sequencial.ToString().PadLeft(5, '0');
                    if (tpEvento.Equals("2210"))
                    {
                        ESocial2210 = ESocial.ESocial2210;
                        eventos.Add(CreateEvent2210(ESocial, ESocial2210, Id, null));
                    }
                    else if (tpEvento.Equals("2220"))
                    {
                        ESocial2220 = ESocial.ESocial2220;
                        eventos.Add(CreateEvent2220(ESocial, ESocial2220, Id, null));
                    }
                    else if (tpEvento.Equals("2240"))
                    {
                        ESocial2240 = ESocial.ESocial2240;
                        eventos.Add(CreateEvent2240(ESocial, ESocial2240, Id, null));
                    }
                    eSocialDB.StatusGet = true;
                    qtde++;
                }

            }
            if (qtde == 0)
            {
                return "erro: Nenhum evento pendente para os CNPJs informados.";
            }
            else
            {
                _unitOfWork.Complete();
                return eventos.ToString(SaveOptions.DisableFormatting);
            }
        }


        public XElement GetXmlEvent(ESocial ESocial)
        {
            string idAux = "";
            int sequencial = 1;

            var Id = $"ID{ESocial.Empregador.TpInsc}{ESocial.NrInsc.Substring(0, 8).PadRight(14, '0')}{DateTime.Now.ToString("yyyyMMddHHmmss")}";
            sequencial = Id.Equals(idAux) ? sequencial++ : 1;
            idAux = Id;
            Id = Id + sequencial.ToString().PadLeft(5, '0');

            XElement evento = null;
            if (ESocial.TpEvento.Equals("2210"))
            {
                evento = CreateEvent2210(ESocial, ESocial.ESocial2210, Id, null);
            }
            else if (ESocial.TpEvento.Equals("2220"))
            {
                evento = CreateEvent2220(ESocial, ESocial.ESocial2220, Id, null);
            }
            else if (ESocial.TpEvento.Equals("2240"))
            {
                evento = CreateEvent2240(ESocial, ESocial.ESocial2240, Id, null);
            }
            return evento;
        }

        public string GetXmlEvent(int idESocial, string tpEvento)
        {

            XElement eventos = new XElement("eventos");

            ESocial ESocial = _unitOfWork.ESocial.GetESocial(idESocial);
            eventos.Add(GetXmlEvent(ESocial));

            return eventos.ToString();
        }

        public XElement CreateEvent2210(ESocial ESocial, ESocial2210 ESocial2210, string Id, X509Certificate2 certificado)
        {
            ideEvento ideEvento = new ideEvento();
            ideEvento.tpAmb = int.Parse(ESocial.Empregador.Transmissor.TpAmb);
            ESocial.TpAmb = ESocial.Empregador.Transmissor.TpAmb;
            ideEvento.procEmi = UtilInfo.ideEventoProcEmi;
            ideEvento.verProc = UtilInfo.ideEventoVerProc;

            if (ESocial.Operacao == "I")
            {
                ideEvento.indRetif = "1";
            }
            else if (ESocial.Operacao == "U")
            {
                ideEvento.indRetif = "2";
                ESocial.NrReciboOriginal = _unitOfWork.ESocial.GetRecibo(ESocial);
                ideEvento.nrRecibo = ESocial.NrReciboOriginal;
            }
            else
            {
                return createEvent3000(ESocial, ESocial2210.Funcionario, Id, certificado, "S-2210");
            }

            ideEmpregador ideEmpregador = new ideEmpregador();
            ideEmpregador.tpInsc = ESocial.Empregador.TpInsc;
            ideEmpregador.nrInsc = ESocial.NrInsc.Substring(0, 8);

            ideVinculo ideVinculo = new ideVinculo();
            ideVinculo.cpfTrab = ESocial.ESocial2210.CpfTrab;
            ideVinculo.codCateg = ESocial.ESocial2210.Funcionario.CodCateg;
            ideVinculo.matricula = ESocial.ESocial2210.Funcionario.Matricula;
            ideVinculo.nisTrab = ESocial.ESocial2210.Funcionario.Nis;

            cat cat = new cat();

            List<parteAtingida> parteAtingida = new List<parteAtingida>();
            List<ESocial2210ParteAtingida> lisParteAtingida = ESocial2210.ESocial2210ParteAtingida;
            for (var i = 0; i < lisParteAtingida.Count; i++)
            {
                ESocial2210ParteAtingida ESocial2210ParteAtingida = lisParteAtingida[i];

                parteAtingida parteAtingidaObj = new parteAtingida();
                parteAtingidaObj.codParteAting = ESocial2210ParteAtingida.CodParteAting;
                parteAtingidaObj.lateralidade = ESocial2210ParteAtingida.Lateralidade;

                parteAtingida.Add(parteAtingidaObj);
            }

            List<agenteCausador> agenteCausador = new List<agenteCausador>();
            List<ESocial2210AgenteCausador> lisAgenteCausador = ESocial2210.ESocial2210AgenteCausador;
            for (var i = 0; i < lisAgenteCausador.Count; i++)
            {
                ESocial2210AgenteCausador ESocial2210AgenteCausador = lisAgenteCausador[i];

                agenteCausador agenteCausadorObj = new agenteCausador();
                agenteCausadorObj.codAgntCausador = ESocial2210AgenteCausador.CodAgntCausador;

                agenteCausador.Add(agenteCausadorObj);
            }
            if (ESocial2210.NrRecCatOrig != null)
            {
                catOrigem catOrigem = new catOrigem();
                catOrigem.nrRecCatOrig = ESocial2210.NrRecCatOrig;
                cat.catOrigem = catOrigem;
            }

            if (ESocial2210.CodCNES != null)
            {
                atestado atestado = new atestado();
                atestado.codCNES = ESocial2210.CodCNES;
                if (ESocial2210.DtAtendimento != null)
                {
                    atestado.dtAtendimento = ESocial2210.DtAtendimento.Value.ToString("yyyy-MM-dd");
                }
                atestado.hrAtendimento = ESocial2210.HrAtendimento;
                atestado.indInternacao = ESocial2210.IndInternacao;
                atestado.durTrat = ESocial2210.DurTrat;
                atestado.indAfast = ESocial2210.IndAfast;
                atestado.dscLesao = ESocial2210.DscLesao;
                atestado.dscCompLesao = ESocial2210.DscCompLesao;
                atestado.diagProvavel = ESocial2210.DiagProvavel;
                atestado.codCID = ESocial2210.CodCID;
                atestado.observacao = ESocial2210.Observacao;

                if (ESocial2210.NmEmit != null)
                {
                    emitente emitente = new emitente();
                    emitente.nmEmit = ESocial2210.NmEmit;
                    emitente.ideOC = ESocial2210.IdeOC;
                    emitente.nrOC = ESocial2210.NrOC;
                    emitente.ufOC = ESocial2210.UfOC;

                    atestado.emitente = emitente;
                }
                cat.atestado = atestado;
            }

            localAcidente localAcidente = new localAcidente();
            localAcidente.tpLocal = ESocial2210.TpLocal;
            localAcidente.dscLocal = ESocial2210.DscLocal;
            localAcidente.codAmb = ESocial2210.CodAmb;
            localAcidente.tpLograd = ESocial2210.TpLograd;
            localAcidente.dscLograd = ESocial2210.DscLograd;
            localAcidente.nrLograd = ESocial2210.NrLograd;
            localAcidente.complemento = ESocial2210.Complemento;
            localAcidente.bairro = ESocial2210.Bairro;
            localAcidente.cep = ESocial2210.Cep;
            localAcidente.codMunic = ESocial2210.CodMunic;
            localAcidente.uf = ESocial2210.Uf;
            localAcidente.pais = ESocial2210.IdPais;
            localAcidente.codPostal = ESocial2210.CodPostal;

            if (ESocial2210.TpInsc != null && ESocial2210.NrInsc != null)
            {
                ideLocalAcid ideLocalAcid = new ideLocalAcid();
                ideLocalAcid.tpInsc = ESocial2210.TpInsc.ToString();
                ideLocalAcid.nrInsc = ESocial2210.NrInsc;

                localAcidente.ideLocalAcid = ideLocalAcid;
            }
            if (ESocial2210.DtAcid != null)
            {
                cat.dtAcid = ESocial2210.DtAcid.ToString("yyyy-MM-dd");
            }
            cat.tpAcid = ESocial2210.TpAcid;
            cat.hrAcid = ESocial2210.HrAcid;
            cat.hrsTrabAntesAcid = ESocial2210.HrsTrabAntesAcid;
            cat.tpCat = ESocial2210.TpCat;
            cat.indCatObito = ESocial2210.IndCatObito;
            if (ESocial2210.DtObito != null)
            {
                cat.dtObito = ESocial2210.DtObito.GetValueOrDefault().ToString("yyyy-MM-dd");
            }
            cat.indComunPolicia = ESocial2210.IndComunPolicia;
            cat.codSitGeradora = ESocial2210.CodSitGeradora;
            cat.iniciatCAT = ESocial2210.IniciatCAT;
            cat.obsCAT = ESocial2210.ObsCAT;

            cat.localAcidente = localAcidente;
            cat.parteAtingida = parteAtingida;
            cat.agenteCausador = agenteCausador;

            evtCAT evtCAT = new evtCAT();
            evtCAT.Id = Id;
            evtCAT.ideEvento = ideEvento;
            evtCAT.ideEmpregador = ideEmpregador;
            evtCAT.ideVinculo = ideVinculo;
            evtCAT.cat = cat;

            ESocial eSocial = _unitOfWork.ESocial.Get(ESocial.Id);
            eSocial.IdEvento = Id;
            _unitOfWork.Complete();

            eSocial2210 eSocial2210 = new eSocial2210();
            eSocial2210.evtCAT = evtCAT;

            evento evento = new evento();
            evento.Id = Id;
            evento.eSocial2210 = eSocial2210;

            String xmlAux = XmlConverter.ObjectToXml(evento).OuterXml;
            xmlAux = xmlAux.Substring(149, xmlAux.Length - 149);
            xmlAux = xmlAux.Substring(0, xmlAux.Length - 9);

            XElement evento2210 = XDocument.Parse(xmlAux).Root;

            XAttribute id = new XAttribute("Id", Id);
            XElement eventoSemAssinatura = new XElement("evento", id);
            eventoSemAssinatura.Add(evento2210);

            ESocial.IdEvento = Id;
            ESocial.XmlEvento = eventoSemAssinatura.ToString();

            if (certificado == null)
            {
                return eventoSemAssinatura;
            }
            else
            {

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(evento2210.ToString(SaveOptions.DisableFormatting));

                string eventoAssinado = XmlSignatureHelper.SignXmlDoc(xml, certificado, 2).OuterXml;
                evento2210 = XElement.Parse(eventoAssinado);

                return evento2210;
            }
        }

        public XElement CreateEvent2220(ESocial ESocial, ESocial2220 ESocial2220, string Id, X509Certificate2 certificado)
        {
            ideEvento ideEvento = new ideEvento();
            ideEvento.tpAmb = int.Parse(ESocial.Empregador.Transmissor.TpAmb);
            ESocial.TpAmb = ESocial.Empregador.Transmissor.TpAmb;
            ideEvento.procEmi = UtilInfo.ideEventoProcEmi;
            ideEvento.verProc = UtilInfo.ideEventoVerProc;

            if (ESocial.Operacao == "I")
            {
                ideEvento.indRetif = "1";
            }
            else if (ESocial.Operacao == "U")
            {
                ideEvento.indRetif = "2";
                ESocial.NrReciboOriginal = _unitOfWork.ESocial.GetRecibo(ESocial);
                ideEvento.nrRecibo = ESocial.NrReciboOriginal;
            }
            else
            {
                return createEvent3000(ESocial, ESocial2220.Funcionario, Id, certificado, "S-2220");
            }

            ideEmpregador ideEmpregador = new ideEmpregador();
            ideEmpregador.tpInsc = ESocial.Empregador.TpInsc;
            ideEmpregador.nrInsc = ESocial.NrInsc.Substring(0, 8);

            ideVinculo ideVinculo = new ideVinculo();
            ideVinculo.cpfTrab = ESocial.ESocial2220.CpfTrab;
            ideVinculo.codCateg = ESocial.ESocial2220.Funcionario.CodCateg;
            ideVinculo.matricula = ESocial.ESocial2220.Funcionario.Matricula;
            ideVinculo.nisTrab = ESocial.ESocial2220.Funcionario.Nis;

            aso aso = new aso();
            aso.dtAso = ESocial2220.DtAso.ToString("yyyy-MM-dd");
            aso.resAso = ESocial2220.ResAso;

            List<ESocial2220Exame> listEESocial2220Exame = ESocial2220.ESocial2220Exame;
            if (listEESocial2220Exame != null && listEESocial2220Exame.Count > 0)
            {
                List<exame> exames = new List<exame>();
                for (var k = 0; k < listEESocial2220Exame.Count; k++)
                {
                    ESocial2220Exame ESocial2220Exame = listEESocial2220Exame[k];
                    exame exame = new exame();
                    exame.dtExm = ESocial2220Exame.DtExm.ToString("yyyy-MM-dd");
                    exame.procRealizado = ESocial2220Exame.ProcRealizado;
                    exame.obsProc = ESocial2220Exame.ObsProc;
                    exame.ordExame = ESocial2220Exame.OrdExame;
                    exame.indResult = ESocial2220Exame.IndResult;
                    exames.Add(exame);

                }
                aso.exames = exames;

                medico medico = new medico();
                medico.nmMed = ESocial2220.NmMed;
                medico.nrCRM = ESocial2220.NrCRMMed;
                medico.ufCRM = ESocial2220.UfCRMMed;
                medico.nisMed = ESocial2220.NisMed;
                medico.cpfMed = ESocial2220.CpfMed;

                aso.medico = medico;
            }

            respMonit respMonit = new respMonit();
            respMonit.cpfResp = ESocial2220.CpfResp;
            respMonit.nmResp = ESocial2220.NmResp;
            respMonit.nrCRM = ESocial2220.NrCRMResp;
            respMonit.ufCRM = ESocial2220.UfCRMResp;

            exMedOcup exMedOcup = new exMedOcup();
            exMedOcup.tpExameOcup = ESocial2220.TpExameOcup;
            exMedOcup.aso = aso;
            exMedOcup.respMonit = respMonit;

            evtMonit evtMonit = new evtMonit();
            evtMonit.Id = Id;
            evtMonit.ideEvento = ideEvento;
            evtMonit.ideEmpregador = ideEmpregador;
            evtMonit.ideVinculo = ideVinculo;
            evtMonit.exMedOcup = exMedOcup;

            ESocial eSocial = _unitOfWork.ESocial.Get(ESocial.Id);
            eSocial.IdEvento = Id;
            _unitOfWork.Complete();

            eSocial2220 eSocial2220 = new eSocial2220();
            eSocial2220.evtMonit = evtMonit;

            evento evento = new evento();
            evento.Id = Id;
            evento.eSocial2220 = eSocial2220;

            String xmlAux = XmlConverter.ObjectToXml(evento).OuterXml;
            xmlAux = xmlAux.Substring(149, xmlAux.Length - 149);
            xmlAux = xmlAux.Substring(0, xmlAux.Length - 9);

            XElement evento2220 = XDocument.Parse(xmlAux).Root;

            XAttribute id = new XAttribute("Id", Id);
            XElement eventoSemAssinatura = new XElement("evento", id);
            eventoSemAssinatura.Add(evento2220);

            ESocial.IdEvento = Id;
            ESocial.XmlEvento = eventoSemAssinatura.ToString();

            if (certificado == null)
            {
                return eventoSemAssinatura;
            }
            else
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(evento2220.ToString(SaveOptions.DisableFormatting));

                string eventoAssinado = XmlSignatureHelper.SignXmlDoc(xml, certificado, 2).OuterXml;
                evento2220 = XElement.Parse(eventoAssinado);

                return evento2220;
            }
        }

        public XElement CreateEvent2240(ESocial ESocial, ESocial2240 ESocial2240, string Id, X509Certificate2 certificado)
        {
            ideEvento ideEvento = new ideEvento();
            ideEvento.tpAmb = int.Parse(ESocial.Empregador.Transmissor.TpAmb);
            ESocial.TpAmb = ESocial.Empregador.Transmissor.TpAmb;
            ideEvento.procEmi = UtilInfo.ideEventoProcEmi;
            ideEvento.verProc = UtilInfo.ideEventoVerProc;

            if (ESocial.Operacao == "I")
            {
                ideEvento.indRetif = "1";
            }
            else if (ESocial.Operacao == "U")
            {
                ideEvento.indRetif = "2";
                ESocial.NrReciboOriginal = _unitOfWork.ESocial.GetRecibo(ESocial);
                ideEvento.nrRecibo = ESocial.NrReciboOriginal;
            }
            else
            {
                return createEvent3000(ESocial, ESocial2240.Funcionario, Id, certificado, "S-2240");
            }

            ideEmpregador ideEmpregador = new ideEmpregador();
            ideEmpregador.tpInsc = ESocial.Empregador.TpInsc;
            ideEmpregador.nrInsc = ESocial.NrInsc.Substring(0, 8);

            ideVinculo ideVinculo = new ideVinculo();
            ideVinculo.cpfTrab = ESocial.ESocial2240.CpfTrab;
            ideVinculo.codCateg = ESocial.ESocial2240.Funcionario.CodCateg;
            ideVinculo.matricula = ESocial.ESocial2240.Funcionario.Matricula;
            ideVinculo.nisTrab = ESocial.ESocial2240.Funcionario.Nis;

            List<infoAmb> infoAmb = new List<infoAmb>();
            List<ESocial2240InfoAmb> listInfoAmb = ESocial2240.ESocial2240InfoAmb;
            for (var a = 0; a < listInfoAmb.Count; a++)
            {
                ESocial2240InfoAmb ESocial2240InfoAmb = listInfoAmb[a];
                infoAmb infoAmbObj = new infoAmb();
                infoAmbObj.localAmb = ESocial2240InfoAmb.LocalAmb;
                infoAmbObj.dscSetor = ESocial2240InfoAmb.DscSetor;
                infoAmbObj.tpInsc = ESocial2240InfoAmb.TpInsc;
                infoAmbObj.nrInsc = ESocial2240InfoAmb.NrInsc;
                infoAmb.Add(infoAmbObj);
            }

            List<ativPericInsal> ativPericInsal = new List<ativPericInsal>();
            List<ESocial2240AtivPericInsal> listAtivPericInsal = ESocial2240.ESocial2240AtivPericInsal;
            for (var b = 0; b < listAtivPericInsal.Count; b++)
            {
                ESocial2240AtivPericInsal ESocial2240AtivPericInsal = listAtivPericInsal[b];
                ativPericInsal ativPericInsalObj = new ativPericInsal();
                ativPericInsalObj.codAtiv = ESocial2240AtivPericInsal.CodAtiv;
                ativPericInsal.Add(ativPericInsalObj);
            }

            infoAtiv infoAtiv = new infoAtiv();
            infoAtiv.dscAtivDes = ESocial2240.DscAtivDes;
            infoAtiv.ativPericInsal = ativPericInsal;

            List<fatRisco> fatRisco = new List<fatRisco>();
            List<ESocial2240FatRisco> listFatRisco = ESocial2240.ESocial2240FatRisco;
            for (var c = 0; c < listFatRisco.Count; c++)
            {
                ESocial2240FatRisco ESocial2240FatRisco = listFatRisco[c];
                epcEpi epcEpi = new epcEpi();
                epcEpi.utilizEPC = ESocial2240FatRisco.UtilizEPC;
                epcEpi.utilizEPI = ESocial2240FatRisco.UtilizEPI;
                epcEpi.eficEpc = ESocial2240FatRisco.EficEpc;

                List<ESocial2240FatRiscoEpi> listFatRiscoEpi = ESocial2240FatRisco.ESocial2240FatRiscoEpi;
                List<epi> epi = new List<epi>();
                for (var d = 0; d < listFatRiscoEpi.Count; d++)
                {
                    ESocial2240FatRiscoEpi ESocial2240FatRiscoEpi = listFatRiscoEpi[d];
                    epi epiObj = new epi();

                    epiObj.docAval = ESocial2240FatRiscoEpi.CaEPI;
                    epiObj.dscEPI = ESocial2240FatRiscoEpi.DscEPI;
                    epiObj.eficEpi = ESocial2240FatRiscoEpi.EficEpi;
                    /*epiObj.medProtecao = ESocial2240FatRiscoEpi.MedProtecao;
                    epiObj.condFuncto = ESocial2240FatRiscoEpi.CondFuncto;
                    epiObj.usoInint = ESocial2240FatRiscoEpi.UsoInint;
                    epiObj.przValid = ESocial2240FatRiscoEpi.PrzValid;
                    epiObj.periodicTroca = ESocial2240FatRiscoEpi.PeriodicTroca;
                    epiObj.higienizacao = ESocial2240FatRiscoEpi.Higienizacao;*/

                    epi.Add(epiObj);
                }
                epcEpi.epi = epi;

                epcEpi.epiCompl = new epiCompl();
                epcEpi.epiCompl.medProtecao = ESocial2240FatRisco.MedProtecao;
                epcEpi.epiCompl.condFuncto = ESocial2240FatRisco.CondFuncto;
                epcEpi.epiCompl.usoInint = ESocial2240FatRisco.UsoInint;
                epcEpi.epiCompl.przValid = ESocial2240FatRisco.PrzValid;
                epcEpi.epiCompl.periodicTroca = ESocial2240FatRisco.PeriodicTroca;
                epcEpi.epiCompl.higienizacao = ESocial2240FatRisco.Higienizacao;

                fatRisco fatRiscoObj = new fatRisco();
                fatRiscoObj.codAgNoc = ESocial2240FatRisco.CodFatRis;
                fatRiscoObj.tpAval = ESocial2240FatRisco.TpAval;
                fatRiscoObj.intConc = ESocial2240FatRisco.IntConc;
                fatRiscoObj.limTol = ESocial2240FatRisco.LimTol;
                fatRiscoObj.unMed = ESocial2240FatRisco.UnMed;
                fatRiscoObj.tecMedicao = ESocial2240FatRisco.TecMedicao;

                /*fatRiscoObj.insalubridade = ESocial2240FatRisco.Insalubridade;
                fatRiscoObj.periculosidade = ESocial2240FatRisco.Periculosidade;
                fatRiscoObj.aposentEsp = ESocial2240FatRisco.AposentEsp;*/
                fatRiscoObj.epcEpi = epcEpi;

                fatRisco.Add(fatRiscoObj);
            }

            List<respReg> respReg = new List<respReg>();
            List<ESocial2240RespReg> listRespReg = ESocial2240.ESocial2240RespReg;
            for (var e = 0; e < listRespReg.Count; e++)
            {
                ESocial2240RespReg ESocial2240RespReg = listRespReg[e];

                respReg respRegObj = new respReg();
                respRegObj.cpfResp = ESocial2240RespReg.CpfResp;
                respRegObj.nisResp = ESocial2240RespReg.NisResp;
                respRegObj.nmResp = ESocial2240RespReg.NmResp;
                respRegObj.ideOC = ESocial2240RespReg.IdeOC;
                respRegObj.dscOC = ESocial2240RespReg.DscOC;
                respRegObj.nrOC = ESocial2240RespReg.NrOC;
                respRegObj.ufOC = ESocial2240RespReg.UfOC;

                respReg.Add(respRegObj);
            }


            obs obs = new obs();
            obs.metErg = ESocial2240.MetErg;
            obs.obsCompl = ESocial2240.ObsCompl;


            infoExpRisco infoExpRisco = new infoExpRisco();
            infoExpRisco.dtIniCondicao = ESocial2240.DtIniCondicao.ToString("yyyy-MM-dd");
            infoExpRisco.infoAmb = infoAmb;
            infoExpRisco.infoAtiv = infoAtiv;
            infoExpRisco.fatRisco = fatRisco;
            infoExpRisco.respReg = respReg;
            infoExpRisco.obs = obs;

            evtExpRisco evtExpRisco = new evtExpRisco();
            evtExpRisco.Id = Id;
            evtExpRisco.ideEmpregador = ideEmpregador;
            evtExpRisco.ideEvento = ideEvento;
            evtExpRisco.ideVinculo = ideVinculo;
            evtExpRisco.infoExpRisco = infoExpRisco;

            ESocial eSocial = _unitOfWork.ESocial.Get(ESocial.Id);
            eSocial.IdEvento = Id;
            _unitOfWork.Complete();

            eSocial2240 eSocial2240 = new eSocial2240();
            eSocial2240.evtExpRisco = evtExpRisco;


            evento evento = new evento();
            evento.Id = Id;
            evento.eSocial2240 = eSocial2240;

            String xmlAux = XmlConverter.ObjectToXml(evento).OuterXml;
            xmlAux = xmlAux.Substring(149, xmlAux.Length - 149);
            xmlAux = xmlAux.Substring(0, xmlAux.Length - 9);

            XElement evento2240 = XDocument.Parse(xmlAux).Root;

            XAttribute id = new XAttribute("Id", Id);
            XElement eventoSemAssinatura = new XElement("evento", id);
            eventoSemAssinatura.Add(evento2240);

            ESocial.IdEvento = Id;
            ESocial.XmlEvento = eventoSemAssinatura.ToString();

            if (certificado == null)
            {
                return eventoSemAssinatura;
            }
            else
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(evento2240.ToString(SaveOptions.DisableFormatting));

                string eventoAssinado = XmlSignatureHelper.SignXmlDoc(xml, certificado, 2).OuterXml;
                evento2240 = XElement.Parse(eventoAssinado);

                return evento2240;
            }
        }

        public XElement createEvent3000(ESocial ESocial, Funcionario Funcionario, string Id, X509Certificate2 certificado, string tpEvento)
        {
            XNamespace ns = "http://www.esocial.gov.br/schema/evt/evtExclusao/v02_05_00";
            XAttribute id = new XAttribute("Id", Id);
            XElement evento3000 = new XElement(ns + "eSocial");
            XElement evtExclusao = new XElement(ns + "evtExclusao", id);

            XElement ideEvento = new XElement(ns + "ideEvento");
            ideEvento.Add(new XElement(ns + "tpAmb", "2"));
            ideEvento.Add(new XElement(ns + "procEmi", "1"));
            ideEvento.Add(new XElement(ns + "verProc", "1"));

            XElement ideEmpregador = new XElement(ns + "ideEmpregador");
            ideEmpregador.Add(new XElement(ns + "tpInsc", ESocial.Empregador.TpInsc));
            ideEmpregador.Add(new XElement(ns + "nrInsc", ESocial.NrInsc.Substring(0, 8)));

            XElement infoExclusao = new XElement(ns + "infoExclusao");
            infoExclusao.Add(new XElement(ns + "tpEvento", tpEvento));
            ESocial.NrReciboOriginal = _unitOfWork.ESocial.GetRecibo(ESocial);
            infoExclusao.Add(new XElement(ns + "nrRecEvt", ESocial.NrReciboOriginal));

            XElement ideTrabalhador = new XElement(ns + "ideTrabalhador");
            ideTrabalhador.Add(new XElement(ns + "cpfTrab", Funcionario.CPF));
            ideTrabalhador.Add(new XElement(ns + "nisTrab", Funcionario.Nis));

            evtExclusao.Add(ideEvento);
            evtExclusao.Add(ideEmpregador);
            infoExclusao.Add(ideTrabalhador);
            evtExclusao.Add(infoExclusao);
            evento3000.Add(evtExclusao);


            XmlDocument xml = new XmlDocument();
            xml.LoadXml(evento3000.ToString(SaveOptions.DisableFormatting));

            string eventoAssinado = XmlSignatureHelper.SignXmlDoc(xml, certificado, 2).OuterXml;
            evento3000 = XElement.Parse(eventoAssinado);

            ESocial.IdEvento = Id;

            return evento3000;
        }
    }
}
