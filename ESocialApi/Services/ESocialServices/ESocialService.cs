﻿using ESocialApi.DB.Model;
using ESocialApi.Interfaces;
using ESocialApi.Interfaces.ESocialTransmissao;
using ESocialApi.Interfaces.Repository;
using ESocialApi.Services.EmailServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESocialApi.Services.ESocialServices
{
    public class ESocialService : IESocialService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICreateLotEvents _createEvent;

        public ESocialService(IUnitOfWork unitOfWork, ICreateLotEvents createEvent)
        {
            _unitOfWork = unitOfWork;
            _createEvent = createEvent;

        }
        public int GenerateEvents(ESocial obj)
        {
            try
            {
                ESocial ESocial = _unitOfWork.ESocial.GetEventoPendente(obj);
                bool insert = false;

                if (ESocial == null)
                {
                    ESocial = new ESocial();
                    ESocial.TpEvento = obj.TpEvento;
                    ESocial.Operacao = obj.Operacao;
                    insert = true;
                }
                else
                {
                    if (obj.Operacao.Equals("D") && ESocial.Operacao.Equals("I"))
                    {
                        _unitOfWork.ESocial.Remove(ESocial);
                        _unitOfWork.Complete();
                        return 0;
                    }
                    else if (obj.Operacao.Equals("D"))
                    {
                        ESocial.Operacao = obj.Operacao;
                    }
                }
                ESocial.DataEvento = DateTime.Now;
                ESocial.NrInsc = obj.NrInsc;
                ESocial.StatusTransmissao = "P";
                checkEmpregador(obj.Empregador);

                if (obj.TpEvento.Equals("2210"))
                {
                    #region 2210
                    if (insert)
                    {
                        _unitOfWork.ESocial.Add(ESocial);
                        _unitOfWork.Complete();
                        ESocial.ESocial2210 = obj.ESocial2210;
                        ESocial.ESocial2210.Id = ESocial.Id;
                    }
                    else
                    {
                        _unitOfWork.ESocial2210.Remove(ESocial.ESocial2210);
                        _unitOfWork.Complete();
                        ESocial.ESocial2210 = obj.ESocial2210;
                        ESocial.ESocial2210.Id = ESocial.Id;
                        _unitOfWork.ESocial2210.Add(ESocial.ESocial2210);
                    }

                    checkFuncionario(obj.ESocial2210.Funcionario);
                    #endregion
                }
                else if (obj.TpEvento.Equals("2220"))
                {
                    #region 2220
                    if (insert)
                    {
                        _unitOfWork.ESocial.Add(ESocial);
                        _unitOfWork.Complete();
                        ESocial.ESocial2220 = obj.ESocial2220;
                        ESocial.ESocial2220.Id = ESocial.Id;
                    }
                    else
                    {
                        _unitOfWork.ESocial2220.Remove(ESocial.ESocial2220);
                        _unitOfWork.Complete();
                        ESocial.ESocial2220 = obj.ESocial2220;
                        ESocial.ESocial2220.Id = ESocial.Id;
                        _unitOfWork.ESocial2220.Add(ESocial.ESocial2220);
                    }

                    checkFuncionario(obj.ESocial2220.Funcionario);
                    #endregion
                }
                else if (obj.TpEvento.Equals("2240"))
                {
                    #region 2240
                    if (insert)
                    {
                        _unitOfWork.ESocial.Add(ESocial);
                        _unitOfWork.Complete();
                        ESocial.ESocial2240 = obj.ESocial2240;
                        ESocial.ESocial2240.Id = ESocial.Id;
                    }
                    else
                    {
                        _unitOfWork.ESocial2240.Remove(ESocial.ESocial2240);
                        _unitOfWork.Complete();
                        ESocial.ESocial2240 = obj.ESocial2240;
                        ESocial.ESocial2240.Id = ESocial.Id;
                        _unitOfWork.ESocial2240.Add(ESocial.ESocial2240);
                    }

                    checkFuncionario(obj.ESocial2240.Funcionario);
                    #endregion
                }

                _unitOfWork.Complete();
                return ESocial.Id;
            }
            catch (Exception ex)
            {
                LogErro logErro = new LogErro();
                logErro.DataErro = DateTime.Now;
                logErro.Message = "id:" + obj.Id + " tp:" + obj.TpEvento + " - " + ex.Message;
                logErro.StackTrace = ex.ToString();

                _unitOfWork.LogErro.Add(logErro);
                _unitOfWork.Complete();
            }
            return 0;
        }

        private void checkEmpregador(Empregador EmpregadorNovo)
        {
            if (EmpregadorNovo != null)
            {
                Empregador Empregador = _unitOfWork.Empregador.Get(EmpregadorNovo.NrInsc);
                if (Empregador != null)
                {
                    Empregador.RazaoSocial = EmpregadorNovo.RazaoSocial;
                    Empregador.NrInscTransmissor = EmpregadorNovo.NrInscTransmissor;
                    Empregador.Transmitir = EmpregadorNovo.Transmitir;
                    Empregador.EnviarEmail = EmpregadorNovo.EnviarEmail;
                    //Empregador.Emails = EmpregadorNovo.Emails;
                }
                else
                {
                    _unitOfWork.Empregador.Add(EmpregadorNovo);
                }

            }
        }

        private void checkFuncionario(Funcionario FuncionarioNovo)
        {
            if (FuncionarioNovo != null)
            {
                Funcionario Funcionario = _unitOfWork.Funcionario.Get(FuncionarioNovo.CPF);
                if (Funcionario != null)
                {
                    Funcionario.Nome = FuncionarioNovo.Nome;
                    Funcionario.Nis = FuncionarioNovo.Nis;
                    Funcionario.Matricula = FuncionarioNovo.Matricula;
                    Funcionario.CodCateg = FuncionarioNovo.CodCateg;
                }
                else
                {
                    _unitOfWork.Funcionario.Add(FuncionarioNovo);
                }

            }
        }

        public async void EnviarEmailEventos()
        {
            try
            {
                List<ESocial> eventos = _unitOfWork.ESocial.ObtemEventosNaoEnviadosEmailPorEmpresa();
                for (var i = 0; i < eventos.Count; i++)
                {
                    ESocial ESocial = _unitOfWork.ESocial.GetESocial(eventos[i].Id);
                    _createEvent.GetXmlEvent(ESocial);

                    PageHTML page = new PageHTML();
                    var to = ESocial.Empregador.Emails;
                    var subject = "Evento " + ESocial.IdEvento;
                    StringBuilder dados = new StringBuilder();

                    dados.Append("<p>").Append("Data do Evento: ").Append(ESocial.DataEvento).Append("</p>");
                    dados.Append("<p style=\"font - size: small;\"> ").Append("XML do Evento em Anexo.").Append("</p>");

                    var body = page.Head + page.BeginBody + page.Title.Replace("[titulo]", "API Agrat e-Social") + page.Card.Replace("[tpEvento]", ESocial.TpEvento).Replace("[idEvento]", ESocial.IdEvento).Replace("<p>[resposta]</p>", dados.ToString()) + page.Footer + page.EndBody;

                    byte[] fileContent = Encoding.ASCII.GetBytes(ESocial.XmlEvento);
                    var fileName = ESocial.IdEvento + "_" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + ".xml";
                    var fileType = "application/xml";

                    List<AttachmentInfo> listaAttachments = new List<AttachmentInfo>() { new AttachmentInfo(fileName, fileContent, fileType) };

                    var messageInfo = new EmailMessageInfo(to, subject, body, listaAttachments);

                    var sendGridHelper = new SendGridHelper();
                    var mensagemEnviada = await sendGridHelper.Send(messageInfo);
                    if (mensagemEnviada)
                    {
                        ESocial.StatusEmail = true;
                    }
                    else
                    {
                        LogErro logErro = new LogErro();
                        logErro.DataErro = DateTime.Now;
                        logErro.Message = "Erro ao enviar e-mail";
                        logErro.StackTrace = logErro.Message;

                        _unitOfWork.LogErro.Add(logErro);
                    }

                }
                _unitOfWork.Complete();
            }
            catch (Exception ex)
            {
                LogErro logErro = new LogErro();
                logErro.DataErro = DateTime.Now;
                logErro.Message = ex.Message;
                logErro.StackTrace = ex.ToString();

                _unitOfWork.LogErro.Add(logErro);
                _unitOfWork.Complete();
            }
        }
    }
}
