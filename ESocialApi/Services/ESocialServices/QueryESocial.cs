﻿using ESocialApi.DB.Model;
using ESocialApi.Helper.Xml;
using ESocialApi.Interfaces.ESocialTransmissao;
using ESocialApi.Interfaces.Repository;
using ESocialApi.Services.EmailServices;
using ESocialApi.Wrappers;
using ESocialApi.Xml.Model.RetornoConsulta;
using ESocialApi.Xml.Model.RetornoConsulta_New;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ESocialApi.Services.ESocialServices
{
    public class QueryESocial : IQueryESocial
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoteRepository _lotReposistory;
        private readonly IOcorrenciaRepository _ocorrencia;
        private readonly ILogErroRepository _logErro;

        public QueryESocial(ILoteRepository lotReposistory, IUnitOfWork unitOfWork, IOcorrenciaRepository ocorrencia, ILogErroRepository logErro)
        {
            _unitOfWork = unitOfWork;
            _lotReposistory = lotReposistory;
            _ocorrencia = ocorrencia;
            _logErro = logErro;
        }

        public async void QueryEvents()
        {
            //Busca todos os lotes enviados com sucesso (Código 201)
            List<Lote> LotList = _unitOfWork.Lote.GetSendedLots();

            Transmissor Transmissor = LotList[0].ESocial[0].Empregador.Transmissor;

            var tasks = new List<Task>();

            using (var semaphore = new SemaphoreSlim(5))
            {
                foreach (Lote lot in LotList)
                {
                    await semaphore.WaitAsync();
                    tasks.Add(MakeRequestESocial(semaphore, Transmissor, lot));
                }
                if (LotList.Count > 0 && LotList[0] != null)
                {
                    await Task.WhenAll(tasks);
                }
            }

            _unitOfWork.Complete();
        }

        public async void QueryEvents(Transmissor Transmissor, Hashtable lotes)
        {
            var tasks = new List<Task>();

            using (var semaphore = new SemaphoreSlim(5))
            {
                foreach (DictionaryEntry item in lotes)
                {
                    await semaphore.WaitAsync();
                    Lote lote = _unitOfWork.Lote.Get(Int32.Parse(item.Key.ToString()));
                    tasks.Add(MakeRequestESocial(semaphore, Transmissor, lote));
                }
                if (lotes.Count > 0 && lotes[0] != null)
                {
                    await Task.WhenAll(tasks);
                }
            }
            _unitOfWork.Complete();
        }

        private Task MakeRequestESocial(SemaphoreSlim semaphore, Transmissor Transmissor, Lote lot)
        {
            try
            {
                XElement retornoXml = ESocialServiceWrapper.consultarLoteEventos(Transmissor, XmlQuery(lot.ProtocoloEnvio)).Result;
                tageSocial ObjRetorno = XmlConverter.XmlToObject<tageSocial>(retornoXml.ToString());

                lot.CdRespostaConsulta = ObjRetorno.tagRetornoProcessamentoLoteEventos.tagStatus.cdResposta;

                if (lot.CdRespostaConsulta != 101)
                {
                    Ocorrencia Ocorrencia;
                    List<Ocorrencia> Ocorencias;

                    if (ObjRetorno.tagRetornoProcessamentoLoteEventos.tagRetornoEventos != null)
                    {
                        List<tagEvento> _eventos = ObjRetorno.tagRetornoProcessamentoLoteEventos.tagRetornoEventos.tagEvento;

                        foreach (var evento in _eventos)
                        {
                            ESocial EventList = _unitOfWork.ESocial.ObtemEventoPorId(evento.Id);

                            //Deleta as Ocorrencias antigas do Evento
                            _ocorrencia.DeleteOcorrencias(EventList.Id);

                            tagProcessamento tagProcessamento = evento.tagRetornoEvento.tageSocial2.tagRetornoEvento2.tagProcessamento;
                            string cdRespostaProcessamento = tagProcessamento.cdResposta;
                            ESocialProcessamento ESocialProcessamento = new ESocialProcessamento();
                            ESocialProcessamento.IdESocial = EventList.Id;
                            ESocialProcessamento.CdResposta = int.Parse(cdRespostaProcessamento);
                            ESocialProcessamento.DescResposta = tagProcessamento.descResposta;
                            ESocialProcessamento.DhProcessamento = tagProcessamento.dhProcessamento;
                            ESocialProcessamento.VersaoAppProcessamento = tagProcessamento.versaoAppProcessamento;
                            if (EventList.ESocialProcessamento != null)
                            {
                                EventList.ESocialProcessamento.Add(ESocialProcessamento);
                            }
                            else
                            {
                                List<ESocialProcessamento> processos = new List<ESocialProcessamento>();
                                processos.Add(ESocialProcessamento);
                                EventList.ESocialProcessamento = processos;
                            }


                            if (cdRespostaProcessamento.Equals("201"))
                            {

                                tagRecibo recibo = evento.tagRetornoEvento.tageSocial2.tagRetornoEvento2.recibo;
                                EventList.NrRecibo = recibo.nrRecibo;
                                //EventList.HashRecibo = recibo.hash;

                                EventList.StatusTransmissao = "F";
                            }
                            else
                            {
                                PageHTML page = new PageHTML();
                                string subject = "", body = "";
                                if (cdRespostaProcessamento.Equals("401") || cdRespostaProcessamento.Equals("402") || cdRespostaProcessamento.Equals("403"))
                                {
                                    Ocorencias = new List<Ocorrencia>();
                                    List<tagOcorrencia> tag = evento.tagRetornoEvento.tageSocial2.tagRetornoEvento2.tagProcessamento.tagOcorrencias.tagOcorrencia;
                                    if (tag.Count > 0)
                                    {
                                        StringBuilder oc = new StringBuilder();
                                        EventList.StatusTransmissao = "O";
                                        for (int i2 = 0; i2 < tag.Count; i2++)
                                        {
                                            Ocorrencia = new Ocorrencia();
                                            Ocorrencia.Id = tag[i2].Id;
                                            Ocorrencia.Codigo = tag[i2].codigo;
                                            Ocorrencia.Descricao = tag[i2].descricao;
                                            Ocorrencia.Tipo = tag[i2].tipo;
                                            Ocorrencia.Localizacao = tag[i2].localizacao;
                                            Ocorencias.Add(Ocorrencia);

                                            oc.Append("<p>");
                                            oc.Append(tag[i2].codigo).Append(" - ").Append(tag[i2].descricao);
                                            if (tag[i2].localizacao != null && !tag[i2].localizacao.Trim().Equals(""))
                                            {
                                                oc.Append(" Localização: ").Append(tag[i2].localizacao);
                                            }
                                            oc.Append("</p>");
                                        }
                                        EventList.Ocorencias = Ocorencias;

                                        string complemento = page.DataEvento.Replace("[DataEvento]", EventList.DataEvento + "") + page.Ambiente.Replace("[Ambiente]", EventList.TpAmb.Equals("1") ? "Produção" : "Homologação") + oc.ToString();
                                        subject = "Evento " + EventList.TpEvento + " com ocorrências";
                                        body = page.Head + page.BeginBody + page.Title.Replace("[titulo]", "Evento com Ocorrências") + page.Card.Replace("[tpEvento]", EventList.TpEvento).Replace("[idEvento]", EventList.IdEvento).Replace("<p>[resposta]</p>", complemento) + page.Footer + page.EndBody;


                                    }
                                }
                                else
                                {
                                    subject = "Evento " + EventList.TpEvento + " com erro";
                                    string complemento = page.DataEvento.Replace("[DataEvento]", EventList.DataEvento + "") + page.Ambiente.Replace("[Ambiente]", EventList.TpAmb.Equals("1") ? "Produção" : "Homologação") + "<p>" + tagProcessamento.cdResposta + " - " + tagProcessamento.descResposta + "</p>";
                                    body = page.Head + page.BeginBody + page.Title.Replace("[titulo]", "Evento com Erro") + page.Card.Replace("[tpEvento]", EventList.TpEvento).Replace("[idEvento]", EventList.IdEvento).Replace("<p>[resposta]</p>", complemento) + page.Footer + page.EndBody;

                                    EventList.StatusTransmissao = "E";
                                }
                                var to = EventList.Empregador.Emails;
                                byte[] fileContent = Encoding.ASCII.GetBytes(EventList.XmlEvento);
                                var fileName = EventList.IdEvento + "_" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + ".xml";
                                var fileType = "application/xml";

                                List<AttachmentInfo> listaAttachments = new List<AttachmentInfo>() { new AttachmentInfo(fileName, fileContent, fileType) };

                                var messageInfo = new EmailMessageInfo(to, subject, body, listaAttachments);

                                var sendGridHelper = new SendGridHelper();

                                var mensagemEnviada = sendGridHelper.Send(messageInfo);
                            }
                        }
                        _unitOfWork.Complete();
                    }
                    else
                    {
                        List<ESocial> EventList = _unitOfWork.ESocial.ObtemEventosPorLote(lot.Id);
                        EventList.Where(e => e.Lote.NLote == lot.NLote);

                        for (int i = 0; i < EventList.Count; i++)
                        {
                            Ocorencias = new List<Ocorrencia>();
                            List<tagOcorrencia> tag = ObjRetorno.tagRetornoProcessamentoLoteEventos.tagStatus.tagOcorrencias.tagOcorrencia;
                            for (int i2 = 0; i2 < tag.Count; i2++)
                            {
                                Ocorrencia = new Ocorrencia();
                                Ocorrencia.Id = tag[i2].Id;
                                Ocorrencia.Codigo = tag[i2].codigo;
                                Ocorrencia.Descricao = tag[i2].descricao;
                                Ocorrencia.Tipo = tag[i2].tipo;
                                Ocorencias.Add(Ocorrencia);
                            }
                            EventList[i].Ocorencias = Ocorencias;
                        }
                        _unitOfWork.Complete();
                    }

                }
                if (lot.CdRespostaConsulta == 101)
                {
                    Task.Delay(5000);

                    MakeRequestESocial(semaphore, Transmissor, lot);
                }

                return null;
            }
            catch (Exception ex)
            {
                LogErro logErro = new LogErro();
                logErro.DataErro = DateTime.Now;
                logErro.Message = ex.Message;
                logErro.StackTrace = ex.ToString();

                _logErro.Add(logErro);
                _unitOfWork.Complete();
                return null;
            }
            finally
            {
                semaphore.Release();
            }
        }

        private static XElement XmlQuery(string protocolo)
        {
            StringBuilder s = new StringBuilder();
            s.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\">");
            s.Append("<soap:Header/>");
            s.Append("<soap:Body>");
            s.Append("<eSocial xmlns=\"http://www.esocial.gov.br/schema/lote/eventos/envio/consulta/retornoProcessamento/v1_0_0\">");
            s.Append("<consultaLoteEventos>");
            s.Append($"<protocoloEnvio>{ protocolo }</protocoloEnvio>");
            s.Append("</consultaLoteEventos>");
            s.Append("</eSocial>");
            s.Append("</soap:Body>");
            s.Append("</soap:Envelope>");

            return XElement.Parse(s.ToString());
        }
    }
}
