﻿using ESocialApi.DB.Model;
using ESocialApi.Helper.Xml;
using ESocialApi.Interfaces.ESocialTransmissao;
using ESocialApi.Interfaces.Repository;
using ESocialApi.Wrappers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using Xml.Model.RetornoEnvio;

namespace ESocialApi.Services.ESocialServices
{
    public class PostEvents : IPostEvents
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICreateLotEvents _createLotEvents;
        private readonly IQueryESocial _queryLotResults;

        public PostEvents(IUnitOfWork unitOfWork, ICreateLotEvents createLotEvents, IQueryESocial queryLotResults)
        {
            _unitOfWork = unitOfWork;
            _createLotEvents = createLotEvents;
            _queryLotResults = queryLotResults;
        }

        //Faz um loop em cima da lista de hashtables. Cada item da hastable coresponde a um lote e cada objeto da lista coresponste a um lote de lotes de uma empresa
        public bool TransmitirLoteEventos(Hashtable Events, Transmissor Transmissor)
        {
            try
            {
                using (var semafore = new SemaphoreSlim(10))
                {
                    foreach (DictionaryEntry item in Events)
                    {
                        XElement xelement = (XElement)item.Value;
                        string x = xelement.ToString(SaveOptions.DisableFormatting);
                        XElement response = ESocialServiceWrapper.enviarLoteEventosAsync(Transmissor, xelement).Result;

                        //Deserializa Xml de retorno
                        eSocial eSocialResponse = XmlConverter.XmlToObject<eSocial>(response.ToString());

                        if (eSocialResponse != null)
                        {
                            Lote ObjLote = _unitOfWork.Lote.Get(System.Int32.Parse(item.Key.ToString()));

                            if (eSocialResponse.retornoEnvioLoteEventos.dadosRecepcaoLote != null)
                            {
                                ObjLote.ProtocoloEnvio = eSocialResponse.retornoEnvioLoteEventos.dadosRecepcaoLote.protocoloEnvio;
                                ObjLote.DhRecepcaoEnvio = eSocialResponse.retornoEnvioLoteEventos.dadosRecepcaoLote.dhRecepcao.ToString();
                                ObjLote.VersaoAppRecepcao = eSocialResponse.retornoEnvioLoteEventos.dadosRecepcaoLote.versaoAplicativoRecepcao;
                            }
                            ObjLote.CdRespostaEnvio = eSocialResponse.retornoEnvioLoteEventos.status.cdResposta;
                            ObjLote.DescRespostaEnvio = eSocialResponse.retornoEnvioLoteEventos.status.descResposta;
                            _unitOfWork.Complete();
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogErro logErro = new LogErro();
                logErro.DataErro = DateTime.Now;
                logErro.Message = ex.Message;
                logErro.StackTrace = ex.ToString();

                _unitOfWork.LogErro.Add(logErro);
                _unitOfWork.Complete();
                
            }
            return false;
        }

        public async void TransmitirEventos()
        {
            try
            {
                var Empresas = _unitOfWork.ESocial.ObtemEmpresasComEventosNaoTrasmitidos();

                foreach (string NrInsc in Empresas)
                {
                    var listaEventos = _unitOfWork.ESocial.ObtemEventosNaoEnviadosPorEmpresa(NrInsc);

                    List<ESocial> eventos1060 = listaEventos
                    .Where(e => e.TpEvento.Equals("1060")).Take(50)
                    .ToList();

                    if (eventos1060.Any())
                    {
                        Transmissor Transmissor = eventos1060[0].Empregador.Transmissor;

                        Hashtable lotesEventos1060 = await _createLotEvents.CreateEventLot(eventos1060, 1);

                        var loteEnviado = TransmitirLoteEventos(lotesEventos1060, Transmissor);
                        if (loteEnviado)
                        {
                            _queryLotResults.QueryEvents(Transmissor, lotesEventos1060);
                        }
                    }

                    List<ESocial> eventosRestantes = listaEventos
                    .Where(e => !e.TpEvento.Equals("1060")).Take(50)
                    .ToList();

                    if (eventosRestantes.Any())
                    {
                        Transmissor Transmissor = eventosRestantes[0].Empregador.Transmissor;

                        Hashtable loteEventosRestantes = await _createLotEvents.CreateEventLot(eventosRestantes, 2);

                        var loteEnviado = TransmitirLoteEventos(loteEventosRestantes, Transmissor);
                        if (loteEnviado)
                        {
                            _queryLotResults.QueryEvents(Transmissor, loteEventosRestantes);
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                LogErro logErro = new LogErro();
                logErro.DataErro = DateTime.Now;
                logErro.Message = ex.Message;
                logErro.StackTrace = ex.ToString();

                _unitOfWork.LogErro.Add(logErro);
                _unitOfWork.Complete();
            }
        }
    }
}
