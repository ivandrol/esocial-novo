﻿using ESocialApi.Interfaces.Repository;
using ESocialApi.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using ESocialApi.Services.ESocialServices;
using ESocialApi.Interfaces.ESocialTransmissao;

namespace ESocialApi.Services
{
    public class RetriveEvents : IRetriveEvents
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueryESocial _queryLotResults;

        public RetriveEvents(IUnitOfWork unitOfWork, IQueryESocial queryLotResults)
        {
            _unitOfWork = unitOfWork;
            _queryLotResults = queryLotResults;
        }

        public async void GetAllEventsAsync()
        {
            /*List<Task> tasks = new List<Task>
            {
                GetS1060(),
                GetS2220(),
                GetS2240(),
                GetS2245()
            };*/
            _queryLotResults.QueryEvents();
        }

        public Task GetS1060()
        {
            return Task.CompletedTask;
        }

        public Task GetS2220()
        {
            return Task.CompletedTask;
        }

        public Task GetS2240()
        {
            return Task.CompletedTask;
        }

        public Task GetS2245()
        {
            return Task.CompletedTask;
        }
    }
}
