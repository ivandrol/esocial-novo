﻿using Newtonsoft.Json;
using System;

namespace ESocialApi.DB.Model
{
    public class ESocial2220Exame
    {
        public int Id { get; set; }
        public int IdESocial { get; set; }
        public DateTime DtExm { get; set; }
        public string ProcRealizado { get; set; }
        public string ObsProc { get; set; }
        public int OrdExame { get; set; }
        public string IndResult { get; set; }

        [JsonIgnore]
        public ESocial2220 ESocial2220 { get; set; }

        public ProcedimentoRealizado ProcedimentoRealizado { get; set; }

        public ESocial2220Exame()
        {

        }

    }
}