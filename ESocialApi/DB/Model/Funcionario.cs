﻿using Newtonsoft.Json;
using System.Collections.Generic;
namespace ESocialApi.DB.Model
{
    public class Funcionario
    {
        public string CPF { get; set; }
        public string Nome { get; set; }
        public string Nis { get; set; }
        public string Matricula { get; set; }
        public string CodCateg { get; set; }

        [JsonIgnore]
        public List<ESocial2210> ESocial2210 { get; set; }
        [JsonIgnore]
        public List<ESocial2220> ESocial2220 { get; set; }
        [JsonIgnore]
        public List<ESocial2240> ESocial2240 { get; set; }

        public CategoriaTrabalhador CategoriaTrabalhador { get; set; }

        public Funcionario()
        {

        }

        public Funcionario(string cpf, string nome, string nis, string matricula)
        {
            CPF = cpf;
            Nome = nome;
            Nis = nis;
            Matricula = matricula;
        }
    }
}
