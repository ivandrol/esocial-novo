﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class CategoriaTrabalhador
    {
        public string Id { get; set; }
        public string DescricaoReduzida { get; set; }
        public string Descricao { get; set; }
        public string Grupo { get; set; }

        [JsonIgnore]
        public List<Funcionario> Funcionario { get; set; }

        public CategoriaTrabalhador()
        {
            Funcionario = new List<Funcionario>();
        }
    }
}
