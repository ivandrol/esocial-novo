﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
namespace ESocialApi.DB.Model
{
    public class ESocial2240
    {
        public int Id { get; set; }
        public string CpfTrab { get; set; }
        public DateTime DtIniCondicao { get; set; }
        public string DscAtivDes { get; set; }
        public string MetErg { get; set; }
        public string ObsCompl { get; set; }

        public List<ESocial2240AtivPericInsal> ESocial2240AtivPericInsal { get; set; }
        public List<ESocial2240FatRisco> ESocial2240FatRisco { get; set; }
        public List<ESocial2240InfoAmb> ESocial2240InfoAmb { get; set; }
        public List<ESocial2240RespReg> ESocial2240RespReg { get; set; }

        [JsonIgnore]
        public ESocial ESocial { get; set; }

        public Funcionario Funcionario { get; set; }


        public ESocial2240()
        {
            ESocial2240AtivPericInsal = new List<ESocial2240AtivPericInsal>();
            ESocial2240FatRisco = new List<ESocial2240FatRisco>();
            ESocial2240InfoAmb = new List<ESocial2240InfoAmb>();
            ESocial2240RespReg = new List<ESocial2240RespReg>();
        }

    }
}
