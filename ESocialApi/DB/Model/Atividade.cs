﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class Atividade
    {
        public string Id { get; set; }
        public string DescricaoReduzida { get; set; }
        public string Descricao { get; set; }
        public string Grupo { get; set; }

        [JsonIgnore]
        public List<ESocial2240AtivPericInsal> ESocial2240AtivPericInsal { get; set; }

        public Atividade()
        {
            ESocial2240AtivPericInsal = new List<ESocial2240AtivPericInsal>();
        }
    }
}
