﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ESocialApi.DB.Model
{
    public class ESocial
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string TpAmb { get; set; }
        public string TpEvento { get; set; }
        public DateTime DataEvento { get; set; }
        public int? IdLote { get; set; }
        public string NrInsc { get; set; }
        public string Operacao { get; set; }
        public string StatusTransmissao { get; set; }
        public bool StatusEmail { get; set; }
        public bool StatusGet { get; set; }
        public string IdEvento { get; set; }
        public string NrRecibo { get; set; }
        public string NrReciboOriginal { get; set; }
        public string XmlEvento { get; set; }

        public ESocial2210 ESocial2210 { get; set; }
        public ESocial2220 ESocial2220 { get; set; }
        public ESocial2240 ESocial2240 { get; set; }
        public List<ESocialProcessamento> ESocialProcessamento { get; set; }
        public List<Ocorrencia> Ocorencias { get; set; }

        public Empregador Empregador { get; set; }
        public Lote Lote { get; set; }

        public ESocial()
        {
            ESocialProcessamento = new List<ESocialProcessamento>();
            Ocorencias = new List<Ocorrencia>();
        }

        public ESocial(string tpEvento, string nrInsc, Empregador empregador, string operacao)
        {
            TpEvento = tpEvento;
            NrInsc = nrInsc;
            Empregador = empregador;
            Operacao = operacao;
            ESocialProcessamento = new List<ESocialProcessamento>();
            Ocorencias = new List<Ocorrencia>();
        }
    }
}
