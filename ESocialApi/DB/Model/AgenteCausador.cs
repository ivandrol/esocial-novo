﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class AgenteCausador
    {
        public string Id { get; set; }
        public string DescricaoReduzida { get; set; }
        public string Descricao { get; set; }
        public string Grupo { get; set; }

        [JsonIgnore]
        public List<ESocial2210AgenteCausador> ESocial2210AgenteCausador { get; set; }

        public AgenteCausador()
        {
            ESocial2210AgenteCausador = new List<ESocial2210AgenteCausador>();
        }
    }
}
