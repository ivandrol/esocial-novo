﻿using Newtonsoft.Json;

namespace ESocialApi.DB.Model
{
    public class ESocial2240RespReg
    {
        public int Id { get; set; }
        public int IdESocial { get; set; }
        public string CpfResp { get; set; }
        public string NisResp { get; set; }
        public string NmResp { get; set; }
        public int IdeOC { get; set; }
        public string DscOC { get; set; }
        public string NrOC { get; set; }
        public string UfOC { get; set; }

        [JsonIgnore]
        public ESocial2240 ESocial2240 { get; set; }

        public ESocial2240RespReg()
        {

        }
    }
}
