﻿using Newtonsoft.Json;
namespace ESocialApi.DB.Model
{
    public class ESocial2210AgenteCausador
    {
        public int Id { get; set; }
        public int IdESocial { get; set; }
        public string CodAgntCausador { get; set; }

        [JsonIgnore]
        public ESocial2210 ESocial2210 { get; set; }

        public AgenteCausador AgenteCausador { get; set; }

        public ESocial2210AgenteCausador(string codAgntCausador)
        {
            CodAgntCausador = codAgntCausador;
        }

        public ESocial2210AgenteCausador()
        {

        }
    }
}
