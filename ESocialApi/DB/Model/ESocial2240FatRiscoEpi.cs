﻿using Newtonsoft.Json;

namespace ESocialApi.DB.Model
{
    public class ESocial2240FatRiscoEpi
    {
        public int Id { get; set; }
        public int IdFatRisco { get; set; }
        public string CaEPI { get; set; }
        public string DscEPI { get; set; }
        public string EficEpi { get; set; }

        [JsonIgnore]
        public ESocial2240FatRisco ESocial2240FatRisco { get; set; }

        public ESocial2240FatRiscoEpi()
        {

        }
    }
}
