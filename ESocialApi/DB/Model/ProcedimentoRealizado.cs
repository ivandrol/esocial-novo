﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class ProcedimentoRealizado
    {
        public string Id { get; set; }
        public string DescricaoReduzida { get; set; }
        public string Descricao { get; set; }

        [JsonIgnore]
        public List<ESocial2220Exame> ESocial2220Exame { get; set; }

        public ProcedimentoRealizado()
        {
            ESocial2220Exame = new List<ESocial2220Exame>();
        }
    }
}
