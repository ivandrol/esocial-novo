﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class Treinamento
    {
        public string Id { get; set; }
        public string DescricaoReduzida { get; set; }
        public string Descricao { get; set; }
        public string Grupo { get; set; }

        public Treinamento()
        {
        }
    }
}
