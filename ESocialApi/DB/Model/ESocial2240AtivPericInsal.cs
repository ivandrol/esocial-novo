﻿using Newtonsoft.Json;

namespace ESocialApi.DB.Model
{
    public class ESocial2240AtivPericInsal
    {
        public int Id { get; set; }
        public int IdESocial { get; set; }
        public string CodAtiv { get; set; }

        [JsonIgnore]
        public ESocial2240 ESocial2240 { get; set; }

        public Atividade Atividade { get; set; }

        public ESocial2240AtivPericInsal()
        {

        }
    }
}
