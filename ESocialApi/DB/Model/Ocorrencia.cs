﻿using Newtonsoft.Json;
using System.Xml.Serialization;

namespace ESocialApi.DB.Model
{
    public class Ocorrencia
    {
        [XmlIgnore]
        public int Id { get; set; }

        public int IdESocial { get; set; }
        public string Tipo { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public string Localizacao { get; set; }

        [JsonIgnore]
        public ESocial ESocial { get; set; }

        public Ocorrencia()
        {
        }
    }
}
