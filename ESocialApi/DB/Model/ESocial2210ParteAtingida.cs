﻿using Newtonsoft.Json;

namespace ESocialApi.DB.Model
{
    public class ESocial2210ParteAtingida
    {
        public int Id { get; set; }
        public int IdESocial { get; set; }
        public string CodParteAting { get; set; }
        public int Lateralidade { get; set; }

        [JsonIgnore]
        public ESocial2210 ESocial2210 { get; set; }

        public ParteAtingida ParteAtingida { get; set; }

        public ESocial2210ParteAtingida(string codParteAting, int lateralidade)
        {
            CodParteAting = codParteAting;
            Lateralidade = lateralidade;
        }

        public ESocial2210ParteAtingida()
        {

        }
    }
}
