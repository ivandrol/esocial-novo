﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class ESocial2240FatRisco
    {
        public int IdFatRisco { get; set; }
        public int IdESocial { get; set; }
        public string CodFatRis { get; set; }
        public int TpAval { get; set; }
        public string IntConc { get; set; }
        public string LimTol { get; set; }
        public string UnMed { get; set; }
        public string TecMedicao { get; set; }
        public string Insalubridade { get; set; }
        public string Periculosidade { get; set; }
        public string AposentEsp { get; set; }
        public string UtilizEPC { get; set; }
        public string EficEpc { get; set; }
        public string UtilizEPI { get; set; }
        public string DscFatRisc { get; set; }

        public string MedProtecao { get; set; }
        public string CondFuncto { get; set; }
        public string UsoInint { get; set; }
        public string PrzValid { get; set; }
        public string PeriodicTroca { get; set; }
        public string Higienizacao { get; set; }

        public List<ESocial2240FatRiscoEpi> ESocial2240FatRiscoEpi { get; set; }

        [JsonIgnore]
        public ESocial2240 ESocial2240 { get; set; }

        public FatorRisco FatorRisco { get; set; }

        public ESocial2240FatRisco()
        {
            ESocial2240FatRiscoEpi = new List<ESocial2240FatRiscoEpi>();
        }
    }
}
