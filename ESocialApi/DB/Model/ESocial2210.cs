﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class ESocial2210
    {
        public int Id { get; set; }
        public string CpfTrab { get; set; }
        public DateTime DtAcid { get; set; }
        public string TpAcid { get; set; }
        public string HrAcid { get; set; }
        public string HrsTrabAntesAcid { get; set; }
        public int TpCat { get; set; }
        public string IndCatObito { get; set; }
        public DateTime? DtObito { get; set; }
        public string IndComunPolicia { get; set; }
        public string CodSitGeradora { get; set; }
        public int IniciatCAT { get; set; }
        public string ObsCAT { get; set; }
        public int TpLocal { get; set; }
        public string DscLocal { get; set; }
        public string CodAmb { get; set; }
        public string TpLograd { get; set; }
        public string DscLograd { get; set; }
        public string NrLograd { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string CodMunic { get; set; }
        public string Uf { get; set; }
        public string IdPais { get; set; }
        public string CodPostal { get; set; }
        public int? TpInsc { get; set; }
        public string NrInsc { get; set; }
        public string CodCNES { get; set; }
        public DateTime? DtAtendimento { get; set; }
        public string HrAtendimento { get; set; }
        public string IndInternacao { get; set; }
        public string DurTrat { get; set; }
        public string IndAfast { get; set; }
        public string DscLesao { get; set; }
        public string DscCompLesao { get; set; }
        public string DiagProvavel { get; set; }
        public string CodCID { get; set; }
        public string Observacao { get; set; }
        public string NmEmit { get; set; }
        public string IdeOC { get; set; }
        public string NrOC { get; set; }
        public string UfOC { get; set; }
        public string NrRecCatOrig { get; set; }

        public List<ESocial2210ParteAtingida> ESocial2210ParteAtingida { get; set; }
        public List<ESocial2210AgenteCausador> ESocial2210AgenteCausador { get; set; }

        [JsonIgnore]
        public ESocial ESocial { get; set; }

        public TipoAcidente TipoAcidente { get; set; }
        public SituacaoGeradora SituacaoGeradora { get; set; }
        public TipoInscricao TipoInscricao { get; set; }
        public Pais Pais { get; set; }
        public Funcionario Funcionario { get; set; }

        public ESocial2210()
        {
            ESocial2210ParteAtingida = new List<ESocial2210ParteAtingida>();
            ESocial2210AgenteCausador = new List<ESocial2210AgenteCausador>();
        }
    }
}
