﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class TipoAcidente
    {
        public string Id { get; set; }
        public string Descricao { get; set; }
        public string DescricaoReduzida { get; set; }

        [JsonIgnore]
        public List<ESocial2210> ESocial2210 { get; set; }

        public TipoAcidente()
        {
            ESocial2210 = new List<ESocial2210>();
        }
    }
}
