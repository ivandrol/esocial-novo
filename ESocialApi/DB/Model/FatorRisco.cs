﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class FatorRisco
    {
        public string Id { get;set;}
        public string DescricaoReduzida { get; set; }
        public string Descricao { get; set; }
        public string Grupo { get; set; }

        [JsonIgnore]
        public List<ESocial2240FatRisco> ESocial2240FatRisco { get; set; }

        public FatorRisco()
        {
            ESocial2240FatRisco = new List<ESocial2240FatRisco>();
        }
    }
}
