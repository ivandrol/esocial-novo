﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class Transmissor
    {
        public string NrInsc { get; set; }
        public int TpInsc { get; set; }
        public string RazaoSocial { get; set; }
        public string SerialNumber { get; set; }
        public string TpAmb { get; set; }

        [JsonIgnore]
        public List<Empregador> Empregador { get; set; }

        public TipoInscricao TipoInscricao { get; set; }

        public Transmissor()
        {
            Empregador = new List<Empregador>();
        }

        public Transmissor(string nrInsc, int tpInsc, string razaoSocial, string serialNumber, string tpAmb)
        {
            NrInsc = nrInsc;
            TpInsc = tpInsc;
            RazaoSocial = razaoSocial;
            SerialNumber = serialNumber;
            TpAmb = tpAmb;
        }
    }
}
