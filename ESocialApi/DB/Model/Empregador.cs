﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class Empregador
    {
        public string NrInsc { get; set; }
        public string NrInscTransmissor { get; set; }
        public int TpInsc { get; set; }
        public string RazaoSocial { get; set; }
        public bool Transmitir { get; set; }
        public bool EnviarEmail { get; set; }
        public string Emails { get; set; }

        [JsonIgnore]
        public List<ESocial> ESocial { get; set; }

        public Transmissor Transmissor { get; set; }
        public TipoInscricao TipoInscricao { get; set; }

        public Empregador()
        {
            ESocial = new List<ESocial>();
        }

        public Empregador(int tpInsc, string nrInsc, string nrInscTransmissor, string razaoSocial, bool transmitir, bool enviarEmail, string emails)
        {
            TpInsc = tpInsc;
            NrInsc = nrInsc;
            NrInscTransmissor = nrInscTransmissor;
            RazaoSocial = razaoSocial;
            Transmitir = transmitir;
            EnviarEmail = enviarEmail;
            Emails = emails;
        }
    }
}
