﻿using Newtonsoft.Json;

namespace ESocialApi.DB.Model
{
    public class ESocialProcessamento
    {
        public int Id { get; set; }
        public int IdESocial { get; set; }
        public int CdResposta { get; set; }
        public string DescResposta { get; set; }
        public string VersaoAppProcessamento { get; set; }
        public string DhProcessamento { get; set; }

        [JsonIgnore]
        public ESocial ESocial { get; set; }

        public ESocialProcessamento()
        {

        }
    }
}
