﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class Lote
    {
        public int Id { get; set; }
        public string NLote { get; set; }
        public string ProtocoloEnvio { get; set; }
        public int? CdRespostaEnvio { get; set; }
        public string DescRespostaEnvio { get; set; }
        public string DhRecepcaoEnvio { get; set; }
        public int? CdRespostaConsulta { get; set; }
        public string VersaoAppRecepcao { get; set; }

        [JsonIgnore]
        public List<ESocial> ESocial { get; set; }

        public Lote(string lote)
        {
            this.NLote = lote;
            ESocial = new List<ESocial>();
        }

        public Lote()
        {
            ESocial = new List<ESocial>();
        }
    }
}
