﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class ParteAtingida
    {
        public string Id { get; set; }
        public string DescricaoReduzida { get; set; }
        public string Descricao { get; set; }

        [JsonIgnore]
        public List<ESocial2210ParteAtingida> ESocial2210ParteAtingida { get; set; }

        public ParteAtingida()
        {
            ESocial2210ParteAtingida = new List<ESocial2210ParteAtingida>();
        }
    }
}
