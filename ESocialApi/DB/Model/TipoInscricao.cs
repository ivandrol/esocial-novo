﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class TipoInscricao
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

        [JsonIgnore]
        public List<ESocial2210> ESocial2210 { get; set; }
        [JsonIgnore]
        public List<Empregador> Empregador { get; set; }
        [JsonIgnore]
        public List<Transmissor> Transmissor { get; set; }

        public TipoInscricao()
        {
            ESocial2210 = new List<ESocial2210>();
            Empregador = new List<Empregador>();
            Transmissor = new List<Transmissor>();
        }
    }
}
