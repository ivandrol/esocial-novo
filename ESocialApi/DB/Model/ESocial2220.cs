﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
namespace ESocialApi.DB.Model
{
    public class ESocial2220
    {
        public int Id { get; set; }
        public string CpfTrab { get; set; }
        public int TpExameOcup { get; set; }
        public DateTime DtAso { get; set; }
        public int ResAso { get; set; }
        public string CpfMed { get; set; }
        public string NisMed { get; set; }
        public string NmMed { get; set; }
        public string NrCRMMed { get; set; }
        public string UfCRMMed { get; set; }
        public string CpfResp { get; set; }
        public string NmResp { get; set; }
        public string NrCRMResp { get; set; }
        public string UfCRMResp { get; set; }
        
        public List<ESocial2220Exame> ESocial2220Exame { get; set; }

        [JsonIgnore]
        public ESocial ESocial { get; set; }

        public Funcionario Funcionario { get; set; }

        public ESocial2220()
        {
            ESocial2220Exame = new List<ESocial2220Exame>();
        }
    }
}
