﻿using Newtonsoft.Json;

namespace ESocialApi.DB.Model
{
    public class ESocial2240InfoAmb
    {
        public int Id { get; set; }
        public int IdESocial { get; set; }
        public string LocalAmb { get; set; }
        public string DscSetor { get; set; }
        public string TpInsc { get; set; }
        public string NrInsc { get; set; }

        [JsonIgnore]
        public ESocial2240 ESocial2240 { get; set; }

        public ESocial2240InfoAmb()
        {

        }
    }
}
