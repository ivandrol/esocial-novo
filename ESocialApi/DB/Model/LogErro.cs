﻿using System;

namespace ESocialApi.DB.Model
{
    public class LogErro
    {
        public int Id{ get; set; }
        public DateTime DataErro { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public LogErro()
        {

        }
    }
}

