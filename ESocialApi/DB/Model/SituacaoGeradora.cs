﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ESocialApi.DB.Model
{
    public class SituacaoGeradora
    {
        public string Id { get; set; }
        public string Descricao { get; set; }

        [JsonIgnore]
        public List<ESocial2210> ESocial2210 { get; set; }

        public SituacaoGeradora()
        {
            ESocial2210 = new List<ESocial2210>();
        }
    }
}
