﻿using ESocialApi.DB.Model;
using Microsoft.EntityFrameworkCore;

namespace ESocialApi.DB
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {
        }

        public DbSet<Lote> Lote { get; set; }
        public DbSet<Ocorrencia> Ocorrencia { get; set; }
        public DbSet<ESocial> ESocial { get; set; }
        public DbSet<ESocial2210> ESocial2210 { get; set; }
        public DbSet<ESocial2210AgenteCausador> ESocial2210AgenteCausador { get; set; }
        public DbSet<ESocial2210ParteAtingida> ESocial2210ParteAtingida { get; set; }
        public DbSet<ESocial2220> ESocial2220 { get; set; }
        public DbSet<ESocial2220Exame> ESocial2220Exame { get; set; }
        public DbSet<ESocial2240> ESocial2240 { get; set; }
        public DbSet<ESocial2240AtivPericInsal> ESocial2240AtivPericInsal { get; set; }
        public DbSet<ESocial2240FatRisco> ESocial2240FatRisco { get; set; }
        public DbSet<ESocial2240FatRiscoEpi> ESocial2240FatRiscoEpi { get; set; }
        public DbSet<ESocial2240InfoAmb> ESocial2240InfoAmb { get; set; }
        public DbSet<ESocial2240RespReg> ESocial2240RespReg { get; set; }
        public DbSet<ESocialProcessamento> ESocialProcessamento { get; set; }
        public DbSet<TipoInscricao> TipoInscricao { get; set; }
        public DbSet<CategoriaTrabalhador> CategoriaTrabalhador { get; set; }
        public DbSet<TipoAcidente> TipoAcidente { get; set; }
        public DbSet<SituacaoGeradora> SituacaoGeradora { get; set; }
        public DbSet<ParteAtingida> ParteAtingida { get; set; }
        public DbSet<AgenteCausador> AgenteCausador { get; set; }
        public DbSet<ProcedimentoRealizado> ProcedimentoRealizado { get; set; }
        public DbSet<Atividade> Atividade { get; set; }
        public DbSet<FatorRisco> FatorRisco { get; set; }
        public DbSet<Treinamento> Treinamento { get; set; }
        public DbSet<Pais> Pais { get; set; }
        public DbSet<LogErro> LogErro { get; set; }
        public DbSet<Empregador> Empregador { get; set; }
        public DbSet<Funcionario> Funcionario { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region ESocial
            modelBuilder.Entity<ESocial>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial>().Property(s => s.TpEvento).HasMaxLength(4);
            modelBuilder.Entity<ESocial>().Property(s => s.DataEvento).HasColumnType("datetime");
            modelBuilder.Entity<ESocial>().Property(s => s.IdEvento).HasMaxLength(36);
            modelBuilder.Entity<ESocial>().Property(s => s.NrRecibo).HasMaxLength(250);
            modelBuilder.Entity<ESocial>().Property(s => s.NrReciboOriginal).HasMaxLength(250);
            modelBuilder.Entity<ESocial>().Property(s => s.IdLote).HasMaxLength(11).IsRequired(false);
            modelBuilder.Entity<ESocial>().Property(s => s.Operacao).HasMaxLength(1);
            modelBuilder.Entity<ESocial>().Property(s => s.StatusTransmissao).HasMaxLength(1);
            modelBuilder.Entity<ESocial>().Property(s => s.StatusEmail).HasMaxLength(1);
            modelBuilder.Entity<ESocial>().Property(s => s.StatusGet).HasMaxLength(1);
            modelBuilder.Entity<ESocial>().Property(s => s.NrInsc).HasMaxLength(15).IsRequired();

            modelBuilder.Entity<ESocial>()
                  .HasOne<Empregador>(e => e.Empregador)
                  .WithMany(e => e.ESocial)
                  .HasForeignKey(e => e.NrInsc);

            modelBuilder.Entity<ESocial>()
                  .HasOne<Lote>(e => e.Lote)
                  .WithMany(e => e.ESocial)
                  .HasForeignKey(e => e.IdLote);


            #endregion

            #region ESocialProcessamento
            modelBuilder.Entity<ESocialProcessamento>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocialProcessamento>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocialProcessamento>().Property(s => s.IdESocial).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocialProcessamento>().Property(s => s.CdResposta).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocialProcessamento>().Property(s => s.DescResposta).HasMaxLength(250).IsRequired();
            modelBuilder.Entity<ESocialProcessamento>().Property(s => s.VersaoAppProcessamento).HasMaxLength(50).IsRequired();
            modelBuilder.Entity<ESocialProcessamento>().Property(s => s.DhProcessamento).HasMaxLength(25).IsRequired();

            modelBuilder.Entity<ESocialProcessamento>()
                  .HasOne<ESocial>(e => e.ESocial)
                  .WithMany(e => e.ESocialProcessamento)
                  .HasForeignKey(e => e.IdESocial);
            #endregion

            #region ESocial2210
            modelBuilder.Entity<ESocial2210>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial2210>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.CpfTrab).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.DtAcid).HasMaxLength(10).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.TpAcid).HasMaxLength(6).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.HrAcid).HasMaxLength(4);
            modelBuilder.Entity<ESocial2210>().Property(s => s.HrsTrabAntesAcid).HasMaxLength(4).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.TpCat).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.IndCatObito).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.DtObito).HasMaxLength(10);
            modelBuilder.Entity<ESocial2210>().Property(s => s.IndComunPolicia).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.CodSitGeradora).HasMaxLength(9).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.IniciatCAT).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.ObsCAT).HasMaxLength(999);
            modelBuilder.Entity<ESocial2210>().Property(s => s.TpLocal).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.DscLocal).HasMaxLength(255);
            modelBuilder.Entity<ESocial2210>().Property(s => s.CodAmb).HasMaxLength(30);
            modelBuilder.Entity<ESocial2210>().Property(s => s.TpLograd).HasMaxLength(4).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.DscLograd).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.NrLograd).HasMaxLength(10).IsRequired();
            modelBuilder.Entity<ESocial2210>().Property(s => s.Complemento).HasMaxLength(30);
            modelBuilder.Entity<ESocial2210>().Property(s => s.Bairro).HasMaxLength(90);
            modelBuilder.Entity<ESocial2210>().Property(s => s.Cep).HasMaxLength(8);
            modelBuilder.Entity<ESocial2210>().Property(s => s.CodMunic).HasMaxLength(7);
            modelBuilder.Entity<ESocial2210>().Property(s => s.Uf).HasMaxLength(2);
            modelBuilder.Entity<ESocial2210>().Property(s => s.IdPais).HasMaxLength(3);
            modelBuilder.Entity<ESocial2210>().Property(s => s.CodPostal).HasMaxLength(12);
            modelBuilder.Entity<ESocial2210>().Property(s => s.TpInsc).HasMaxLength(1);
            modelBuilder.Entity<ESocial2210>().Property(s => s.NrInsc).HasMaxLength(15);
            modelBuilder.Entity<ESocial2210>().Property(s => s.CodCNES).HasMaxLength(7);
            modelBuilder.Entity<ESocial2210>().Property(s => s.DtAtendimento).HasMaxLength(10);
            modelBuilder.Entity<ESocial2210>().Property(s => s.HrAtendimento).HasMaxLength(4);
            modelBuilder.Entity<ESocial2210>().Property(s => s.IndInternacao).HasMaxLength(1);
            modelBuilder.Entity<ESocial2210>().Property(s => s.DurTrat).HasMaxLength(4);
            modelBuilder.Entity<ESocial2210>().Property(s => s.IndAfast).HasMaxLength(1);
            modelBuilder.Entity<ESocial2210>().Property(s => s.DscLesao).HasMaxLength(9);
            modelBuilder.Entity<ESocial2210>().Property(s => s.DscCompLesao).HasMaxLength(200);
            modelBuilder.Entity<ESocial2210>().Property(s => s.DiagProvavel).HasMaxLength(100);
            modelBuilder.Entity<ESocial2210>().Property(s => s.CodCID).HasMaxLength(4);
            modelBuilder.Entity<ESocial2210>().Property(s => s.Observacao).HasMaxLength(255);
            modelBuilder.Entity<ESocial2210>().Property(s => s.NmEmit).HasMaxLength(70);
            modelBuilder.Entity<ESocial2210>().Property(s => s.IdeOC).HasMaxLength(1);
            modelBuilder.Entity<ESocial2210>().Property(s => s.NrOC).HasMaxLength(14);
            modelBuilder.Entity<ESocial2210>().Property(s => s.UfOC).HasMaxLength(2);
            modelBuilder.Entity<ESocial2210>().Property(s => s.NrRecCatOrig).HasMaxLength(40);

            modelBuilder.Entity<ESocial>()
                  .HasOne(e => e.ESocial2210)
                  .WithOne(e => e.ESocial)
                  .HasForeignKey<ESocial2210>(e => e.Id);

            modelBuilder.Entity<ESocial2210>()
                  .HasOne<Funcionario>(e => e.Funcionario)
                  .WithMany(e => e.ESocial2210)
                  .HasForeignKey(e => e.CpfTrab);

            modelBuilder.Entity<ESocial2210>()
                  .HasOne<TipoAcidente>(e => e.TipoAcidente)
                  .WithMany(e => e.ESocial2210)
                  .HasForeignKey(e => e.TpAcid);

            modelBuilder.Entity<ESocial2210>()
                  .HasOne<Pais>(e => e.Pais)
                  .WithMany(e => e.ESocial2210)
                  .HasForeignKey(e => e.IdPais);

            modelBuilder.Entity<ESocial2210>()
                  .HasOne<SituacaoGeradora>(e => e.SituacaoGeradora)
                  .WithMany(e => e.ESocial2210)
                  .HasForeignKey(e => e.CodSitGeradora);

            modelBuilder.Entity<ESocial2210>()
                  .HasOne<TipoInscricao>(e => e.TipoInscricao)
                  .WithMany(e => e.ESocial2210)
                  .HasForeignKey(e => e.TpInsc);

            #region ESocial2210AgenteCausador
            modelBuilder.Entity<ESocial2210AgenteCausador>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial2210AgenteCausador>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2210AgenteCausador>().Property(s => s.IdESocial).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2210AgenteCausador>().Property(s => s.CodAgntCausador).HasMaxLength(9).IsRequired();

            modelBuilder.Entity<ESocial2210AgenteCausador>()
                  .HasOne<ESocial2210>(e => e.ESocial2210)
                  .WithMany(e => e.ESocial2210AgenteCausador)
                  .HasForeignKey(e => e.IdESocial);

            modelBuilder.Entity<ESocial2210AgenteCausador>()
                  .HasOne<AgenteCausador>(e => e.AgenteCausador)
                  .WithMany(e => e.ESocial2210AgenteCausador)
                  .HasForeignKey(e => e.CodAgntCausador);
            #endregion

            #region ESocial2210ParteAtingida
            modelBuilder.Entity<ESocial2210ParteAtingida>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial2210ParteAtingida>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2210ParteAtingida>().Property(s => s.IdESocial).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2210ParteAtingida>().Property(s => s.CodParteAting).HasMaxLength(9).IsRequired();
            modelBuilder.Entity<ESocial2210ParteAtingida>().Property(s => s.Lateralidade).HasMaxLength(1).IsRequired();

            modelBuilder.Entity<ESocial2210ParteAtingida>()
                  .HasOne<ESocial2210>(e => e.ESocial2210)
                  .WithMany(e => e.ESocial2210ParteAtingida)
                  .HasForeignKey(e => e.IdESocial);

            modelBuilder.Entity<ESocial2210ParteAtingida>()
                  .HasOne<ParteAtingida>(e => e.ParteAtingida)
                  .WithMany(e => e.ESocial2210ParteAtingida)
                  .HasForeignKey(e => e.CodParteAting);
            #endregion

            #endregion

            #region ESocial2220
            modelBuilder.Entity<ESocial2220>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial2220>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2220>().Property(s => s.CpfTrab).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2220>().Property(s => s.TpExameOcup).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2220>().Property(s => s.DtAso).HasMaxLength(10).IsRequired();
            modelBuilder.Entity<ESocial2220>().Property(s => s.ResAso).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2220>().Property(s => s.CpfMed).HasMaxLength(11);
            modelBuilder.Entity<ESocial2220>().Property(s => s.NisMed).HasMaxLength(11);
            modelBuilder.Entity<ESocial2220>().Property(s => s.NmMed).HasMaxLength(70).IsRequired();
            modelBuilder.Entity<ESocial2220>().Property(s => s.NrCRMMed).HasMaxLength(8).IsRequired();
            modelBuilder.Entity<ESocial2220>().Property(s => s.UfCRMMed).HasMaxLength(2).IsRequired();
            modelBuilder.Entity<ESocial2220>().Property(s => s.CpfResp).HasMaxLength(11);
            modelBuilder.Entity<ESocial2220>().Property(s => s.NmResp).HasMaxLength(70);
            modelBuilder.Entity<ESocial2220>().Property(s => s.NrCRMResp).HasMaxLength(8);
            modelBuilder.Entity<ESocial2220>().Property(s => s.UfCRMResp).HasMaxLength(2);

            modelBuilder.Entity<ESocial>()
                  .HasOne(e => e.ESocial2220)
                  .WithOne(e => e.ESocial)
                  .HasForeignKey<ESocial2220>(e => e.Id);

            modelBuilder.Entity<ESocial2220>()
                  .HasOne<Funcionario>(e => e.Funcionario)
                  .WithMany(e => e.ESocial2220)
                  .HasForeignKey(e => e.CpfTrab);

            #region ESocial2220Exame
            modelBuilder.Entity<ESocial2220Exame>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial2220Exame>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2220Exame>().Property(s => s.IdESocial).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2220Exame>().Property(s => s.DtExm).IsRequired();
            modelBuilder.Entity<ESocial2220Exame>().Property(s => s.ProcRealizado).HasMaxLength(4).IsRequired();
            modelBuilder.Entity<ESocial2220Exame>().Property(s => s.ObsProc).HasMaxLength(999);
            modelBuilder.Entity<ESocial2220Exame>().Property(s => s.OrdExame).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2220Exame>().Property(s => s.IndResult).HasMaxLength(1).IsRequired(false);

            modelBuilder.Entity<ESocial2220Exame>()
                  .HasOne<ESocial2220>(e => e.ESocial2220)
                  .WithMany(e => e.ESocial2220Exame)
                  .HasForeignKey(e => e.IdESocial);

            modelBuilder.Entity<ESocial2220Exame>()
                  .HasOne<ProcedimentoRealizado>(e => e.ProcedimentoRealizado)
                  .WithMany(e => e.ESocial2220Exame)
                  .HasForeignKey(e => e.ProcRealizado);
            #endregion

            #endregion

            #region ESocial2240
            modelBuilder.Entity<ESocial2240>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial2240>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240>().Property(s => s.CpfTrab).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240>().Property(s => s.DtIniCondicao).IsRequired();
            modelBuilder.Entity<ESocial2240>().Property(s => s.DscAtivDes).HasMaxLength(999).IsRequired();
            modelBuilder.Entity<ESocial2240>().Property(s => s.MetErg).HasMaxLength(999);
            modelBuilder.Entity<ESocial2240>().Property(s => s.ObsCompl).HasMaxLength(999);

            modelBuilder.Entity<ESocial>()
                  .HasOne(e => e.ESocial2240)
                  .WithOne(e => e.ESocial)
                  .HasForeignKey<ESocial2240>(e => e.Id);

            modelBuilder.Entity<ESocial2240>()
                  .HasOne<Funcionario>(e => e.Funcionario)
                  .WithMany(e => e.ESocial2240)
                  .HasForeignKey(e => e.CpfTrab);

            #region ESocial2240InfoAmb
            modelBuilder.Entity<ESocial2240InfoAmb>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial2240InfoAmb>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240InfoAmb>().Property(s => s.IdESocial).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240InfoAmb>().Property(s => s.LocalAmb).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2240InfoAmb>().Property(s => s.DscSetor).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<ESocial2240InfoAmb>().Property(s => s.TpInsc).HasMaxLength(1);
            modelBuilder.Entity<ESocial2240InfoAmb>().Property(s => s.DscSetor).HasMaxLength(14);

            modelBuilder.Entity<ESocial2240InfoAmb>()
                  .HasOne<ESocial2240>(e => e.ESocial2240)
                  .WithMany(e => e.ESocial2240InfoAmb)
                  .HasForeignKey(e => e.IdESocial);
            #endregion


            #region ESocial2240AtivPericInsal
            modelBuilder.Entity<ESocial2240AtivPericInsal>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial2240AtivPericInsal>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240AtivPericInsal>().Property(s => s.IdESocial).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240AtivPericInsal>().Property(s => s.CodAtiv).HasMaxLength(6).IsRequired();

            modelBuilder.Entity<ESocial2240AtivPericInsal>()
                  .HasOne<ESocial2240>(e => e.ESocial2240)
                  .WithMany(e => e.ESocial2240AtivPericInsal)
                  .HasForeignKey(e => e.IdESocial);

            modelBuilder.Entity<ESocial2240AtivPericInsal>()
                  .HasOne<Atividade>(e => e.Atividade)
                  .WithMany(e => e.ESocial2240AtivPericInsal)
                  .HasForeignKey(e => e.CodAtiv);
            #endregion

            #region ESocial2240FatRisco
            modelBuilder.Entity<ESocial2240FatRisco>().HasKey(e => e.IdFatRisco);
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.IdFatRisco).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.IdESocial).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.CodFatRis).HasMaxLength(9).IsRequired();
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.CodFatRis).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.IntConc).HasMaxLength(10).IsRequired(false);
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.LimTol).HasMaxLength(10).IsRequired(false);
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.UnMed).HasMaxLength(2).IsRequired(false);
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.TecMedicao).HasMaxLength(40);
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.Insalubridade).HasMaxLength(1);
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.Periculosidade).HasMaxLength(1);
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.AposentEsp).HasMaxLength(1);
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.UtilizEPC).HasMaxLength(1).IsRequired(false);
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.EficEpc).HasMaxLength(1);
            modelBuilder.Entity<ESocial2240FatRisco>().Property(s => s.UtilizEPI).HasMaxLength(1).IsRequired(false);

            modelBuilder.Entity<ESocial2240FatRisco>()
                  .HasOne<ESocial2240>(e => e.ESocial2240)
                  .WithMany(e => e.ESocial2240FatRisco)
                  .HasForeignKey(e => e.IdESocial);

            modelBuilder.Entity<ESocial2240FatRisco>()
                  .HasOne<FatorRisco>(e => e.FatorRisco)
                  .WithMany(e => e.ESocial2240FatRisco)
                  .HasForeignKey(e => e.CodFatRis);
            #endregion

            #region ESocial2240FatRiscoEpi
            modelBuilder.Entity<ESocial2240FatRiscoEpi>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial2240FatRiscoEpi>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240FatRiscoEpi>().Property(s => s.IdFatRisco).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240FatRiscoEpi>().Property(s => s.CaEPI).HasMaxLength(20);
            modelBuilder.Entity<ESocial2240FatRiscoEpi>().Property(s => s.DscEPI).HasMaxLength(999);
            modelBuilder.Entity<ESocial2240FatRiscoEpi>().Property(s => s.EficEpi).HasMaxLength(1).IsRequired();

            modelBuilder.Entity<ESocial2240FatRiscoEpi>()
                  .HasOne<ESocial2240FatRisco>(e => e.ESocial2240FatRisco)
                  .WithMany(e => e.ESocial2240FatRiscoEpi)
                  .HasForeignKey(e => e.IdFatRisco);
            #endregion

            #region ESocial2240RespReg
            modelBuilder.Entity<ESocial2240RespReg>().HasKey(e => e.Id);
            modelBuilder.Entity<ESocial2240RespReg>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240RespReg>().Property(s => s.IdESocial).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240RespReg>().Property(s => s.CpfResp).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240RespReg>().Property(s => s.NisResp).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<ESocial2240RespReg>().Property(s => s.NmResp).HasMaxLength(70).IsRequired();
            modelBuilder.Entity<ESocial2240RespReg>().Property(s => s.IdeOC).HasMaxLength(1).IsRequired();
            modelBuilder.Entity<ESocial2240RespReg>().Property(s => s.DscOC).HasMaxLength(20);
            modelBuilder.Entity<ESocial2240RespReg>().Property(s => s.NrOC).HasMaxLength(14).IsRequired();
            modelBuilder.Entity<ESocial2240RespReg>().Property(s => s.UfOC).HasMaxLength(2).IsRequired();

            modelBuilder.Entity<ESocial2240RespReg>()
                  .HasOne<ESocial2240>(e => e.ESocial2240)
                  .WithMany(e => e.ESocial2240RespReg)
                  .HasForeignKey(e => e.IdESocial);
            #endregion

            #endregion

            #region Lote e Ocorrencia
            modelBuilder.Entity<Ocorrencia>().HasKey(e => e.Id);
            modelBuilder.Entity<Ocorrencia>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<Ocorrencia>().Property(s => s.IdESocial).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<Ocorrencia>().Property(s => s.Tipo).HasMaxLength(1).IsRequired(false);
            modelBuilder.Entity<Ocorrencia>().Property(s => s.Codigo).HasMaxLength(25);
            modelBuilder.Entity<Ocorrencia>().Property(s => s.Descricao).HasMaxLength(1000);
            modelBuilder.Entity<Ocorrencia>().Property(s => s.Localizacao).HasMaxLength(250);

            modelBuilder.Entity<Ocorrencia>()
                  .HasOne<ESocial>(e => e.ESocial)
                  .WithMany(e => e.Ocorencias)
                  .HasForeignKey(e => e.IdESocial);


            modelBuilder.Entity<Lote>().HasKey(e => e.Id);
            modelBuilder.Entity<Lote>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<Lote>().Property(s => s.NLote).HasMaxLength(255);
            modelBuilder.Entity<Lote>().Property(s => s.ProtocoloEnvio).HasMaxLength(250);
            modelBuilder.Entity<Lote>().Property(s => s.CdRespostaEnvio).HasMaxLength(11).IsRequired(false);
            modelBuilder.Entity<Lote>().Property(s => s.DescRespostaEnvio).HasMaxLength(250);
            modelBuilder.Entity<Lote>().Property(s => s.DhRecepcaoEnvio).HasMaxLength(20);
            modelBuilder.Entity<Lote>().Property(s => s.CdRespostaConsulta).HasMaxLength(11).IsRequired(false);


            modelBuilder.Entity<ESocial>()
                  .HasOne<Lote>(e => e.Lote)
                  .WithMany(e => e.ESocial)
                  .HasForeignKey(e => e.IdLote);
            #endregion

            #region Tabelas Auxiliares
            modelBuilder.Entity<TipoInscricao>().HasKey(e => e.Id);
            modelBuilder.Entity<TipoInscricao>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<TipoInscricao>().Property(s => s.Descricao).HasMaxLength(5);

            modelBuilder.Entity<CategoriaTrabalhador>().HasKey(e => e.Id);
            modelBuilder.Entity<CategoriaTrabalhador>().Property(s => s.Id).HasMaxLength(3).IsRequired();
            modelBuilder.Entity<CategoriaTrabalhador>().Property(s => s.DescricaoReduzida).HasMaxLength(100);
            modelBuilder.Entity<CategoriaTrabalhador>().Property(s => s.Descricao).HasMaxLength(300);
            modelBuilder.Entity<CategoriaTrabalhador>().Property(s => s.Grupo).HasMaxLength(50);

            modelBuilder.Entity<TipoAcidente>().HasKey(e => e.Id);
            modelBuilder.Entity<TipoAcidente>().Property(s => s.Id).HasMaxLength(6).IsRequired();
            modelBuilder.Entity<CategoriaTrabalhador>().Property(s => s.DescricaoReduzida).HasMaxLength(100);
            modelBuilder.Entity<TipoAcidente>().Property(s => s.Descricao).HasMaxLength(600);

            modelBuilder.Entity<SituacaoGeradora>().HasKey(e => e.Id);
            modelBuilder.Entity<SituacaoGeradora>().Property(s => s.Id).HasMaxLength(9).IsRequired();
            modelBuilder.Entity<SituacaoGeradora>().Property(s => s.Descricao).HasMaxLength(100);

            modelBuilder.Entity<ParteAtingida>().HasKey(e => e.Id);
            modelBuilder.Entity<ParteAtingida>().Property(s => s.Id).HasMaxLength(9).IsRequired();
            modelBuilder.Entity<ParteAtingida>().Property(s => s.DescricaoReduzida).HasMaxLength(100);
            modelBuilder.Entity<ParteAtingida>().Property(s => s.Descricao).HasMaxLength(500);

            modelBuilder.Entity<AgenteCausador>().HasKey(e => e.Id);
            modelBuilder.Entity<AgenteCausador>().Property(s => s.Id).HasMaxLength(9).IsRequired();
            modelBuilder.Entity<AgenteCausador>().Property(s => s.DescricaoReduzida).HasMaxLength(100);
            modelBuilder.Entity<AgenteCausador>().Property(s => s.Descricao).HasMaxLength(1000);
            modelBuilder.Entity<AgenteCausador>().Property(s => s.Grupo).HasMaxLength(60);
            
            modelBuilder.Entity<ProcedimentoRealizado>().HasKey(e => e.Id);
            modelBuilder.Entity<ProcedimentoRealizado>().Property(s => s.Id).HasMaxLength(4).IsRequired();
            modelBuilder.Entity<ProcedimentoRealizado>().Property(s => s.DescricaoReduzida).HasMaxLength(100);
            modelBuilder.Entity<ProcedimentoRealizado>().Property(s => s.Descricao).HasMaxLength(250);

            modelBuilder.Entity<Atividade>().HasKey(e => e.Id);
            modelBuilder.Entity<Atividade>().Property(s => s.Id).HasMaxLength(6).IsRequired();
            modelBuilder.Entity<Atividade>().Property(s => s.DescricaoReduzida).HasMaxLength(100);
            modelBuilder.Entity<Atividade>().Property(s => s.Descricao).HasMaxLength(3000);
            modelBuilder.Entity<Atividade>().Property(s => s.Grupo).HasMaxLength(100);

            modelBuilder.Entity<FatorRisco>().HasKey(e => e.Id);
            modelBuilder.Entity<FatorRisco>().Property(s => s.Id).HasMaxLength(9).IsRequired();
            modelBuilder.Entity<FatorRisco>().Property(s => s.DescricaoReduzida).HasMaxLength(100);
            modelBuilder.Entity<FatorRisco>().Property(s => s.Descricao).HasMaxLength(250);
            modelBuilder.Entity<FatorRisco>().Property(s => s.Grupo).HasMaxLength(50);

            modelBuilder.Entity<Treinamento>().HasKey(e => e.Id);
            modelBuilder.Entity<Treinamento>().Property(s => s.Id).HasMaxLength(4).IsRequired();
            modelBuilder.Entity<Treinamento>().Property(s => s.DescricaoReduzida).HasMaxLength(100);
            modelBuilder.Entity<Treinamento>().Property(s => s.Descricao).HasMaxLength(200);
            modelBuilder.Entity<Treinamento>().Property(s => s.Grupo).HasMaxLength(100);

            modelBuilder.Entity<Pais>().HasKey(e => e.Id);
            modelBuilder.Entity<Pais>().Property(s => s.Id).HasMaxLength(3).IsRequired();
            modelBuilder.Entity<Pais>().Property(s => s.Descricao).HasMaxLength(100);
            #endregion

            #region Log Erro
            modelBuilder.Entity<LogErro>().HasKey(e => e.Id);
            modelBuilder.Entity<LogErro>().Property(s => s.Id).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<LogErro>().Property(s => s.DataErro).IsRequired();
            modelBuilder.Entity<LogErro>().Property(s => s.Message).HasMaxLength(11);
            modelBuilder.Entity<LogErro>().Property(s => s.StackTrace).HasMaxLength(250);
            #endregion

            #region Others
            modelBuilder.Entity<Empregador>().HasKey(e => e.NrInsc);
            modelBuilder.Entity<Empregador>().Property(s => s.NrInsc).HasMaxLength(15).IsRequired();
            modelBuilder.Entity<Empregador>().Property(s => s.TpInsc).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<Empregador>().Property(s => s.RazaoSocial).HasMaxLength(250);
            modelBuilder.Entity<Empregador>().Property(s => s.Transmitir).IsRequired();
            modelBuilder.Entity<Empregador>().Property(s => s.EnviarEmail).IsRequired();
            modelBuilder.Entity<Empregador>().Property(s => s.Emails).HasMaxLength(250);

            modelBuilder.Entity<Empregador>()
                  .HasOne<Transmissor>(e => e.Transmissor)
                  .WithMany(e => e.Empregador)
                  .HasForeignKey(e => e.NrInscTransmissor);

            modelBuilder.Entity<Empregador>()
                  .HasOne<TipoInscricao>(e => e.TipoInscricao)
                  .WithMany(e => e.Empregador)
                  .HasForeignKey(e => e.TpInsc);

            modelBuilder.Entity<Transmissor>().HasKey(e => e.NrInsc);
            modelBuilder.Entity<Transmissor>().Property(s => s.NrInsc).HasMaxLength(15).IsRequired();
            modelBuilder.Entity<Transmissor>().Property(s => s.TpInsc).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<Transmissor>().Property(s => s.RazaoSocial).HasMaxLength(250);
            modelBuilder.Entity<Transmissor>().Property(s => s.SerialNumber).HasMaxLength(100);

            modelBuilder.Entity<Transmissor>()
                  .HasOne<TipoInscricao>(e => e.TipoInscricao)
                  .WithMany(e => e.Transmissor)
                  .HasForeignKey(e => e.TpInsc);

            modelBuilder.Entity<Funcionario>().HasKey(e => e.CPF);
            modelBuilder.Entity<Funcionario>().Property(s => s.CPF).HasMaxLength(11).IsRequired();
            modelBuilder.Entity<Funcionario>().Property(s => s.Nome).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<Funcionario>().Property(s => s.Nis).HasMaxLength(11);
            modelBuilder.Entity<Funcionario>().Property(s => s.Matricula).HasMaxLength(30);
            modelBuilder.Entity<Funcionario>().Property(s => s.CodCateg).HasMaxLength(3);

            modelBuilder.Entity<Funcionario>()
                  .HasOne<CategoriaTrabalhador>(e => e.CategoriaTrabalhador)
                  .WithMany(e => e.Funcionario)
                  .HasForeignKey(e => e.CodCateg);

            #endregion
        }
    }
}
