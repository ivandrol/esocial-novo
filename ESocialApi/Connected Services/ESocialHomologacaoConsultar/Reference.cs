﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Esse código foi gerado por uma ferramenta.
//     //
//     As alterações no arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace ESocialHomologacaoConsultar
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/consulta/retorno" +
        "Processamento/v1_1_0", ConfigurationName="ESocialHomologacaoConsultar.ServicoConsultarLoteEventos")]
    public interface ServicoConsultarLoteEventos
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/consulta/retorno" +
            "Processamento/v1_1_0/ServicoConsultarLoteEventos/ConsultarLoteEventos", ReplyAction="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/consulta/retorno" +
            "Processamento/v1_1_0/ServicoConsultarLoteEventos/ConsultarLoteEventosResponse")]
        System.Threading.Tasks.Task<ESocialHomologacaoConsultar.ConsultarLoteEventosResponse> ConsultarLoteEventosAsync(ESocialHomologacaoConsultar.ConsultarLoteEventosRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ConsultarLoteEventosRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ConsultarLoteEventos", Namespace="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/consulta/retorno" +
            "Processamento/v1_1_0", Order=0)]
        public ESocialHomologacaoConsultar.ConsultarLoteEventosRequestBody Body;
        
        public ConsultarLoteEventosRequest()
        {
        }
        
        public ConsultarLoteEventosRequest(ESocialHomologacaoConsultar.ConsultarLoteEventosRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/consulta/retorno" +
        "Processamento/v1_1_0")]
    public partial class ConsultarLoteEventosRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public System.Xml.Linq.XElement consulta;
        
        public ConsultarLoteEventosRequestBody()
        {
        }
        
        public ConsultarLoteEventosRequestBody(System.Xml.Linq.XElement consulta)
        {
            this.consulta = consulta;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ConsultarLoteEventosResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ConsultarLoteEventosResponse", Namespace="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/consulta/retorno" +
            "Processamento/v1_1_0", Order=0)]
        public ESocialHomologacaoConsultar.ConsultarLoteEventosResponseBody Body;
        
        public ConsultarLoteEventosResponse()
        {
        }
        
        public ConsultarLoteEventosResponse(ESocialHomologacaoConsultar.ConsultarLoteEventosResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/consulta/retorno" +
        "Processamento/v1_1_0")]
    public partial class ConsultarLoteEventosResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public System.Xml.Linq.XElement ConsultarLoteEventosResult;
        
        public ConsultarLoteEventosResponseBody()
        {
        }
        
        public ConsultarLoteEventosResponseBody(System.Xml.Linq.XElement ConsultarLoteEventosResult)
        {
            this.ConsultarLoteEventosResult = ConsultarLoteEventosResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public interface ServicoConsultarLoteEventosChannel : ESocialHomologacaoConsultar.ServicoConsultarLoteEventos, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public partial class ServicoConsultarLoteEventosClient : System.ServiceModel.ClientBase<ESocialHomologacaoConsultar.ServicoConsultarLoteEventos>, ESocialHomologacaoConsultar.ServicoConsultarLoteEventos
    {
        
    /// <summary>
    /// Implemente este método parcial para configurar o ponto de extremidade de serviço.
    /// </summary>
    /// <param name="serviceEndpoint">O ponto de extremidade a ser configurado</param>
    /// <param name="clientCredentials">As credenciais do cliente</param>
    static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public ServicoConsultarLoteEventosClient() : 
                base(ServicoConsultarLoteEventosClient.GetDefaultBinding(), ServicoConsultarLoteEventosClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.Servicos.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServicoConsultarLoteEventosClient(EndpointConfiguration endpointConfiguration) : 
                base(ServicoConsultarLoteEventosClient.GetBindingForEndpoint(endpointConfiguration), ServicoConsultarLoteEventosClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServicoConsultarLoteEventosClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(ServicoConsultarLoteEventosClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServicoConsultarLoteEventosClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(ServicoConsultarLoteEventosClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServicoConsultarLoteEventosClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ESocialHomologacaoConsultar.ConsultarLoteEventosResponse> ESocialHomologacaoConsultar.ServicoConsultarLoteEventos.ConsultarLoteEventosAsync(ESocialHomologacaoConsultar.ConsultarLoteEventosRequest request)
        {
            return base.Channel.ConsultarLoteEventosAsync(request);
        }
        
        public System.Threading.Tasks.Task<ESocialHomologacaoConsultar.ConsultarLoteEventosResponse> ConsultarLoteEventosAsync(System.Xml.Linq.XElement consulta)
        {
            ESocialHomologacaoConsultar.ConsultarLoteEventosRequest inValue = new ESocialHomologacaoConsultar.ConsultarLoteEventosRequest();
            inValue.Body = new ESocialHomologacaoConsultar.ConsultarLoteEventosRequestBody();
            inValue.Body.consulta = consulta;
            return ((ESocialHomologacaoConsultar.ServicoConsultarLoteEventos)(this)).ConsultarLoteEventosAsync(inValue);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.Servicos))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                result.Security.Mode = System.ServiceModel.BasicHttpSecurityMode.Transport;
                result.Security.Transport.ClientCredentialType = System.ServiceModel.HttpClientCredentialType.Certificate;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Não foi possível encontrar o ponto de extremidade com o nome \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.Servicos))
            {
                return new System.ServiceModel.EndpointAddress("https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/consultar" +
                        "loteeventos/WsConsultarLoteEventos.svc");
            }
            throw new System.InvalidOperationException(string.Format("Não foi possível encontrar o ponto de extremidade com o nome \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return ServicoConsultarLoteEventosClient.GetBindingForEndpoint(EndpointConfiguration.Servicos);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return ServicoConsultarLoteEventosClient.GetEndpointAddress(EndpointConfiguration.Servicos);
        }
        
        public enum EndpointConfiguration
        {
            
            Servicos,
        }
    }
}
