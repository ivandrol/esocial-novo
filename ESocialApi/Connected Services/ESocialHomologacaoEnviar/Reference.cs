﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Esse código foi gerado por uma ferramenta.
//     //
//     As alterações no arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace ESocialHomologacaoEnviar
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/v1_1_0", ConfigurationName="ESocialHomologacaoEnviar.ServicoEnviarLoteEventos")]
    public interface ServicoEnviarLoteEventos
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/v1_1_0/ServicoEn" +
            "viarLoteEventos/EnviarLoteEventos", ReplyAction="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/v1_1_0/ServicoEn" +
            "viarLoteEventos/EnviarLoteEventosResponse")]
        System.Threading.Tasks.Task<ESocialHomologacaoEnviar.EnviarLoteEventosResponse> EnviarLoteEventosAsync(ESocialHomologacaoEnviar.EnviarLoteEventosRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class EnviarLoteEventosRequest
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="EnviarLoteEventos", Namespace="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/v1_1_0", Order=0)]
        public ESocialHomologacaoEnviar.EnviarLoteEventosRequestBody Body;
        
        public EnviarLoteEventosRequest()
        {
        }
        
        public EnviarLoteEventosRequest(ESocialHomologacaoEnviar.EnviarLoteEventosRequestBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/v1_1_0")]
    public partial class EnviarLoteEventosRequestBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public System.Xml.Linq.XElement loteEventos;
        
        public EnviarLoteEventosRequestBody()
        {
        }
        
        public EnviarLoteEventosRequestBody(System.Xml.Linq.XElement loteEventos)
        {
            this.loteEventos = loteEventos;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class EnviarLoteEventosResponse
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="EnviarLoteEventosResponse", Namespace="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/v1_1_0", Order=0)]
        public ESocialHomologacaoEnviar.EnviarLoteEventosResponseBody Body;
        
        public EnviarLoteEventosResponse()
        {
        }
        
        public EnviarLoteEventosResponse(ESocialHomologacaoEnviar.EnviarLoteEventosResponseBody Body)
        {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://www.esocial.gov.br/servicos/empregador/lote/eventos/envio/v1_1_0")]
    public partial class EnviarLoteEventosResponseBody
    {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public System.Xml.Linq.XElement EnviarLoteEventosResult;
        
        public EnviarLoteEventosResponseBody()
        {
        }
        
        public EnviarLoteEventosResponseBody(System.Xml.Linq.XElement EnviarLoteEventosResult)
        {
            this.EnviarLoteEventosResult = EnviarLoteEventosResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public interface ServicoEnviarLoteEventosChannel : ESocialHomologacaoEnviar.ServicoEnviarLoteEventos, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public partial class ServicoEnviarLoteEventosClient : System.ServiceModel.ClientBase<ESocialHomologacaoEnviar.ServicoEnviarLoteEventos>, ESocialHomologacaoEnviar.ServicoEnviarLoteEventos
    {
        
    /// <summary>
    /// Implemente este método parcial para configurar o ponto de extremidade de serviço.
    /// </summary>
    /// <param name="serviceEndpoint">O ponto de extremidade a ser configurado</param>
    /// <param name="clientCredentials">As credenciais do cliente</param>
    static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public ServicoEnviarLoteEventosClient() : 
                base(ServicoEnviarLoteEventosClient.GetDefaultBinding(), ServicoEnviarLoteEventosClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.WsEnviarLoteEventos.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServicoEnviarLoteEventosClient(EndpointConfiguration endpointConfiguration) : 
                base(ServicoEnviarLoteEventosClient.GetBindingForEndpoint(endpointConfiguration), ServicoEnviarLoteEventosClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServicoEnviarLoteEventosClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(ServicoEnviarLoteEventosClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServicoEnviarLoteEventosClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(ServicoEnviarLoteEventosClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServicoEnviarLoteEventosClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ESocialHomologacaoEnviar.EnviarLoteEventosResponse> ESocialHomologacaoEnviar.ServicoEnviarLoteEventos.EnviarLoteEventosAsync(ESocialHomologacaoEnviar.EnviarLoteEventosRequest request)
        {
            return base.Channel.EnviarLoteEventosAsync(request);
        }
        
        public System.Threading.Tasks.Task<ESocialHomologacaoEnviar.EnviarLoteEventosResponse> EnviarLoteEventosAsync(System.Xml.Linq.XElement loteEventos)
        {
            ESocialHomologacaoEnviar.EnviarLoteEventosRequest inValue = new ESocialHomologacaoEnviar.EnviarLoteEventosRequest();
            inValue.Body = new ESocialHomologacaoEnviar.EnviarLoteEventosRequestBody();
            inValue.Body.loteEventos = loteEventos;
            return ((ESocialHomologacaoEnviar.ServicoEnviarLoteEventos)(this)).EnviarLoteEventosAsync(inValue);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.WsEnviarLoteEventos))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                result.Security.Mode = System.ServiceModel.BasicHttpSecurityMode.Transport;
                result.Security.Transport.ClientCredentialType = System.ServiceModel.HttpClientCredentialType.Certificate;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Não foi possível encontrar o ponto de extremidade com o nome \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.WsEnviarLoteEventos))
            {
                return new System.ServiceModel.EndpointAddress("https://webservices.producaorestrita.esocial.gov.br/servicos/empregador/enviarlot" +
                        "eeventos/WsEnviarLoteEventos.svc");
            }
            throw new System.InvalidOperationException(string.Format("Não foi possível encontrar o ponto de extremidade com o nome \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return ServicoEnviarLoteEventosClient.GetBindingForEndpoint(EndpointConfiguration.WsEnviarLoteEventos);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return ServicoEnviarLoteEventosClient.GetEndpointAddress(EndpointConfiguration.WsEnviarLoteEventos);
        }
        
        public enum EndpointConfiguration
        {
            
            WsEnviarLoteEventos,
        }
    }
}
